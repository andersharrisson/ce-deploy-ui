Feature: Deployment Tool Main Page

  I want to see login screen when opening application
  
  @focus
  Scenario: Opening application when not authenticated
    Given the user is not authenticated
    When the user opens the application
    Then the login button should be visible