import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";

const baseUrl = 'http://localhost:3000'

Given('the user is not authenticated', () => {
    cy.clearCookies();

    cy.request({
        url: baseUrl+'/api/v1/get_helper/user_info', 
        failOnStatusCode: false
    })
  .should((response) => {
    expect(response.status).to.eq(401)
  })
})

When('the user opens the application', () => {
  cy.visit(baseUrl);
})

Then('the login button should be visible', () => {
  cy.get("button").contains("Login")
})

