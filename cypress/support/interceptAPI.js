import { apiHandlers } from "../../src/mocks/mockAPI";

const glue = (requestHandler) => {
  return (req) => {
    const d = requestHandler(req);
    req.reply({
      body: d.body,
      statusCode: d.status ?? 200,
      headers: d.headers
    })
  }
}

export function interceptAPI() {
  apiHandlers.forEach(h => {
    cy.intercept(h.method, h.matcher, glue(h.requestHandler)).as(h.name)
  })
}