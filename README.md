# CE deploy & monitor web UI

This project is meant to be the web User Interface for the CE deploy & monitor tool. The project is written in ReactJS.

## Developing environment
* NodeJS and npm
* Docker (if want to run backend locally for test purposes)
* IDE for JavaScript developement (e.g. Visual Studio Code)
* Credentials from ESS Artifactory, with permission to the ics-npm registry

(For developing purposes it is suggested to use _Node Version Manager_ [NVM] which makes easy to change between different Node versions).
* Windows: https://github.com/coreybutler/nvm-windows
* Mac/Linux: https://github.com/nvm-sh/nvm

### Required versions
* Node: v14.10+
* npm: v6.10+

## Local development setup

### Installing Dependencies

This project includes private dependencies from Artifactory, so additional configuration is required.

 1. Create an `.npmrc` file based on the example provided in this registry, substituting your email and base64 credentials.
 1. Install dependencies with `npm ci`.

Note, we lock dependency versions with _package-lock.json_; please install dependencies with `npm ci` (not npm install) to avoid overwriting the package-lock.

### Proxying to Existing Backends
For being able to run the backend, and frontend application on the same machine (locally) a proxy has been set in the project!
If you <ins>don't want to run</ins> the backend server, and frontend on the same machine, or have different settings, please adjust the `package.json` at the following line:
`"proxy": "http://localhost:8080"`

## External configuration
All necessary configuration is stored in a config file in the `public` folder, called `config.js`. The config file has to be included in the _index.html_ file!
The following values can be set in the file:

| Environment variable     | Description |
| -------------------------|-------------|
| SERVER_ADDRESS        | The backend server base URL _(host:port)_ (if backend server is running natively on the same host as the UI then the value has to be empty, and proxy has to be set!)|
| API_BASE_ENDPOINT     | The context path of the application's REST API _(e.g. /api/spec)_ |
| SWAGGER_UI_URL        | The URL for the backend Swagger-ui interface _(e.g./api/docs)_ |
| TOKEN_RENEW_INTERVAL  | Time interval to renew JWT auth token _(in milliseconds)_ |
| NAMING_ADDRESS        | Naming service base URL (used for IOC name Naming reference feature) | 
| ENVIRONMENT_TITLE     | Adds a (sub)text to the HTLM title, and also for the appBar header. Supposed to show information to the user which environment they are using. The default value is empty. _Optional field!_ |

Refering to a value in the JS source code is: _window.field-name_ (e.g. window.SERVER_ADDRESS).

### Setting Environment Variables
Environment variables are stored in `.env` files.
Precedency of environment variable files are (files on the left have more priority than files on the right):
* npm start: `.env.development.local`, `.env.local`, `.env.development`, `.env`
* npm run build: `.env.production.local`, `.env.local`, `.env.production`, `.env`
* npm test: `.env.test.local`, `.env.test`, `.env` (note .env.local is missing)

All environment variables start with `REACT_APP_` prefix.

## Useful links to start developing in ReactJS

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

## [ESLint](https://eslint.org) & [Prettier](https://prettier.io)
Linting can be checked, and to some extent auto fixed by running following scripts:  

### `npm run lint`
will run `npx eslint src` which will run eslint on all files in the src directory and write out the results of the check in your console.  

### `npm run lint:fix`
will run `npm run lint -- --fix` which will run eslint on all files in the src directory and fix what can be auto fixed and write out the rest of the results of the check, of what could not be auto fixed, in your console.  

### `npm run prettier`
will run `npx prettier src --check` which will run prettier on all files in the src directory and write out which files would be changed in your console.  

### `npm run prettier:fix`
will run `npm run prettier -- --write` which will run prettier on all files in the src directory and write out which files have be changed in your console.  

### `npm run format`
will run `npm run prettier:fix && npm run lint:fix` which will run lint:fix and the prettier:fix scripts mentioned above.  

## [Pre-Commit](https://pre-commit.com/)

### Installation
You will need to install pre-commit first locally on you machine, the install it in you project, instructions on how to do this you can find here: https://pre-commit.com/#install  

By running `pre-commit run --all-files` pre-commit will run both eslint and prettier on all files in src, much like the scripts from the package.json

## End-to-end testing with Cypress

For end-to-end testing we user Cypress, tests are written in gherkin syntax.
To run Cypress tests, <ins>you have to</ins>
- run the backend server
- run the frontend code

Details:
https://github.com/TheBrainFamily/cypress-cucumber-preprocessor

Then to open Cypress dialog window enter:
### `npx cypress open`

To run Cypress tests from command line (without the test-dialog), enter
### `npx cypress run`

## Jest-tests

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

[https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

[https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

[https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

[https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

[https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

[https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
