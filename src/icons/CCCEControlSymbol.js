import React from "react";
import { ReactComponent as CcceControlSymbolGray } from "./resources/control/ccce-control-symbol_757575.svg";
import SvgIcon from "@mui/material/SvgIcon";

export function CCCEControlSymbol(props) {
  return (
    <SvgIcon {...props}>
      <CcceControlSymbolGray />
    </SvgIcon>
  );
}
