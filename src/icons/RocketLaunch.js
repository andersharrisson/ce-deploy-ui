import React from "react";
import { ReactComponent as RocketLaunchBlack } from "./resources/rocket/rocket_launch_black_24dp.svg";
import { Tooltip } from "@mui/material";
import SvgIcon from "@mui/material/SvgIcon";

export function RocketLaunch(props) {
  return (
    <SvgIcon {...props}>
      <Tooltip title="Deployment">
        <RocketLaunchBlack />
      </Tooltip>
    </SvgIcon>
  );
}
