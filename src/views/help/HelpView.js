import React, { useContext, useEffect } from "react";
import { RootPaper, Help } from "@ess-ics/ce-ui-common";
import { Stack, Typography } from "@mui/material";
import { GlobalAppBarContext } from "@ess-ics/ce-ui-common";
import { applicationTitle } from "../../components/common/Helper";

export function HelpView() {
  const { setTitle } = useContext(GlobalAppBarContext);
  useEffect(() => setTitle(applicationTitle("Help")), [setTitle]);

  return (
    <RootPaper>
      <Help
        summary={
          <Stack gap={1.5}>
            <Typography>
              CE Deploy & Monitor is the deployment and monitoring parts of the
              IOC toolchain in the Controls Ecosystem.{" "}
            </Typography>
            <Typography>
              It is a tool that manages IOCs on host machines, using Ansible to
              perform the necessary configuration management. This tool will set
              up IOC hosts and deploy IOCs in a consistent manner, and will
              configure all necessary services needed for this purpose. It also
              integrates with various other systems to enable monitoring of IOCs
              and hosts, and further enables some limited remote execution
              features.
            </Typography>
            <Typography fontStyle="italic">
              Be aware that the tool does{" "}
              <Typography
                component="span"
                fontWeight="bold"
              >
                not
              </Typography>{" "}
              set up your host (and OS) - this needs to be done in CSEntry.
            </Typography>
          </Stack>
        }
        docsHref="https://confluence.esss.lu.se/x/CVGQFg"
        supportHref={window.SUPPORT_URL}
        version={window.FRONTEND_VERSION}
        versionHref={`${window.FRONTEND_REPOSITORY_TAGS}${window.FRONTEND_VERSION}`}
      />
    </RootPaper>
  );
}
