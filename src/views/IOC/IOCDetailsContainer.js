import React, { useEffect, useContext, useState, useMemo } from "react";
import { IOCDetailsView } from "./IOCDetailsView";
import { LinearProgress } from "@mui/material";
import NotFoundView from "../../components/navigation/NotFoundView/NotFoundView";
import { onFetchEntityError } from "../../components/common/Helper";
import { userContext } from "@ess-ics/ce-ui-common/dist/contexts/User";
import useUrlState from "@ahooksjs/use-url-state";
import { apiContext } from "../../api/DeployApi";
import { useAPIMethod } from "@ess-ics/ce-ui-common";

export function IOCDetailsContainer({ id }) {
  const { user } = useContext(userContext);
  const [urlState] = useUrlState();
  const [error, setError] = useState(null);

  const client = useContext(apiContext);

  const params = useMemo(
    () => ({
      ioc_id: id
    }),
    [id]
  );

  const {
    value: ioc,
    wrapper: getIOC,
    loading,
    error: fetchError,
    abort: abortGetIOC
  } = useAPIMethod({
    fcn: client.apis.IOCs.getIoc,
    params
  });

  useEffect(() => {
    if (fetchError) {
      onFetchEntityError(fetchError?.message, fetchError?.status, setError);
    }
  }, [fetchError]);

  useEffect(() => {
    // user logs in => clear error message, and try to re-request userInfo
    if (user) {
      setError(null);
    } else if (!user && urlState?.tab) {
      // user is not logged in => show a message
      setError({ message: "Unauthorized", status: "401" });
    }
  }, [urlState, user]);

  return (
    <>
      {error ? (
        <NotFoundView
          message={error?.message}
          status={error?.status}
        />
      ) : ioc ? (
        <IOCDetailsView
          ioc={ioc}
          getIOC={getIOC}
          abortGetIOC={abortGetIOC}
          loading={loading}
        />
      ) : (
        <LinearProgress color="primary" />
      )}
    </>
  );
}
