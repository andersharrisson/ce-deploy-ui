import React from "react";
import { GlobalAppBarContext } from "@ess-ics/ce-ui-common";
import { applicationTitle } from "../../components/common/Helper";
import { CreateIOC } from "../../components/IOC/CreateIOC";
import { useContext, useEffect } from "react";

export const CreateIOCView = () => {
  const { setTitle } = useContext(GlobalAppBarContext);
  useEffect(() => setTitle(applicationTitle("Create IOC")), [setTitle]);

  return <CreateIOC />;
};
