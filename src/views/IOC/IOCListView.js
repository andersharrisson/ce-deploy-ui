import React, { useState, useEffect, useContext, useCallback } from "react";
import { Container, Grid, Tabs, Tab } from "@mui/material";
import {
  GlobalAppBarContext,
  RootPaper,
  useAPIMethod,
  usePagination
} from "@ess-ics/ce-ui-common";
import {
  applicationTitle,
  initRequestParams
} from "../../components/common/Helper";
import { SearchBar } from "../../components/common/SearchBar/SearchBar";
import useUrlState from "@ahooksjs/use-url-state";
import {
  serialize,
  deserialize
} from "../../components/common/URLState/URLState";
import { useMemo } from "react";
import { apiContext } from "../../api/DeployApi";
import IOCTable from "../../components/IOC/IOCTable";

export function IOCListView() {
  const { setTitle } = useContext(GlobalAppBarContext);
  useEffect(() => setTitle(applicationTitle("IOCs")), [setTitle]);

  const client = useContext(apiContext);

  const {
    value: iocs,
    wrapper: getIocs,
    loading,
    dataReady,
    abort
  } = useAPIMethod({
    fcn: client.apis.IOCs.listIocs,
    call: false
  });

  const [urlState, setUrlState] = useUrlState(
    {
      tab: "0",
      rows: "20",
      page: "0",
      query: ""
    },
    { navigateMode: "replace" }
  );

  const [deploymentStatus, setDeploymentStatus] = useState("ALL");

  const handleTabChange = useCallback(
    (tab) => {
      setUrlState((s) =>
        serialize(s.tab) === serialize(tab)
          ? { tab: serialize(tab) }
          : { tab: serialize(tab), page: "0" }
      );
      changeTab(tab);
    },
    [setUrlState]
  );

  const changeTab = (tab) => {
    if (tab === 0) {
      setDeploymentStatus("ALL");
    } else if (tab === 1) {
      setDeploymentStatus("DEPLOYED");
    } else if (tab === 2) {
      setDeploymentStatus("NOT_DEPLOYED");
    }
  };

  useEffect(() => {
    urlState.tab && changeTab(deserialize(urlState.tab));
  }, [urlState.tab]);

  const urlPagination = useMemo(() => {
    return {
      rows: deserialize(urlState.rows),
      page: deserialize(urlState.page)
    };
  }, [urlState.rows, urlState.page]);

  const setUrlPagination = useCallback(
    ({ rows, page }) => {
      setUrlState({
        rows: serialize(rows),
        page: serialize(page)
      });
    },
    [setUrlState]
  );

  const rowsPerPage = [20, 50];

  const { pagination, setPagination } = usePagination({
    rowsPerPageOptions: rowsPerPage,
    initLimit: urlPagination.rows ?? rowsPerPage[0],
    initPage: urlPagination.page ?? 0
  });

  // update pagination whenever search result total pages change
  useEffect(() => {
    setPagination({ totalCount: iocs?.totalCount ?? 0 });
  }, [setPagination, iocs?.totalCount]);

  // whenever url state changes, update pagination
  useEffect(() => {
    setPagination({ ...urlPagination });
  }, [setPagination, urlPagination]);

  // whenever table pagination internally changes (user clicks next page etc)
  // update the row params
  useEffect(() => {
    setUrlPagination(pagination);
  }, [pagination, setUrlPagination]);

  // Invoked by Table on change to pagination
  const onPage = (params) => {
    setPagination(params);
    abort();
  };

  useEffect(() => {
    let requestParams = initRequestParams(
      pagination,
      deserialize(urlState.query)
    );

    requestParams.deployment_status = deploymentStatus;

    getIocs(requestParams);

    return () => {
      abort();
    };
  }, [
    getIocs,
    urlPagination,
    deploymentStatus,
    urlState.query,
    pagination,
    abort
  ]);

  // Callback for searchbar, called whenever user updates search
  const setSearch = useCallback(
    (query) => {
      setUrlState({ query: serialize(query) });
    },
    [setUrlState]
  );

  let content = (
    <SearchBar
      search={setSearch}
      query={deserialize(urlState.query)}
      loading={loading}
      placeholder="Search"
    >
      <IOCTable
        iocs={iocs?.iocList}
        loading={loading || !dataReady}
        rowType="explore"
        pagination={pagination}
        onPage={onPage}
      />
    </SearchBar>
  );

  return (
    <RootPaper>
      <Grid
        container
        spacing={1}
      >
        <Grid
          container
          spacing={1}
          component={Container}
          justifyContent="space-between"
          alignItems="center"
          style={{ display: "flex" }}
        >
          <Grid item>
            <Tabs
              value={deserialize(urlState.tab)}
              onChange={(event, tab) => handleTabChange(tab)}
            >
              <Tab label="All" />
              <Tab label="Only Deployed" />
              <Tab label="Only Not Deployed" />
            </Tabs>
          </Grid>
        </Grid>
        <Grid
          item
          xs={12}
          md={12}
        >
          {content}
        </Grid>
      </Grid>
    </RootPaper>
  );
}
