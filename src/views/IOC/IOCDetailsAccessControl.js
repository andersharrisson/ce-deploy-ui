import React from "react";
import { RootPaper } from "@ess-ics/ce-ui-common";
import AccessControl from "../../components/auth/AccessControl";
import { IOCDetailsContainer } from "./IOCDetailsContainer";
import { useParams } from "react-router-dom";

export function IOCDetailsAccessControl() {
  const { id } = useParams();

  return (
    <RootPaper>
      <AccessControl allowedRoles={[]}>
        <IOCDetailsContainer id={id} />
      </AccessControl>
    </RootPaper>
  );
}
