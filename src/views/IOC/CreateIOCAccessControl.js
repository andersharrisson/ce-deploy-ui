import { AccessControl } from "@ess-ics/ce-ui-common";
import React from "react";
import { CreateIOCView } from "./CreateIOCView";
import { Navigate } from "react-router-dom";

export const CreateIOCAccessControl = () => {
  return (
    <AccessControl
      allowedRoles={["DeploymentToolAdmin", "DeploymentToolIntegrator"]}
      renderNoAccess={() => (
        <Navigate
          to="/"
          push
        />
      )}
    >
      <CreateIOCView />
    </AccessControl>
  );
};
