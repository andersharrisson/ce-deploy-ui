import { Grid, IconButton, Stack } from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import React, {
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState
} from "react";
import { IOCLiveStatus } from "../../components/IOC/IOCLiveStatus";
import { IOCManage } from "../../components/IOC/IOCManage";
import { useNavigate } from "react-router-dom";
import IOCAdmin from "../../components/IOC/IOCAdmin";
import {
  applicationTitle,
  initRequestParams
} from "../../components/common/Helper";
import {
  GlobalAppBarContext,
  useAPIMethod,
  useIsCurrentUserPermitted,
  TabPanel,
  usePagination
} from "@ess-ics/ce-ui-common";
import { useSafePolling } from "../../hooks/Polling";
import useUrlState from "@ahooksjs/use-url-state";
import {
  serialize,
  deserialize
} from "../../components/common/URLState/URLState";
import { apiContext } from "../../api/DeployApi";

const IOC_POLL_INTERVAL = 10000;
export function IOCDetailsView({ ioc, getIOC, abortGetIOC, loading }) {
  const { setTitle } = useContext(GlobalAppBarContext);
  useEffect(() => {
    if (ioc) {
      setTitle(applicationTitle(`IOC Details: ${ioc.namingName}`));
    }
  }, [ioc, setTitle]);

  const [urlState, setUrlState] = useUrlState(
    {
      tab: "Status",
      jobs_rows: "5",
      jobs_page: "0"
    },
    { navigateMode: "replace" }
  );

  const [buttonDisabled, setButtonDisabled] = useState(false);
  const navigate = useNavigate();

  const client = useContext(apiContext);
  const {
    value: operations,
    wrapper: getOperations,
    loading: operationsLoading,
    dataReady: operationsDataReady,
    abort: abortGetOperations
  } = useAPIMethod({
    fcn: client.apis.Deployments.listOperations,
    call: false
  });

  const ongoingCommandParams = useMemo(
    () => ({
      ioc_id: ioc.id,
      type: "COMMAND",
      status: "ONGOING"
    }),
    [ioc]
  );

  const {
    value: ongoingCommand,
    wrapper: getOngoingCommand,
    loading: ongoingCommandLoading,
    dataReady: ongoingCommandDataReady,
    abort: abortGetOngoingCommand
  } = useAPIMethod({
    fcn: client.apis.Deployments.listOperations,
    params: ongoingCommandParams,
    call: false
  });

  const jobUrlPagination = useMemo(() => {
    return {
      rows: deserialize(urlState.jobs_rows),
      page: deserialize(urlState.jobs_page)
    };
  }, [urlState.jobs_rows, urlState.jobs_page]);

  const setJobUrlPagination = useCallback(
    (params) => {
      setUrlState({
        jobs_rows: serialize(params.rows),
        jobs_page: serialize(params.page)
      });
    },
    [setUrlState]
  );

  const rowsPerPage = [5, 20];

  const { pagination: jobPagination, setPagination: setJobPagination } =
    usePagination({
      rowsPerPageOptions: rowsPerPage,
      initLimit: jobUrlPagination.rows ?? rowsPerPage[0],
      initPage: jobUrlPagination.page ?? 0
    });

  // update pagination whenever search result total pages change
  useEffect(() => {
    setJobPagination({ totalCount: operations?.totalCount ?? 0 });
  }, [setJobPagination, operations?.totalCount]);

  // whenever url state changes, update pagination
  useEffect(() => {
    setJobPagination({ ...jobUrlPagination });
  }, [setJobPagination, jobUrlPagination]);

  // whenever table pagination internally changes (user clicks next page etc)
  // update the row params
  useEffect(() => {
    setJobUrlPagination(jobPagination);
  }, [jobPagination, setJobUrlPagination]);

  // Invoked by Table on change to pagination
  const onPage = useCallback(
    (params) => {
      setJobPagination(params);
      abortGetOperations();
    },
    [abortGetOperations, setJobPagination]
  );

  useSafePolling(getIOC, loading, IOC_POLL_INTERVAL, true, abortGetIOC);
  useSafePolling(
    getOngoingCommand,
    ongoingCommandLoading || !ongoingCommandDataReady,
    IOC_POLL_INTERVAL,
    true,
    abortGetOngoingCommand
  );

  useEffect(() => {
    setButtonDisabled(Boolean(ioc?.operationInProgress));
  }, [ioc?.operationInProgress]);

  const onTabChange = (index, label) => {
    setUrlState({ tab: serialize(label) });
  };

  // Submit new search whenever pagination or ioc changes
  useEffect(() => {
    let requestParams = initRequestParams(jobPagination);
    requestParams.ioc_id = ioc.id;
    getOperations(requestParams);

    return () => {
      abortGetOperations();
    };
  }, [getOperations, jobPagination, ioc, abortGetOperations]);

  const handleClick = () => {
    navigate(-1);
  };

  const resetTab = useCallback(() => {
    if (ioc?.activeDeployment) {
      setUrlState({ tab: "Status" });
    } else {
      setUrlState({ tab: "Management" });
    }
  }, [ioc?.activeDeployment, setUrlState]);

  const setButtonDisabledAndUpdate = useCallback(
    (isDisabled) => {
      setButtonDisabled(isDisabled);
      getIOC();
    },
    [getIOC]
  );

  const tabs = [
    {
      label: "Status",
      content: <IOCLiveStatus ioc={ioc} />
    }
  ];

  const isPermittedManagement = useIsCurrentUserPermitted({
    allowedRoles: ["DeploymentToolAdmin", "DeploymentToolIntegrator"]
  });

  const isPermittedAdmin = useIsCurrentUserPermitted({
    allowedRoles: ["DeploymentToolAdmin"]
  });

  if (isPermittedManagement) {
    tabs.push({
      label: "Management",
      content: (
        <IOCManage
          ioc={ioc}
          getIOC={getIOC}
          buttonDisabled={buttonDisabled}
          currentCommand={
            ongoingCommand?.operations?.length > 0
              ? ongoingCommand.operations[0]
              : null
          }
          operations={operations?.operations}
          operationsLoading={operationsLoading || !operationsDataReady}
          getOperations={getOperations}
          setButtonDisabled={setButtonDisabledAndUpdate}
          pagination={jobPagination}
          onPage={onPage}
        />
      )
    });
  }
  if (isPermittedAdmin) {
    tabs.push({
      label: "Admin",
      content: (
        <IOCAdmin
          ioc={ioc}
          getIOC={getIOC}
          resetTab={resetTab}
          buttonDisabled={buttonDisabled}
          setButtonDisabled={setButtonDisabled}
        />
      )
    });
  }
  const initialIndex = tabs.map((it) => it.label).indexOf(urlState?.tab);

  return (
    <Grid
      container
      spacing={1}
    >
      <Grid
        item
        xs={12}
        style={{ paddingBottom: 0 }}
      >
        <TabPanel
          tabs={tabs}
          initialIndex={initialIndex}
          onTabChange={onTabChange}
          TabsProps={{ centered: true, sx: { flex: 1 } }}
          renderTabs={(tabs) => (
            <Stack
              flexDirection="row"
              justifyContent="space-between"
            >
              <IconButton
                color="inherit"
                onClick={handleClick}
                size="large"
              >
                <ArrowBackIcon />
              </IconButton>
              {tabs}
            </Stack>
          )}
        />
      </Grid>
    </Grid>
  );
}
