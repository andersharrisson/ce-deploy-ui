import React, {
  useState,
  useEffect,
  useCallback,
  useContext,
  useMemo
} from "react";
import { Container, Grid, Tabs, Tab } from "@mui/material";
import { HostTable } from "../../components/host/HostTable";
import {
  GlobalAppBarContext,
  RootPaper,
  useAPIMethod,
  usePagination
} from "@ess-ics/ce-ui-common";
import {
  applicationTitle,
  initRequestParams,
  transformHostQuery
} from "../../components/common/Helper";
import { SearchBar } from "../../components/common/SearchBar/SearchBar";
import useUrlState from "@ahooksjs/use-url-state";
import {
  serialize,
  deserialize
} from "../../components/common/URLState/URLState";
import { apiContext } from "../../api/DeployApi";

export function HostListView() {
  const { setTitle } = useContext(GlobalAppBarContext);
  useEffect(() => setTitle(applicationTitle("IOC hosts")), [setTitle]);

  const client = useContext(apiContext);
  const {
    value: hosts,
    wrapper: getHosts,
    loading,
    abort
  } = useAPIMethod({
    fcn: client.apis.Hosts.listHosts,
    call: false
  });

  const [urlState, setUrlState] = useUrlState(
    {
      tab: "0",
      rows: "20",
      page: "0",
      query: ""
    },
    { navigateMode: "replace" }
  );
  const [hostFilter, setHostFilter] = useState("ALL");

  const urlPagination = useMemo(
    () => ({
      rows: deserialize(urlState.rows),
      page: deserialize(urlState.page)
    }),
    [urlState.rows, urlState.page]
  );

  const setUrlPagination = useCallback(
    ({ rows, page }) => {
      setUrlState({
        rows: serialize(rows),
        page: serialize(page)
      });
    },
    [setUrlState]
  );

  const handleTabChange = useCallback(
    (event, tab) => {
      setUrlState((s) =>
        serialize(s.tab) === serialize(tab)
          ? { tab: serialize(tab) }
          : { tab: serialize(tab), page: "0" }
      );

      changeTab(tab);
    },
    [setUrlState]
  );

  const changeTab = (tab) => {
    if (tab === 0) {
      setHostFilter("ALL");
    } else if (tab === 1) {
      setHostFilter("IOCS_DEPLOYED");
    } else if (tab === 2) {
      setHostFilter("NO_IOCS");
    }
  };

  useEffect(() => {
    urlState.tab && changeTab(deserialize(urlState.tab));
  }, [urlState]);

  const rowsPerPage = [20, 50];

  const { pagination, setPagination } = usePagination({
    rowsPerPageOptions: rowsPerPage,
    initLimit: urlPagination.rows ?? rowsPerPage[0],
    initPage: urlPagination.page ?? 0
  });

  // update pagination whenever search result total pages change
  useEffect(() => {
    setPagination({ totalCount: hosts?.totalCount ?? 0 });
  }, [setPagination, hosts?.totalCount]);

  // whenever url state changes, update pagination
  useEffect(() => {
    setPagination({ ...urlPagination });
  }, [setPagination, urlPagination]);

  // whenever table pagination internally changes (user clicks next page etc)
  // update the row params
  useEffect(() => {
    setUrlPagination(pagination);
  }, [pagination, setUrlPagination]);

  // Request new search results whenever search or pagination changes
  useEffect(() => {
    let requestParams = initRequestParams(
      pagination,
      transformHostQuery(deserialize(urlState.query))
    );
    requestParams.filter = hostFilter;
    getHosts(requestParams);

    return () => {
      abort();
    };
  }, [getHosts, hostFilter, urlState.query, pagination, abort]);

  // Callback for searchbar, called whenever user updates search
  const setSearch = useCallback(
    (query) => {
      setUrlState({ query: serialize(query) });
    },
    [setUrlState]
  );

  // Invoked by Table on change to pagination
  const onPage = (params) => {
    setPagination(params);
    abort();
  };

  const content = (
    <>
      <SearchBar
        search={setSearch}
        query={deserialize(urlState.query)}
        loading={loading}
      >
        <HostTable
          hosts={hosts?.csEntryHosts ?? []}
          loading={loading}
          pagination={pagination}
          onPage={onPage}
        />
      </SearchBar>
    </>
  );

  return (
    <RootPaper>
      <Grid
        container
        spacing={1}
      >
        <Grid
          container
          spacing={1}
          component={Container}
          justifyContent="space-between"
          alignItems="center"
          style={{ display: "flex" }}
        >
          <Grid item>
            <Tabs
              value={deserialize(urlState.tab)}
              onChange={handleTabChange}
            >
              <Tab label="All" />
              <Tab label="Only hosts with IOCs" />
              <Tab label="Only hosts without IOCs" />
            </Tabs>
          </Grid>
        </Grid>
        <Grid
          item
          xs={12}
          md={12}
        >
          {content}
        </Grid>
      </Grid>
    </RootPaper>
  );
}
