import React from "react";
import { RootPaper } from "@ess-ics/ce-ui-common";
import AccessControl from "../../components/auth/AccessControl";
import { HostDetailsContainer } from "./HostDetailsContainer";
import { useParams } from "react-router-dom";

export function HostDetailsAccessControl() {
  const { id } = useParams();

  return (
    <RootPaper data-testid="host-details-container">
      <AccessControl allowedRoles={[]}>
        <HostDetailsContainer id={id} />
      </AccessControl>
    </RootPaper>
  );
}
