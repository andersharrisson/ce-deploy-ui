import React, { useState, useMemo, useContext, useEffect } from "react";
import { HostDetailsView } from "./HostDetailsView";
import { LinearProgress } from "@mui/material";
import { onFetchEntityError } from "../../components/common/Helper";
import NotFoundView from "../../components/navigation/NotFoundView/NotFoundView";
import { apiContext } from "../../api/DeployApi";
import { useAPIMethod } from "@ess-ics/ce-ui-common";

export function HostDetailsContainer({ id }) {
  const [error, setError] = useState(null);
  const client = useContext(apiContext);

  const params = useMemo(
    () => ({
      host_csentry_id: id
    }),
    [id]
  );

  const { value: host, error: fetchError } = useAPIMethod({
    fcn: client.apis.Hosts.findHostById,
    params
  });

  useEffect(() => {
    if (fetchError) {
      onFetchEntityError(fetchError?.message, fetchError?.status, setError);
    }
  }, [fetchError]);

  return (
    <>
      {error ? (
        <NotFoundView />
      ) : host ? (
        <HostDetailsView
          host={host}
          id={id}
        />
      ) : (
        <LinearProgress color="primary" />
      )}
    </>
  );
}
