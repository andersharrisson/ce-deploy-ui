import React, { useEffect, useCallback, useContext, useMemo } from "react";
import {
  Box,
  IconButton,
  Typography,
  Link as MuiLink,
  Stack
} from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { HostBadge } from "../../components/host/HostBadge";
import {
  KeyValueTable,
  SimpleAccordion,
  GlobalAppBarContext,
  useAPIMethod,
  AlertBannerList,
  usePagination
} from "@ess-ics/ce-ui-common";
import { LokiPanel } from "../../components/common/Loki/LokiPanel";
import { useNavigate, Link as ReactRouterLink } from "react-router-dom";
import {
  applicationTitle,
  initRequestParams
} from "../../components/common/Helper";
import AccessControl from "../../components/auth/AccessControl";
import useUrlState from "@ahooksjs/use-url-state";
import {
  serialize,
  deserialize
} from "../../components/common/URLState/URLState";
import { apiContext } from "../../api/DeployApi";
import IOCTable from "../../components/IOC/IOCTable";
import {
  ExternalLink,
  ExternalLinkContents
} from "../../components/common/Link";

export function HostDetailsView({ id, host }) {
  const { setTitle } = useContext(GlobalAppBarContext);
  useEffect(() => {
    if (host && host.csEntryHost) {
      setTitle(applicationTitle("Host Details: " + host.csEntryHost.name));
    }
  }, [host, setTitle]);

  const client = useContext(apiContext);

  const {
    value: iocs,
    wrapper: getIocs,
    loading,
    dataReady,
    abort: abortGetIocs
  } = useAPIMethod({
    fcn: client.apis.Hosts.findAssociatedIocsByHostId,
    call: false
  });

  const [urlState, setUrlState] = useUrlState(
    {
      iocs_rows: "20",
      iocs_page: "0",
      iocs_open: "true",
      syslog_open: "true",
      details_open: "false"
    },
    { navigateMode: "replace" }
  );

  const navigate = useNavigate();

  const urlPagination = useMemo(() => {
    return {
      rows: deserialize(urlState.iocs_rows),
      page: deserialize(urlState.iocs_page)
    };
  }, [urlState]);

  const setUrlPagination = useCallback(
    ({ rows, page }) => {
      setUrlState({
        iocs_rows: serialize(rows),
        iocs_page: serialize(page)
      });
    },
    [setUrlState]
  );

  const rowsPerPage = [20, 50];

  const { pagination, setPagination } = usePagination({
    rowsPerPageOptions: rowsPerPage,
    initLimit: urlPagination.iocs_rows ?? rowsPerPage[0],
    initPage: urlPagination.iocs_page ?? 0
  });

  // update pagination whenever search result total pages change
  useEffect(() => {
    setPagination({ totalCount: iocs?.totalCount ?? 0 });
  }, [setPagination, iocs?.totalCount]);

  // whenever url state changes, update pagination
  useEffect(() => {
    setPagination({ ...urlPagination });
  }, [setPagination, urlPagination]);

  // whenever table pagination internally changes (user clicks next page etc)
  // update the row params
  useEffect(() => {
    setUrlPagination(pagination);
  }, [pagination, setUrlPagination]);

  // Invoked by Table on change to pagination
  const onPage = (params) => {
    setPagination(params);
    abortGetIocs();
  };

  useEffect(() => {
    let requestParams = initRequestParams(urlPagination, null);

    requestParams.host_csentry_id = id;

    getIocs(requestParams);

    return () => {
      abortGetIocs();
    };
  }, [getIocs, urlPagination, id, abortGetIocs]);

  function listToString(list) {
    if (list) {
      if (Array.isArray(list)) {
        return list.join(", ");
      }
      return list;
    }

    return "";
  }

  function renderVmOwners(owners) {
    if (owners && owners.length > 0) {
      return (
        <Stack
          direction="row"
          flexWrap="wrap"
          divider={<Typography component="span">,&nbsp;</Typography>}
        >
          {owners.map((username) => (
            <MuiLink
              component={ReactRouterLink}
              to={`/user/${username}`}
              underline="hover"
              key={username}
            >
              {username}
            </MuiLink>
          ))}
        </Stack>
      );
    } else {
      return "---";
    }
  }

  const renderHost = {
    "registered by": (
      <MuiLink
        component={ReactRouterLink}
        to={`/user/${host?.csEntryHost?.user}`}
        underline="hover"
      >
        {host?.csEntryHost?.user}
      </MuiLink>
    ),
    "device type": host?.csEntryHost?.device_type,
    description: host?.csEntryHost?.description,
    scope: host?.csEntryHost?.scope,
    "vm owner": renderVmOwners(host?.csEntryHost?.ansible_vars?.vm_owner),
    "ansible groups": listToString(host?.csEntryHost?.ansible_groups)
  };

  const handleClick = (event) => {
    navigate(-1);
  };

  return (
    <Stack gap={2}>
      <Box>
        <IconButton
          color="inherit"
          onClick={handleClick}
          size="large"
        >
          <ArrowBackIcon />
        </IconButton>
      </Box>
      {host ? (
        <>
          <AlertBannerList alerts={host?.alerts ?? []} />
          <HostBadge host={host} />
        </>
      ) : null}
      <Stack gap={2}>
        <Typography variant="h3">IOCs</Typography>
        <IOCTable
          iocs={iocs?.deployedIocs}
          loading={loading || !dataReady}
          rowType="host"
          pagination={pagination}
          onPage={onPage}
        />
      </Stack>
      <AccessControl
        allowedRoles={["DeploymentToolAdmin", "DeploymentToolIntegrator"]}
        renderNoAccess={() => <></>}
      >
        {host ? (
          <SimpleAccordion
            summary="Host details"
            expanded={deserialize(urlState.details_open)}
            onChange={(event, expanded) =>
              setUrlState({ details_open: serialize(expanded) })
            }
            sx={{ marginTop: "0 !important" }}
          >
            <KeyValueTable
              obj={renderHost}
              variant="overline"
            />
          </SimpleAccordion>
        ) : null}
      </AccessControl>
      <AccessControl
        allowedRoles={["DeploymentToolAdmin", "DeploymentToolIntegrator"]}
        renderNoAccess={() => <></>}
      >
        <Stack gap={2}>
          <Typography variant="h3">Host log stream</Typography>
          {host ? (
            <LokiPanel
              host={host}
              isSyslog
              isDeployed
            />
          ) : null}
        </Stack>
      </AccessControl>
      <KeyValueTable
        obj={{
          "Host Configuration": (
            <ExternalLink
              href={`https://csentry.esss.lu.se/network/hosts/view/${host?.csEntryHost.name}`}
              aria-label="Host Configuration"
            >
              <ExternalLinkContents>
                {`https://csentry.esss.lu.se/network/hosts/view/${host?.csEntryHost?.name}`}
              </ExternalLinkContents>
            </ExternalLink>
          ),
          "Host Metrics": (
            <ExternalLink
              href={`https://grafana.tn.esss.lu.se/d/5zJT23xWz/node-exporter-full?orgId=1&var-node=${host?.csEntryHost.fqdn}`}
              aria-label="Host Metrics"
            >
              <ExternalLinkContents>
                {`https://grafana.tn.esss.lu.se/d/5zJT23xWz/node-exporter-full?orgId=1&var-node=${host?.csEntryHost.fqdn}`}
              </ExternalLinkContents>
            </ExternalLink>
          )
        }}
        variant="overline"
      />
    </Stack>
  );
}
