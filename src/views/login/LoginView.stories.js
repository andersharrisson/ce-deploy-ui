import React from "react";
import { AppHarness } from "../../mocks/AppHarness";
import { LoginView } from "./LoginView";

export default {
  title: "Views/Login/LoginView",
  component: LoginView
};

const Template = (args) => (
  <AppHarness initialHistory={["/login"]}>
    <LoginView {...args} />
  </AppHarness>
);

export const Default = Template.bind({});
Default.args = {};
