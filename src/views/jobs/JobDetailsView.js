import React, { useContext, useEffect } from "react";
import { IconButton } from "@mui/material";
import { JobDetails } from "../../components/Job/JobDetails";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { useNavigate } from "react-router-dom";
import { GlobalAppBarContext } from "@ess-ics/ce-ui-common";
import { applicationTitle } from "../../components/common/Helper";

export function JobDetailsView({ operation, job }) {
  const navigate = useNavigate();

  const handleClick = () => {
    navigate(-1);
  };

  const { setTitle } = useContext(GlobalAppBarContext);
  useEffect(
    () =>
      operation &&
      setTitle(applicationTitle(`Job Details: ${operation.iocName}`)),
    [setTitle, operation]
  );

  return (
    <>
      <IconButton
        color="inherit"
        onClick={handleClick}
        size="large"
      >
        <ArrowBackIcon />
      </IconButton>
      {operation && (
        <JobDetails
          operation={operation}
          job={job}
        />
      )}
    </>
  );
}
