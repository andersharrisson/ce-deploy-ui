import React, {
  useCallback,
  useRef,
  useState,
  useContext,
  useMemo,
  useEffect
} from "react";
import { JobDetailsView } from "./JobDetailsView";
import { LinearProgress } from "@mui/material";
import { useSafePolling } from "../../hooks/Polling";
import NotFoundView from "../../components/navigation/NotFoundView/NotFoundView";
import { onFetchEntityError } from "../../components/common/Helper";
import { useAPIMethod } from "@ess-ics/ce-ui-common";
import { apiContext } from "../../api/DeployApi";

const POLL_DEPLOYMENT_INTERVAL = 5000;
export function JobDetailsContainer({ id }) {
  const [notFoundError, setNotFoundError] = useState();

  const client = useContext(apiContext);

  const params = useMemo(
    () => ({
      operation_id: id
    }),
    [id]
  );
  const {
    value: operation,
    wrapper: getOperation,
    loading: operationLoading,
    error: operationError
  } = useAPIMethod({
    fcn: client.apis.Deployments.fetchOperation,
    call: false,
    params
  });

  useEffect(() => {
    if (operationError) {
      onFetchEntityError(
        operationError?.message,
        operationError?.status,
        setNotFoundError
      );
    }
  }, [operationError]);

  const deploymentJobParams = useMemo(
    () => ({ awx_job_id: operation?.awxJobId }),
    [operation]
  );

  const {
    value: job,
    wrapper: getJobById,
    loading: jobLoading,
    dataReady
  } = useAPIMethod({
    fcn: client.apis.Deployments.fetchJobDetails,
    params: deploymentJobParams
  });

  const firstTime = useRef(true);

  const jobFinished = job?.finished;

  const getOperationUntilFinished = useCallback(() => {
    if (!jobFinished) {
      getOperation();
    }
  }, [jobFinished, getOperation]);

  const getAwxJobUntilFinished = useCallback(() => {
    if (!jobFinished || firstTime.current) {
      if (operation?.awxJobId) {
        getJobById(operation?.awxJobId);
        firstTime.current = false;
      }
    }
  }, [operation?.awxJobId, jobFinished, getJobById]);

  return (
    <>
      {notFoundError ? (
        <NotFoundView />
      ) : operation ? (
        <JobDetailsView
          operation={operation}
          job={job}
        />
      ) : (
        <LinearProgress color="primary" />
      )}
      <Watcher
        get={getOperationUntilFinished}
        loading={operationLoading}
      />
      {operation?.awxJobId && (
        <Watcher
          get={getAwxJobUntilFinished}
          loading={jobLoading || !dataReady}
        />
      )}
    </>
  );
}

function Watcher({ get, loading }) {
  useSafePolling(get, loading, POLL_DEPLOYMENT_INTERVAL);
  return null;
}
