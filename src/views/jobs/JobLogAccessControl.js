import React, { useContext, useEffect } from "react";
import AccessControl from "../../components/auth/AccessControl";
import { GlobalAppBarContext } from "@ess-ics/ce-ui-common";
import { JobListView } from "./JobListView";
import { applicationTitle } from "../../components/common/Helper";

export function JobLogAccessControl() {
  const { setTitle } = useContext(GlobalAppBarContext);
  useEffect(() => setTitle(applicationTitle("Log")), [setTitle]);

  return (
    <AccessControl allowedRoles={[]}>
      <JobListView />
    </AccessControl>
  );
}
