import React, { useContext, useCallback, useMemo } from "react";
import { Box } from "@mui/material";
import { RootPaper, useAPIMethod, usePagination } from "@ess-ics/ce-ui-common";
import { initRequestParams } from "../../components/common/Helper";
import { useEffect } from "react";
import { SearchBar } from "../../components/common/SearchBar/SearchBar";
import useUrlState from "@ahooksjs/use-url-state";
import {
  serialize,
  deserialize
} from "../../components/common/URLState/URLState";
import { JobTable } from "../../components/Job/JobTable";
import { apiContext } from "../../api/DeployApi";

export function JobListView() {
  const client = useContext(apiContext);

  const {
    value: operations,
    wrapper: getOperations,
    loading,
    dataReady,
    abort
  } = useAPIMethod({
    fcn: client.apis.Deployments.listOperations,
    call: false
  });

  const [urlState, setUrlState] = useUrlState(
    {
      rows: "20",
      page: "0",
      query: ""
    },
    { navigateMode: "replace" }
  );

  const urlPagination = useMemo(() => {
    return {
      rows: deserialize(urlState.rows),
      page: deserialize(urlState.page)
    };
  }, [urlState.rows, urlState.page]);

  const setUrlPagination = useCallback(
    ({ rows, page }) => {
      setUrlState({
        rows: serialize(rows),
        page: serialize(page)
      });
    },
    [setUrlState]
  );

  const rowsPerPage = [20, 50];

  const { pagination, setPagination } = usePagination({
    rowsPerPageOptions: rowsPerPage,
    initLimit: urlPagination.rows ?? rowsPerPage[0],
    initPage: urlPagination.page ?? 0
  });

  // update pagination whenever search result total pages change
  useEffect(() => {
    setPagination({ totalCount: operations?.totalCount ?? 0 });
  }, [setPagination, operations?.totalCount]);

  // whenever url state changes, update pagination
  useEffect(() => {
    setPagination({ ...urlPagination });
  }, [setPagination, urlPagination]);

  // whenever table pagination internally changes (user clicks next page etc)
  // update the row params
  useEffect(() => {
    setUrlPagination(pagination);
  }, [pagination, setUrlPagination]);

  // Request new search results whenever search or pagination changes
  useEffect(() => {
    let requestParams = initRequestParams(
      urlPagination,
      deserialize(urlState.query)
    );

    getOperations(requestParams);

    return () => {
      abort();
    };
  }, [getOperations, urlPagination, urlState.query, abort]);

  const setSearch = useCallback(
    (query) => {
      setUrlState({ query: serialize(query) });
    },
    [setUrlState]
  );

  // Invoked by Table on change to pagination
  const onPage = (params) => {
    setPagination(params);
    abort();
  };

  const content = (
    <SearchBar
      search={setSearch}
      query={deserialize(urlState.query)}
      loading={loading}
      placeholder="Search"
    >
      <JobTable
        jobs={operations?.operations}
        pagination={pagination}
        onPage={onPage}
        loading={loading || !dataReady}
      />
    </SearchBar>
  );

  return (
    <RootPaper>
      <Box paddingY={2}>{content}</Box>
    </RootPaper>
  );
}
