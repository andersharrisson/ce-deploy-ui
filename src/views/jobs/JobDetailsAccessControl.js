import React from "react";
import { RootPaper } from "@ess-ics/ce-ui-common";
import AccessControl from "../../components/auth/AccessControl";
import { JobDetailsContainer } from "./JobDetailsContainer";
import { useParams } from "react-router-dom";

export function JobDetailsAccessControl() {
  const { id } = useParams();

  return (
    <RootPaper>
      <AccessControl allowedRoles={[]}>
        <JobDetailsContainer id={id} />
      </AccessControl>
    </RootPaper>
  );
}
