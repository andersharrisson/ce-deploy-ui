import React, { useContext, useEffect, useMemo, useState } from "react";
import { UserPageView } from "./UserPageView";
import { LinearProgress } from "@mui/material";
import NotFoundView from "../../components/navigation/NotFoundView/NotFoundView";
import { useParams } from "react-router-dom";
import { userContext, useAPIMethod } from "@ess-ics/ce-ui-common";
import { apiContext } from "../../api/DeployApi";

function unpackUser(user) {
  if (user?.length > 0) {
    return { ...user[0] };
  } else {
    return {};
  }
}

export function UserDetailsContainer() {
  const { userName } = useParams();
  const { user } = useContext(userContext);
  const [error, setError] = useState(null);

  const client = useContext(apiContext);

  const params = useMemo(
    () => ({
      user_name: userName
    }),
    [userName]
  );

  const {
    value: userInfo,
    wrapper: getUserInfo,
    error: userInfoResponseError
  } = useAPIMethod({
    fcn: client.apis.Git.infoFromUserName,
    call: true,
    params,
    unpacker: unpackUser
  });

  useEffect(() => {
    if (userInfoResponseError) {
      const { status } = userInfoResponseError;
      if (status === 404) {
        setError({ message: "Page not found", status: `${status}` });
      }

      // user doesn't have permission to fetch userInfo
      if (status === 401) {
        setError({ message: "Unauthorized", status: `${status}` });
      }
    }
  }, [userInfoResponseError]);

  useEffect(() => {
    // user logs in => clear error message, and try to re-request userInfo
    if (user) {
      setError(null);
      getUserInfo();
    }
  }, [user, userName, getUserInfo]);

  // If there is an error, show it; highest priority
  if (error) {
    return (
      <NotFoundView
        status={error?.status ?? "404"}
        message={error?.message}
      />
    );
  }

  // If the user is logged in and the userInfo is available
  // Then show the user page
  if (user && userInfo) {
    return (
      <UserPageView
        userName={userName}
        user={user}
        userInfo={userInfo}
      />
    );
  }

  // Otherwise assume loading (smoothest experience / least flickering)
  return <LinearProgress color="primary" />;
}
