import React, { useContext, useEffect } from "react";
import { Grid } from "@mui/material";
import { GlobalAppBarContext } from "@ess-ics/ce-ui-common";
import { applicationTitle } from "../../components/common/Helper";
import { UserProfile } from "../../components/common/User/UserProfile";
import { UserIocList } from "../../components/common/User/UserIOCList";
import { UserOperationList } from "../../components/common/User/UserOperationList";

export function UserPageView({ userName, userInfo }) {
  const { setTitle } = useContext(GlobalAppBarContext);
  useEffect(() => setTitle(applicationTitle(userName)), [setTitle, userName]);

  return (
    <Grid
      container
      spacing={1}
    >
      <Grid
        item
        xs={12}
      >
        <UserProfile userInfo={userInfo} />
      </Grid>
      <Grid
        item
        xs={12}
      >
        <UserIocList userName={userName} />
      </Grid>
      <Grid
        item
        xs={12}
      >
        <UserOperationList userName={userName} />
      </Grid>
    </Grid>
  );
}
