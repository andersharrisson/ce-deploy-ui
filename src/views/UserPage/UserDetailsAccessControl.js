import React from "react";
import { RootPaper } from "@ess-ics/ce-ui-common";
import AccessControl from "../../components/auth/AccessControl";
import { UserDetailsContainer } from "./UserDetailsContainer";

export function UserDetailsAccessControl() {
  return (
    <RootPaper>
      <AccessControl allowedRoles={[]}>
        <UserDetailsContainer />
      </AccessControl>
    </RootPaper>
  );
}
