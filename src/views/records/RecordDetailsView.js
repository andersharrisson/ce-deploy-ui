import React, {
  useEffect,
  useCallback,
  useState,
  useContext,
  useMemo
} from "react";
import {
  IconButton,
  Typography,
  LinearProgress,
  Link as MuiLink
} from "@mui/material";
import { Link as ReactRouterLink } from "react-router-dom";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import {
  RootPaper,
  KeyValueTable,
  GlobalAppBarContext
} from "@ess-ics/ce-ui-common";
import { RecordBadge } from "../../components/records/RecordBadge";
import { applicationTitle, formatDate } from "../../components/common/Helper";
import { useParams } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { onFetchEntityError } from "../../components/common/Helper";
import NotFoundView from "../../components/navigation/NotFoundView/NotFoundView";

import { apiContext } from "../../api/DeployApi";
import { useAPIMethod } from "@ess-ics/ce-ui-common";

export function RecordDetailsView() {
  const { name } = useParams();
  const decodedName = decodeURIComponent(name);
  const [error, setError] = useState(null);

  const client = useContext(apiContext);

  const params = useMemo(
    () => ({
      name: decodedName
    }),
    [decodedName]
  );

  const {
    value: record,
    error: fetchError,
    loading: recordLoading,
    dataReady
  } = useAPIMethod({
    fcn: client.apis.Records.getRecord,
    params
  });

  useEffect(() => {
    if (fetchError) {
      onFetchEntityError(fetchError?.message, fetchError?.status, setError);
    }
  }, [fetchError]);

  const navigate = useNavigate();

  const { setTitle } = useContext(GlobalAppBarContext);

  useEffect(() => {
    if (record) {
      setTitle(applicationTitle(`Record Details: ${record.name}`));
    }
  }, [setTitle, record]);

  const handleClick = (event) => {
    navigate(-1);
  };

  const getSubset = useCallback((record) => {
    let subset = {
      IOC: record.iocId ? (
        <Typography>
          <MuiLink
            component={ReactRouterLink}
            to={`/iocs/${record.iocId}`}
            underline="hover"
          >
            {record?.iocName}
          </MuiLink>
        </Typography>
      ) : (
        record?.iocName
      ),
      Alias: record.alias ? (
        <Typography>
          <MuiLink
            component={ReactRouterLink}
            to={`/records/${record.alias}`}
            underline="hover"
          >
            {record?.alias}
          </MuiLink>
        </Typography>
      ) : (
        record?.alias
      ),
      Description: record?.description,
      Version: record?.iocVersion,
      "Record type": record.recordType,
      Host: record.hostCSentryId ? (
        <Typography>
          <MuiLink
            component={ReactRouterLink}
            to={`/hosts/${record.hostCSentryId}`}
            underline="hover"
          >
            {record?.hostName}
          </MuiLink>
        </Typography>
      ) : (
        record?.hostName
      )
    };

    for (const [key, value] of Object.entries(record.properties)) {
      if (key.toLowerCase().includes("time")) {
        subset["Last updated"] = formatDate(value);
      } else if (key.toLowerCase().includes("recordtype")) {
        subset["Record type"] = value;
      } else if (key.toLowerCase().includes("recorddesc")) {
        subset.Description = value;
      } else if (key.toLowerCase().includes("alias")) {
        subset["Record type"] = value;
      } else if (key.toLowerCase() === "iocversion") {
        subset["IOC Revision"] = value;
      } else if (key.toLowerCase() !== "iocid") {
        subset[key.replaceAll("_", " ")] = value;
      }
    }

    return subset;
  }, []);

  return (
    <RootPaper>
      {error ? (
        <NotFoundView />
      ) : recordLoading || !dataReady ? (
        <LinearProgress color="primary" />
      ) : (
        <>
          <IconButton
            color="inherit"
            onClick={handleClick}
            size="large"
          >
            <ArrowBackIcon />
          </IconButton>
          {record && (
            <>
              <RecordBadge
                record={record}
                sx={{ mt: 2.5 }}
              />
              <KeyValueTable
                obj={getSubset(record)}
                variant="overline"
                sx={{ border: 0 }}
                valueOptions={{ headerName: "" }}
              />
            </>
          )}
        </>
      )}
    </RootPaper>
  );
}
