import { RecordListView } from "./RecordListView";
import { RecordDetailsView } from "./RecordDetailsView";

export { RecordListView, RecordDetailsView };
export default RecordListView;
