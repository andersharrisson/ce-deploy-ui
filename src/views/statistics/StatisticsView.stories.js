import React from "react";
import { StatisticsView } from "./StatisticsView";
import { AppHarness } from "../../mocks/AppHarness";

export default {
  title: "Views/Statistics/StatisticsView",
  component: StatisticsView
};

const Template = (args) => (
  <AppHarness>
    <StatisticsView {...args} />
  </AppHarness>
);

export const Default = Template.bind({});
Default.args = {};
