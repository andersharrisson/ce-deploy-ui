import React, { useContext, useEffect } from "react";
import { styled } from "@mui/material/styles";
import { Grid, useTheme, Box } from "@mui/material";
import {
  GlobalAppBarContext,
  useAPIMethod,
  RootPaper
} from "@ess-ics/ce-ui-common";
import ActiveIOCChart from "../../components/statistics/ActiveIOCChart";
import DeploymentLineChart from "../../components/statistics/DeploymentLineChart";
import { HostStatistics } from "../../components/statistics/HostStatistics";
import { IOCStatistics } from "../../components/statistics/IOCStatistics";
import OperationChart from "../../components/statistics/OperationChart/OperationChart";
import { applicationTitle } from "../../components/common/Helper";
import { apiContext } from "../../api/DeployApi";

const PREFIX = "StatisticsView";

const classes = {
  box: `${PREFIX}-paper`
};

const StyledRootPaper = styled(RootPaper)(({ theme }) => ({
  padding: theme.spacing(0),
  [`& .${classes.box}`]: {
    padding: theme.spacing(4)
  }
}));

export function StatisticsView() {
  const { setTitle } = useContext(GlobalAppBarContext);
  useEffect(() => setTitle(applicationTitle("Statistics")), [setTitle]);
  const client = useContext(apiContext);

  const theme = useTheme();

  const {
    value: iocsOverTime,
    wrapper: getIocsOverTime,
    abort: abortGetIocsOverTime
  } = useAPIMethod({
    fcn: client.apis.Statistics.activeIocHistory,
    call: false
  });

  const {
    value: operationStatistics,
    wrapper: getOperationStatistics,
    abort: abortGetOperationStatistics
  } = useAPIMethod({
    fcn: client.apis.Statistics.operationHistory,
    call: false
  });

  return (
    <StyledRootPaper>
      <Grid
        container
        spacing={theme.spacing(2)}
      >
        <Grid
          item
          xs={6}
        >
          <IOCStatistics />
        </Grid>

        <Grid
          item
          xs={6}
        >
          <HostStatistics />
        </Grid>

        <Grid
          item
          xs={12}
        >
          <Box className={classes.box}>
            <OperationChart
              title="Operations over time"
              iocDeployments={operationStatistics}
              getIOCDeployments={getOperationStatistics}
              abortGetIOCDeployments={abortGetOperationStatistics}
            />
          </Box>
        </Grid>
        <Grid
          item
          xs={12}
        >
          <Box className={classes.box}>
            <ActiveIOCChart title="Number of IOCs detected per network" />
          </Box>
        </Grid>
        <Grid
          item
          xs={12}
        >
          <Box className={classes.box}>
            <DeploymentLineChart
              title="Number of IOCs detected over time"
              chartLabel="Active IOCs over time"
              iocDeployments={iocsOverTime}
              getIOCDeployments={getIocsOverTime}
              abortGetIOCDeployments={abortGetIocsOverTime}
            />
          </Box>
        </Grid>
      </Grid>
    </StyledRootPaper>
  );
}
