import React, { useContext } from "react";
import { DeployAPIProvider } from "./DeployApi";
import { UserProvider } from "./UserProvider";
import { userContext } from "@ess-ics/ce-ui-common";
import { SnackbarProvider } from "notistack";

function AppHarness({ children }) {
  return (
    <SnackbarProvider
      preventDuplicate
      maxSnack="5"
    >
      <DeployAPIProvider>{children}</DeployAPIProvider>
    </SnackbarProvider>
  );
}

function mountIntoHarness(children) {
  cy.mount(<AppHarness>{children}</AppHarness>);
}

function DisplayUserContextValue() {
  const contextValue = useContext(userContext);

  return <pre id="display">{JSON.stringify(contextValue, null, 2)}</pre>;
}

describe("UserProvider", () => {
  context("when logged in", () => {
    beforeEach(() => {
      cy.login();
    });

    it("provides the user", () => {
      mountIntoHarness(
        <UserProvider>
          <DisplayUserContextValue />
        </UserProvider>
      );

      cy.wait("@getUserRoles");
      cy.wait("@infoFromUserName");
      cy.get("#display").contains("John Smith").contains("DeploymentToolAdmin");

      cy.get("@infoFromUserName.all").should("have.length", 1);
    });
  });
});
