const jobMessages = {
  queued: "job is queued on the server",
  running: "job is running",
  failed: "job has failed",
  successful: "job was successful"
};
const typeMap = {
  DEPLOY: "Deployment",
  UNDEPLOY: "Undeployment",
  START: "Start command",
  STOP: "Stop command"
};
export class AWXJobDetails {
  constructor(operationType, job) {
    this.operationType = operationType;
    this.job = job;
  }
  isRunning() {
    return !this.job?.finished;
  }
  isFinished() {
    return !this.isRunning();
  }
  wasSuccessful() {
    return this.job?.status.toLowerCase() === "successful";
  }
  typeLabel() {
    return typeMap[this.operationType];
  }
  status() {
    return this.job?.status.toLowerCase();
  }
  message() {
    const stem = this.job ? `${this.typeLabel()} ` : "";
    const info = this.job
      ? jobMessages[this.job.status.toLowerCase()]
      : "Fetching data";
    const message = stem + info;
    return message;
  }
  severity() {
    const status = this.job?.status.toLowerCase();
    if (status === "failed") {
      return "error";
    } else if (status === "successful") {
      return "success";
    } else {
      return "info";
    }
  }
}
export class DeploymentInfoDetails {
  constructor(deployment) {
    this.deployment = deployment;
  }
  type() {
    if (this.deployment) {
      return this.deployment.undeployment ? "undeployment" : "deployment";
    } else {
      return null;
    }
  }
}
export class DeploymentStatus {
  constructor(deployment, deploymentJob) {
    this.deployment = deployment;
    this.deploymentJob = deploymentJob;
    this.deploymentHelper = new DeploymentInfoDetails(deployment);
    this.jobHelper = new AWXJobDetails(
      deployment?.undeployment ? "UNDEPLOY" : "DEPLOY",
      deploymentJob
    );
  }
  isFinished() {
    return this.jobHelper.isFinished();
  }
  wasSuccessful() {
    return this.jobHelper.wasSuccessful();
  }
  status() {
    return this.deploymentJob.status.toLowerCase();
  }
  message() {
    const stem = `The ${this.deploymentHelper.type()} `;
    const info = this.deploymentJob
      ? jobMessages[this.deploymentJob.status.toLowerCase()]
      : ": fetching data";
    const message = stem + info;
    return message;
  }
  severity() {
    const status = this.deploymentJob?.status.toLowerCase();
    if (status === "failed") {
      return "error";
    } else if (status === "successful") {
      return "success";
    } else {
      return "info";
    }
  }
}
export class AWXCommandDetails {
  constructor(job) {
    this.job = job;
  }
  notFinished() {
    return !this.isFinished();
  }
  isFinished() {
    return ["failed", "successful"].includes(this.job?.status);
  }
  wasSuccessful() {
    return this.job?.status === "successful";
  }
  wasFailed() {
    return this.job?.status === "failed";
  }
  isRunning() {
    return this.job?.status === "running";
  }
}
export class AdHocCommand {
  constructor(command) {
    this.command = command;
  }
  isRunning() {
    return !this.isFinished();
  }
  isFinished() {
    return ["failed", "successful"].includes(
      this.command?.status.toLowerCase()
    );
  }
  wasSuccessful() {
    return this.job?.status.toLowerCase() === "successful";
  }
  message() {
    const message = `The ${this.command.type} command ${
      jobMessages[this.command?.status.toLowerCase()]
    }`;
    return message;
  }
}
export class CommandStatus {
  constructor(command, commandJob) {
    this.command = command;
    this.job = commandJob;
    this.commandHelper = new AdHocCommand(command);
    this.jobHelper = new AWXCommandDetails(commandJob);
  }
  message() {
    const message = `The ${this.command.type} command ${
      jobMessages[this.command.status.toLowerCase()]
    }`;
    return message;
  }
}
export class IocActiveDeployment {
  constructor(activeDeployment) {
    this.activeDeployment = activeDeployment;
  }
  failedDeployment() {
    return (
      this.activeDeployment &&
      "successful".localeCompare(this.activeDeployment?.jobStatus, undefined, {
        sensitivity: "base"
      }) !== 0
    );
  }
  calculateNotDeployedText(deployedText) {
    if (this.activeDeployment) {
      return deployedText;
    }
    return "---";
  }
  hasDeployment() {
    return (
      this.activeDeployment !== null && this.activeDeployment !== "undefined"
    );
  }
}
export class Operation {
  constructor(operation) {
    this.operation = operation;
  }
  isFinished() {
    return ["failed", "successful"].includes(
      this.operation?.status?.toLowerCase()
    );
  }
  wasSuccessful() {
    return this.operation?.status?.toLowerCase() === "successful";
  }
  hasFailed() {
    return this.operation?.status?.toLowerCase() === "failed";
  }
  hasFinished() {
    return ["failed", "successful"].includes(
      this.operation?.status?.toLowerCase()
    );
  }
}
