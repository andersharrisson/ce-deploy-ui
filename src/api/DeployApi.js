/**
 * DeployAPIProvider
 * Provide CE Deploy Backend API
 */
import React from "react";
import { createContext } from "react";
import { APIProvider } from "@ess-ics/ce-ui-common";
import { node, arrayOf, oneOfType } from "prop-types";

const propTypes = {
  /** Elements used as children */
  children: oneOfType([
    /** Array of elements. */
    arrayOf(node),
    /** One or more elements without array. */
    node
  ])
};

export const apiContext = createContext(null);

const apiOptions = {
  url: `${window.API_BASE_ENDPOINT}`,
  server: `${window.SERVER_ADDRESS}`,
  // Workaround for https://github.com/swagger-api/swagger-js/issues/1022
  // Empty body POST requests are sent with Content-Type text/plaintext
  // instead of no content type, or application/json
  requestInterceptor: (req) => {
    req.headers["Content-Type"] = "application/json";
    req.headers.accept = "application/json";
    window.req = req;
    return req;
  },
  responseInterceptor: (res) => {
    return res;
  }
};

export function DeployAPIProvider({ children }) {
  return (
    <APIProvider
      context={apiContext}
      options={apiOptions}
    >
      {children}
    </APIProvider>
  );
}

DeployAPIProvider.propTypes = propTypes;
