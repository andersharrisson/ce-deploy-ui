import React from "react";
import { useCallback, useEffect, useState, useContext } from "react";
import { userContext, useAPIMethod } from "@ess-ics/ce-ui-common";
import { apiContext } from "./DeployApi";

function loginRequest(username, password) {
  return {
    userName: username, // fyi on template it is username, but on deploy it is userName
    password: password
  };
}

function unpackUser(user) {
  if (user?.length > 0) {
    return { ...user[0] };
  } else {
    return {};
  }
}

export function UserProvider({ children }) {
  const [initialized, setInitialized] = useState(false);
  const [user, setUser] = useState(null);
  const [userRoles, setUserRoles] = useState([]);
  const [loginErrorMsg, setLoginErrorMsg] = useState();

  const client = useContext(apiContext);
  const {
    error: loginError,
    wrapper: callLoginAPI,
    value: loginResponse,
    loading: loginLoading
  } = useAPIMethod({ fcn: client.apis.Authentication.login, call: false });
  const {
    wrapper: callUserAPI,
    value: userResponse,
    loading: userLoading
  } = useAPIMethod({
    fcn: client.apis.Git.infoFromUserName,
    call: true,
    unpacker: unpackUser
  });
  const { wrapper: callLogoutAPI, loading: logoutLoading } = useAPIMethod({
    fcn: client.apis.Authentication.logout,
    call: false
  });
  const {
    wrapper: callUserRolesAPI,
    value: userRolesResponse,
    loading: userRolesLoading
  } = useAPIMethod({
    fcn: client.apis.Authentication.getUserRoles,
    call: true
  });

  const login = useCallback(
    (username, password) => {
      callLoginAPI({}, { requestBody: loginRequest(username, password) });
    },
    [callLoginAPI]
  );

  const logout = useCallback(() => {
    callLogoutAPI({}, {});
    setUser(null);
  }, [callLogoutAPI]);

  useEffect(() => {
    if (loginResponse) {
      callUserAPI();
      callUserRolesAPI();
    }
  }, [loginResponse, callUserAPI, callUserRolesAPI]);

  useEffect(() => {
    setUser(userResponse);
  }, [userResponse, setUser]);

  useEffect(() => {
    setUserRoles(userRolesResponse);
  }, [userRolesResponse, setUserRoles]);

  const loading = Boolean(
    loginLoading || userLoading || userRolesLoading || logoutLoading
  );

  useEffect(() => {
    setLoginErrorMsg(loginError?.response?.body?.description);
  }, [loginError]);

  const resetLoginError = useCallback(
    () => setLoginErrorMsg(null),
    [setLoginErrorMsg]
  );

  const createValue = useCallback(() => {
    return {
      user: user,
      userRoles: userRoles,
      login,
      loginError: loginErrorMsg,
      logout,
      resetLoginError
    };
  }, [user, userRoles, login, loginErrorMsg, logout, resetLoginError]);

  const [value, setValue] = useState(createValue());

  useEffect(() => {
    if (!loading) {
      setInitialized(true);
    }
  }, [loading]);

  useEffect(() => {
    if (!loading) {
      setValue(createValue());
    }
  }, [loading, setValue, createValue]);

  return (
    initialized && (
      <userContext.Provider value={value}>{children}</userContext.Provider>
    )
  );
}
