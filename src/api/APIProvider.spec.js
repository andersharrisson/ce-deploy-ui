import React, { useContext } from "react";
import { DeployAPIProvider, apiContext } from "./DeployApi";
import { SnackbarProvider } from "notistack";

function AppHarness({ children }) {
  return (
    <SnackbarProvider
      preventDuplicate
      maxSnack="5"
    >
      {children}
    </SnackbarProvider>
  );
}

function mountIntoHarness(children) {
  cy.mount(<AppHarness>{children}</AppHarness>);
}

function checkAPIDisplayed() {
  cy.get("#api").contains("CE deploy & monitor backend");
}

function DisplayAPISpecification({ api }) {
  return <pre id="api">{JSON.stringify(api, null, 2)}</pre>;
}

context("API", () => {
  describe("ApiProvider", () => {
    it("makes the API available via context", () => {
      const Glue = () => {
        const api = useContext(apiContext);
        return <DisplayAPISpecification api={api} />;
      };
      mountIntoHarness(
        <DeployAPIProvider>
          <Glue />
        </DeployAPIProvider>
      );
      checkAPIDisplayed();
    });
  });
});
