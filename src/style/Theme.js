import { createTheme } from "@mui/material/styles";
import { theme as ceuiTheme } from "@ess-ics/ce-ui-common";

// Contrasting colors used for charts and graphs
const chartColors = {
  blue: "rgba(66, 165, 245, 1)",
  orange: "rgba(245, 132, 66, 1)",
  lime: "rgba(245, 221, 66, 1)",
  purple: "rgba(194, 66, 245, 1)",
  green: "rgba(100, 242, 75, 1)",
  salmon: "rgba(242, 75, 100, 1)",
  teal: "rgba(75, 242, 192, 1)",
  gray: "rgba(161, 168, 167, 1)",
  black: "rgba(37, 38, 38, 1)",
  pink: "rgba(242, 162, 172, 1)",
  lightgreen: "rgba(172, 242, 162, 1)",
  seafoam: "rgba(162, 242, 222, 1)"
};

// Create from common theme
let theme = createTheme(ceuiTheme, {});

// add customizations
theme = createTheme(theme, {
  components: {
    // TODO: Move to common theme?
    MuiAlert: {
      styleOverrides: {
        standardInfo: {
          backgroundColor: theme.palette.grey[200]
        }
      }
    }
  },
  palette: {
    // TODO: Add secondary to common theme?
    secondary: {
      main: theme.palette.essNavy.main
    },
    status: {
      ...theme.palette.status,
      icons: theme.palette.grey[800]
    },
    graph: chartColors
  }
});

export { theme };
