import React from "react";
import { createRoot } from "react-dom/client";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { LicenseInfo } from "@mui/x-license-pro";

if (
  process.env.NODE_ENV === "development" &&
  process.env.REACT_APP_MOCK_CCCE_API === "YES"
) {
  const { worker } = require("./mocks/browser");
  worker.start();
}

if (
  process.env.NODE_ENV === "development" &&
  process.env.REACT_APP_MOCK_CCCE_API === "YES"
) {
  const { worker } = require("./mocks/browser");
  worker.start();
}

// Set license key
LicenseInfo.setLicenseKey(process.env.REACT_APP_MUI_PRO_LICENSE_KEY);

const container = document.getElementById("root");
const root = createRoot(container);

root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
