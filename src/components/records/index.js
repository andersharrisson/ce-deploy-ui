import {
  RecordTable,
  createRecordsRow,
  createIocDetailsRow
} from "./RecordTable";
import { RecordBadge } from "./RecordBadge";
import { RecordStatusIcon } from "./RecordIcons";
import { RecordSearch } from "./RecordSearch";

export {
  RecordTable,
  createRecordsRow,
  createIocDetailsRow,
  RecordBadge,
  RecordStatusIcon,
  RecordSearch
};
export default RecordTable;
