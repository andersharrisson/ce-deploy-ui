import React from "react";
import { Table } from "@ess-ics/ce-ui-common";
import { RecordStatusIcon } from "./RecordIcons";
import { Grid, Tooltip, Typography, Link } from "@mui/material";
import { EllipsisTextLink } from "../common/Helper";

const recordsColumns = [
  {
    field: "bulb",
    headerName: "Status",
    flex: 0,
    headerAlign: "center",
    align: "center"
  },
  { field: "name", headerName: "Record", width: "15ch", sortable: false },
  {
    field: "description",
    headerName: "Description",
    width: "20ch",
    sortable: false
  },
  { field: "iocName", headerName: "IOC name", width: "10ch", sortable: false },
  { field: "hostName", headerName: "Host", width: "10ch", sortable: false }
];

const iocDetailsColumns = [
  {
    field: "bulb",
    headerName: "Status",
    flex: 0,
    headerAlign: "center",
    align: "center"
  },
  { field: "name", headerName: "Record", width: "15ch", sortable: false },
  {
    field: "description",
    headerName: "Description",
    width: "20ch",
    sortable: false
  },
  { field: "iocVersion", headerName: "Version", width: "15ch", sortable: false }
];

function createRecordName(record) {
  return (
    <EllipsisTextLink
      href={`/records/${encodeURIComponent(record.name)}`}
      text={record.name}
      TooltipProps={{
        PopperProps: {
          modifiers: [
            {
              name: "offset",
              options: {
                offset: [100, 0]
              }
            }
          ]
        }
      }}
    />
  );
}

function createRecordDescription(description) {
  return (
    <>
      {description ? (
        <Tooltip
          title={description}
          arrow
          enterDelay={400}
        >
          <Typography
            sx={{
              overflow: "hidden",
              textOverflow: "ellipsis",
              whiteSpace: "nowrap"
            }}
          >
            {description}
          </Typography>
        </Tooltip>
      ) : (
        "---"
      )}
    </>
  );
}

function createIocLink(record) {
  // IOC ID only exists if it has been created in the DB -> avoid linking if it hasn't been created
  if (record.iocId) {
    return <Link href={`/iocs/${record.iocId}`}>{record.iocName}</Link>;
  }

  return record.iocName;
}

function createHostLink(record) {
  // create host link only if the host exists in CSEntry
  if (record.hostCSentryId) {
    return (
      <Link href={`/hosts/${record.hostCSentryId}`}>{record.hostName}</Link>
    );
  }

  return record.hostName;
}

export function createRecordsRow(record) {
  return {
    id: record.name,
    bulb: (
      <Grid
        container
        direction="column"
        justifyContent="center"
        alignItems="center"
      >
        <RecordStatusIcon record={record} />
      </Grid>
    ),
    name: createRecordName(record),
    description: createRecordDescription(record.description),
    iocName: createIocLink(record),
    hostName: createHostLink(record)
  };
}

export function createIocDetailsRow(record) {
  return {
    id: record.name,
    bulb: (
      <Grid
        container
        direction="column"
        justifyContent="center"
        alignItems="center"
      >
        <RecordStatusIcon record={record} />
      </Grid>
    ),
    description: createRecordDescription(record.description),
    name: createRecordName(record),
    iocVersion: record.iocVersion
  };
}

export function RecordTable({
  records,
  rowType = "records",
  loading,
  pagination,
  onPage
}) {
  const tableTypeSpecifics = {
    records: [recordsColumns, createRecordsRow],
    iocDetails: [iocDetailsColumns, createIocDetailsRow]
  };

  const [columns, createRow] = tableTypeSpecifics[rowType];
  const rows = (records?.recordList ?? []).map((record) => createRow(record));

  return (
    <Table
      columns={columns}
      rows={rows}
      pagination={pagination}
      onPage={onPage}
      loading={loading}
      disableHover
    />
  );
}
