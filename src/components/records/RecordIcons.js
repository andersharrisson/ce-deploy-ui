import React from "react";
import { useTheme } from "@mui/material";
import LabeledIcon from "../common/LabeledIcon";
import {
  Brightness1,
  RadioButtonUnchecked,
  HelpOutline
} from "@mui/icons-material";

export function RecordStatusIcon({ record }) {
  const theme = useTheme();

  let { pvStatus } = record;

  const iconConfig = {
    active: {
      title: "Active",
      icon: Brightness1
    },
    inactive: {
      title: "Inactive",
      icon: RadioButtonUnchecked
    },
    null: {
      title: "Unknown",
      icon: HelpOutline
    }
  };

  let state = pvStatus ? pvStatus.toLowerCase() : null;
  const iconStyle = { fill: theme.palette.status.icons };
  const iconTitle = iconConfig[state].title;
  const statusIcon = iconConfig[state].icon;

  return (
    <LabeledIcon
      label={iconTitle}
      labelPosition="tooltip"
      Icon={statusIcon}
      IconProps={{ style: iconStyle }}
    />
  );
}
