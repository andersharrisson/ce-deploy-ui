import React from "react";
import { IconBadge } from "../common/Badge/IconBadge";
import { RecordStatusIcon } from "./RecordIcons";

export function RecordBadge({ record, ...rest }) {
  return (
    <IconBadge
      icon={<RecordStatusIcon record={record} />}
      title={record.name}
      subheader={record.iocName}
      {...rest}
    />
  );
}
