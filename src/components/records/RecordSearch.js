import React, {
  useEffect,
  useCallback,
  useState,
  useContext,
  useMemo
} from "react";
import { initRequestParams } from "../common/Helper";
import { RecordTable } from "./RecordTable";
import { SearchBar } from "../common/SearchBar/SearchBar";
import useUrlState from "@ahooksjs/use-url-state";
import { serialize, deserialize } from "../common/URLState/URLState";
import { Grid, Tabs, Tab } from "@mui/material";
import { useAPIMethod, usePagination } from "@ess-ics/ce-ui-common";
import { apiContext } from "../../api/DeployApi";

export function RecordSearch({ iocName, rowType }) {
  const client = useContext(apiContext);

  const {
    value: records,
    wrapper: getRecords,
    loading,
    dataReady,
    abort
  } = useAPIMethod({
    fcn: client.apis.Records.findAllRecords,
    call: false
  });

  const [urlState, setUrlState] = useUrlState(
    {
      record_tab: "0",
      rows: "20",
      page: "0",
      query: ""
    },
    { navigateMode: "replace" }
  );
  const [recordFilter, setRecordFilter] = useState();

  const handleTabChange = useCallback(
    (event, tab) => {
      setUrlState((s) =>
        serialize(s.record_tab) === serialize(tab)
          ? { record_tab: serialize(tab) }
          : { record_tab: serialize(tab), page: "0" }
      );

      changeTab(tab);
    },
    [setUrlState]
  );

  useEffect(() => {
    urlState.record_tab && changeTab(deserialize(urlState.record_tab));
  }, [urlState]);

  const changeTab = (tab) => {
    if (tab === 0) {
      setRecordFilter(null);
    } else if (tab === 1) {
      setRecordFilter("ACTIVE");
    } else if (tab === 2) {
      setRecordFilter("INACTIVE");
    }
  };

  const urlPagination = useMemo(() => {
    return {
      rows: deserialize(urlState.rows),
      page: deserialize(urlState.page)
    };
  }, [urlState.rows, urlState.page]);

  const setUrlPagination = useCallback(
    ({ rows, page }) => {
      setUrlState({ rows: serialize(rows), page: serialize(page) });
    },
    [setUrlState]
  );

  const rowsPerPage = [20, 50];

  const { pagination, setPagination, setTotalCount } = usePagination({
    rowsPerPageOptions: rowsPerPage,
    initLimit: urlPagination.rows ?? rowsPerPage[0],
    initPage: urlPagination.page ?? 0
  });

  // update pagination whenever search result total pages change
  useEffect(() => {
    setTotalCount((prev) => {
      if (
        prev === pagination.totalCount &&
        prev > 0 &&
        prev === records?.totalCount
      ) {
        return prev;
      } else {
        return records?.totalCount ?? 0;
      }
    });
  }, [pagination, records?.totalCount, setTotalCount]);

  // whenever url state changes, update pagination
  useEffect(() => {
    setPagination({ ...urlPagination });
  }, [setPagination, urlPagination]);

  // whenever table pagination internally changes (user clicks next page etc)
  // update the row params
  useEffect(() => {
    setUrlPagination(pagination);
  }, [pagination, setUrlPagination]);

  // Request new search results whenever search or pagination changes
  useEffect(() => {
    let requestParams = initRequestParams(pagination);
    requestParams.pv_status = recordFilter;
    requestParams.record_name = deserialize(urlState.query);
    requestParams.ioc_name = iocName;
    getRecords(requestParams);

    return () => {
      abort();
    };
  }, [getRecords, recordFilter, urlState.query, pagination, abort, iocName]);

  // Callback for searchbar, called whenever user updates search
  const setSearch = useCallback(
    (query) => {
      setUrlState({ query: serialize(query) });
    },
    [setUrlState]
  );

  // Invoked by Table on change to pagination
  const onPage = (params) => {
    setPagination(params);
    abort();
  };

  return (
    <Grid
      container
      spacing={1}
      flexDirection={"column"}
    >
      <Grid item>
        <Tabs
          value={deserialize(urlState.record_tab)}
          onChange={handleTabChange}
        >
          <Tab label="All" />
          <Tab label="Only active" />
          <Tab label="Only inactive" />
        </Tabs>
      </Grid>
      <Grid item>
        <SearchBar
          search={setSearch}
          query={deserialize(urlState.query)}
          loading={loading || !dataReady}
          placeholder="Search"
        >
          <RecordTable
            records={records}
            loading={loading || !dataReady}
            rowType={rowType}
            pagination={pagination}
            onPage={onPage}
          />
        </SearchBar>
      </Grid>
    </Grid>
  );
}
