import React, {
  useCallback,
  useRef,
  useEffect,
  useContext,
  useMemo
} from "react";
import { LinearProgress, Stack } from "@mui/material";
import { Console } from "../common/Console/Console";
import { useSafePolling } from "../../hooks/Polling";
import { apiContext } from "../../api/DeployApi";
import { useAPIMethod } from "@ess-ics/ce-ui-common";

const LOG_POLL_INTERVAL = 5000;
export function DeploymentJobOutput({ deploymentJob }) {
  const client = useContext(apiContext);

  const params = useMemo(
    () => ({
      awx_job_id: deploymentJob?.id
    }),
    [deploymentJob]
  );

  const {
    value: log,
    wrapper: getLogById,
    loading: logLoading,
    dataReady: logDataReady,
    abort: abortGetLogById
  } = useAPIMethod({
    fcn: client.apis.Deployments.fetchDeploymentJobLog,
    params
  });

  const finalResultsNeeded = useRef(true);

  useEffect(() => {
    finalResultsNeeded.current = true;
  }, [deploymentJob.id]);

  const getLog = useCallback(() => {
    if (!deploymentJob.finished || finalResultsNeeded.current) {
      getLogById(deploymentJob.id);
      finalResultsNeeded.current = !deploymentJob.finished;
    }
  }, [deploymentJob.finished, deploymentJob.id, getLogById]);
  useSafePolling(
    getLog,
    logLoading || !logDataReady,
    LOG_POLL_INTERVAL,
    true,
    abortGetLogById
  );

  const dataReady = useCallback(() => {
    return deploymentJob?.started;
  }, [deploymentJob?.started]);

  return (
    <Stack>
      {log ? (
        <div
          style={{
            width: "100%"
          }}
        >
          <Console
            html={log.stdoutHtml}
            dataReady={dataReady}
            title="AWX job details"
          />
        </div>
      ) : (
        <div style={{ width: "100%" }}>
          <LinearProgress color="primary" />
        </div>
      )}
    </Stack>
  );
}
