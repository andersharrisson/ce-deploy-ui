import React from "react";
import {
  CheckCircleOutline,
  ErrorOutline,
  RotateRightOutlined,
  QueueOutlined,
  AddCircleOutline,
  RemoveCircleOutline
} from "@mui/icons-material";
import { theme } from "../../style/Theme";
import { LabeledIcon } from "../common/LabeledIcon";

export function DeploymentStatusIcon({ status }) {
  const deploymentStatusIcons = {
    successful: {
      title: "Successful",
      icon: CheckCircleOutline
    },
    failed: {
      title: "Failed",
      icon: ErrorOutline
    },
    running: {
      title: "Running",
      icon: RotateRightOutlined
    },
    queued: {
      title: "Queued",
      icon: QueueOutlined
    }
  };

  const deploymentStatusColors = {
    successful: theme.palette.status.ok,
    failed: theme.palette.status.fail,
    running: theme.palette.status.progress,
    queued: theme.palette.status.progress
  };

  const state = status.toLowerCase();
  const iconStyle = { fill: deploymentStatusColors[state] };
  const iconTitle = deploymentStatusIcons[state].title;
  const statusIcon = deploymentStatusIcons[state].icon;

  return (
    <LabeledIcon
      label={iconTitle}
      labelPosition="tooltip"
      Icon={statusIcon}
      IconProps={{ style: iconStyle }}
    />
  );
}

export function DeploymentTypeIcon({ type, labelPosition = "tooltip" }) {
  const deploymentTypeIcons = {
    deploy: {
      title: "Deployment",
      icon: AddCircleOutline
    },
    undeploy: {
      title: "Undeployment",
      icon: RemoveCircleOutline
    }
  };

  const iconStyle = { fill: theme.palette.status.icons };
  const iconTitle = deploymentTypeIcons[type.toLowerCase()].title;
  const StatusIcon = deploymentTypeIcons[type.toLowerCase()].icon;

  return (
    <LabeledIcon
      label={iconTitle}
      LabelProps={{ noWrap: true }}
      labelPosition={labelPosition}
      Icon={StatusIcon}
      IconProps={{ style: { iconStyle } }}
    />
  );
}
