import React from "react";
import { List, ListItem, Paper } from "@mui/material";
import { Link } from "react-router-dom";
import { DeploymentBadge } from "./DeploymentBadge";

export function DeploymentListItem({ deployment }) {
  return (
    <Paper>
      <ListItem
        button
        component={Link}
        to={deployment.url}
      >
        <DeploymentBadge deployment={deployment} />
      </ListItem>
    </Paper>
  );
}

export function DeploymentsList({ deployments }) {
  return (
    <List>
      {deployments &&
        deployments.map((deployment) => {
          deployment.url = `/jobs/${deployment.operationId}`;
          return (
            <DeploymentListItem
              key={deployment.id}
              deployment={deployment}
            />
          );
        })}
    </List>
  );
}
