import React from "react";
import { IconBadge } from "../common/Badge/IconBadge";
import { DeploymentTypeIcon } from "./DeploymentIcons";
import { Link as MuiLink } from "@mui/material";
import { Link as ReactRouterLink } from "react-router-dom";

export function DeploymentBadge({ deployment }) {
  const linkToIoc = (
    <MuiLink
      component={ReactRouterLink}
      to={`/iocs/${deployment.iocId}`}
      underline="hover"
    >
      {deployment.namingName}
    </MuiLink>
  );

  return (
    <IconBadge
      icon={
        <>
          <DeploymentTypeIcon
            type={deployment.undeployment ? "undeploy" : "deploy"}
          />
        </>
      }
      title={linkToIoc}
      subheader={deployment.host.fqdn || "---"}
    />
  );
}
