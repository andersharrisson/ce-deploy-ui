import React from "react";
import { IconBadge } from "../common/Badge/IconBadge";
import { JobTypeIcon } from "./JobIcons";
import { Link as MuiLink } from "@mui/material";
import { Link as ReactRouterLink } from "react-router-dom";

export function JobBadge({ operation }) {
  const linkToIoc = (
    <MuiLink
      component={ReactRouterLink}
      to={`/iocs/${operation.iocId}`}
      underline="hover"
    >
      {operation.iocName}
    </MuiLink>
  );

  return (
    <IconBadge
      icon={
        <>
          <JobTypeIcon
            type={operation.type}
            colorStyle="black"
          />
        </>
      }
      title={linkToIoc}
      subheader={operation.host.fqdn || "---"}
    />
  );
}
