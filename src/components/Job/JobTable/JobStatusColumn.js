import React from "react";
import EventIcon from "@mui/icons-material/Event";
import { Stack, Tooltip } from "@mui/material";
import { formatDate, timeAgo } from "../../common/Helper";
import LabeledIcon from "../../common/LabeledIcon";
import { JobStatusIcon } from "../JobStatus";
import { JobDuration } from "../JobDuration";

export const JobStatusColumn = ({ job }) => {
  const createOrStartDate = new Date(job?.startTime ?? job.createdAt);
  const formattedCreateOrStartDate = `${
    job?.startTime ? "Started" : "Created"
  } ${timeAgo.format(new Date(createOrStartDate))}`;

  return (
    <Stack>
      <Stack gap={0.5}>
        <JobStatusIcon job={job} />
        <Stack>
          <Tooltip
            title={`${formatDate(createOrStartDate)}`}
            aria-label={`${formattedCreateOrStartDate}, on ${formatDate(
              createOrStartDate
            )}`}
          >
            <LabeledIcon
              label={formattedCreateOrStartDate}
              LabelProps={{ variant: "body2" }}
              labelPosition="right"
              Icon={EventIcon}
              IconProps={{ fontSize: "small" }}
            />
          </Tooltip>
          {job?.finishedAt ? <JobDuration job={job} /> : null}
        </Stack>
      </Stack>
    </Stack>
  );
};
