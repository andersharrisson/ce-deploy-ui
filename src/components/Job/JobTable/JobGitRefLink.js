import React, { useContext, useMemo } from "react";
import GitRefLink from "../../IOC/GitRefLink/GitRefLink";
import { apiContext } from "../../../api/DeployApi";
import { useAPIMethod } from "@ess-ics/ce-ui-common";
import { Skeleton } from "@mui/material";

export const JobGitRefLink = ({ job }) => {
  const client = useContext(apiContext);
  const projectParams = useMemo(
    () => ({
      project_id: job?.gitProjectId
    }),
    [job]
  );

  const { value: project } = useAPIMethod({
    fcn: client.apis.Git.gitProjectDetails,
    params: projectParams
  });

  const shortRefParams = useMemo(
    () => ({
      project_id: job?.gitProjectId,
      reference: job?.gitReference
    }),
    [job]
  );
  const { value: commits } = useAPIMethod({
    fcn: client.apis.Git.listTagsAndCommitIds,
    params: shortRefParams
  });

  if (!project || !commits) {
    return <Skeleton width={100} />;
  }

  const gitRef = commits?.at(0)?.shortReference ?? job.gitReference;

  return (
    <GitRefLink
      url={project?.projectUrl}
      revision={gitRef}
      renderLinkContents={(revision) => revision}
      LinkProps={{
        "aria-label": `External Git Link, project ${project.name}, revision ${gitRef}`
      }}
    />
  );
};
