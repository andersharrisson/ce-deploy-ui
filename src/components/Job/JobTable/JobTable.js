import React from "react";
import { Table } from "@ess-ics/ce-ui-common";
import { JobStatusColumn } from "./JobStatusColumn";
import { JobSummaryColumn } from "./JobSummaryColumn";
import { UserAvatar } from "../../common/User/UserAvatar";
import { JobHostColumn } from "./JobHostColumn";

const jobLogColumns = [
  {
    field: "status",
    headerName: "Status"
  },
  {
    field: "job",
    headerName: "Job"
  },
  {
    field: "host",
    headerName: "Host"
  },
  {
    field: "user",
    headerName: "User",
    align: "center",
    headerAlign: "center"
  }
];

const userPageJobLogColumns = [
  ...jobLogColumns.filter((it) => it.field !== "user")
];

function createTableRow(job, colorStyle) {
  return {
    id: job.id,
    status: <JobStatusColumn {...{ job }} />,
    job: <JobSummaryColumn {...{ job, colorStyle }} />,
    host: <JobHostColumn {...{ job }} />,
    user: <UserAvatar username={job.createdBy} />
  };
}

function createTableRowJobLog(job) {
  return createTableRow(job, "black");
}

export function JobTable({
  jobs,
  rowType = "jobLog",
  pagination,
  onPage,
  loading
}) {
  const tableTypeSpecifics = {
    jobLog: [jobLogColumns, createTableRowJobLog],
    userPageJobLog: [userPageJobLogColumns, createTableRowJobLog]
  };

  const [columns, createRow] = tableTypeSpecifics[rowType];

  return (
    <Table
      columns={columns}
      rows={jobs?.map((job) => createRow(job))}
      pagination={pagination}
      onPage={onPage}
      loading={loading}
      rowHeight={90}
    />
  );
}
