import { Chip, Link, Stack } from "@mui/material";
import React from "react";
import CommitIcon from "@mui/icons-material/Commit";
import { JobGitRefLink } from "./JobGitRefLink";
import { JobTypeIcon } from "../JobIcons";

export const JobSummaryColumn = ({ job, colorStyle }) => {
  return (
    <Stack>
      <JobTypeIcon
        type={job.type}
        colorStyle={colorStyle}
        labelPosition="right"
      />
      <Link
        href={`/iocs/${job.iocId}`}
        aria-label={`IOC ${job.iocName}`}
      >
        {job.iocName}
      </Link>
      <Stack
        flexDirection="row"
        gap={0.5}
        alignItems="center"
      >
        <Link
          href={`/jobs/${job?.id}`}
          aria-label={`Job ID ${job.id}`}
        >{`#${job.id}`}</Link>
        {job.type === "DEPLOY" ? (
          <Chip
            size="small"
            icon={<CommitIcon />}
            label={<JobGitRefLink job={job} />}
          />
        ) : null}
      </Stack>
    </Stack>
  );
};
