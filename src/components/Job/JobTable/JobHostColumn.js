import { Link, Stack, Typography } from "@mui/material";
import React from "react";

export const JobHostColumn = ({ job }) => {
  if (job.host?.hostName) {
    return (
      <Stack>
        <Link
          href={`/hosts/${job?.host?.externalHostId}`}
          aria-label={`Host ${job.host?.hostName}`}
        >
          {job.host.hostName}
        </Link>
        <Typography variant="body2">{job.host.network}</Typography>
      </Stack>
    );
  }

  return null;
};
