import React from "react";
import AccessTimeIcon from "@mui/icons-material/AccessTime";
import { Tooltip } from "@mui/material";
import LabeledIcon from "../common/LabeledIcon";
import moment from "moment";

const formattedDuration = ({ startDate, finishDate }) => {
  if (startDate && finishDate) {
    return moment
      .utc(finishDate.getTime() - startDate.getTime())
      .format("HH:mm:ss");
  } else {
    return null;
  }
};

export const JobDuration = ({ job, renderContents }) => {
  const createOrStartDate = new Date(job?.startTime ?? job.createdAt);

  const duration = formattedDuration({
    finishDate: new Date(job.finishedAt),
    startDate: new Date(createOrStartDate)
  });

  const contents = renderContents ? (
    renderContents(duration)
  ) : (
    <LabeledIcon
      label={`${duration}`}
      LabelProps={{ variant: "body2" }}
      labelPosition="right"
      Icon={AccessTimeIcon}
      IconProps={{ fontSize: "small" }}
    />
  );

  return (
    <Tooltip
      title={`Finished ${job.finishedAt}`}
      aria-label={`Finshed ${job.finishedAt}, after ${duration}`}
    >
      {contents}
    </Tooltip>
  );
};
