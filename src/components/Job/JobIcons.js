import React from "react";
import {
  DeploymentStatusIcon,
  DeploymentTypeIcon
} from "../deployments/DeploymentIcons";
import { CommandTypeIcon } from "../IOC/IOCIcons";

export function JobStatusIcon({ status }) {
  return <DeploymentStatusIcon status={status} />;
}

export function JobTypeIcon({ type, colorStyle, labelPosition }) {
  const icon = ["START", "STOP"].includes(type) ? (
    <CommandTypeIcon
      type={type}
      colorStyle={colorStyle}
      labelPosition={labelPosition}
    />
  ) : (
    <DeploymentTypeIcon
      type={type}
      labelPosition={labelPosition}
    />
  );

  return icon;
}
