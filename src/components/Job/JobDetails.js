import React, { useEffect, useState, useMemo } from "react";
import {
  Typography,
  Card,
  CardContent,
  Link as MuiLink,
  Stack
} from "@mui/material";
import {
  KeyValueTable,
  SimpleAccordion,
  AlertBannerList
} from "@ess-ics/ce-ui-common";
import { JobBadge } from "./JobBadge";
import { DeploymentJobOutput } from "../deployments/DeploymentJobOutput";
import { Link as ReactRouterLink } from "react-router-dom";
import { formatDate } from "../common/Helper";
import GitRefLink from "../IOC/GitRefLink";
import AccessControl from "../auth/AccessControl";
import { AWXJobDetails } from "../../api/DataTypes";
import { JobStatusStepper } from "./JobStatus";
import { ExternalLink, ExternalLinkContents } from "../common/Link";
import { JobDuration } from "./JobDuration";

function createAlert(operation, job) {
  const jobDetails = new AWXJobDetails(operation.type, job);
  const message = jobDetails.message();
  const severity = jobDetails.severity();
  return {
    type: severity,
    message: message
  };
}

export function JobDetails({ operation, job }) {
  let jobAlert = useMemo(() => operation.alerts ?? [], [operation]);

  const finishedJob = useMemo(
    () => operation && new AWXJobDetails(operation.type, job).isFinished(),
    [operation, job]
  );

  const [alert, setAlert] = useState(createAlert(operation, job));

  useEffect(() => {
    setAlert(createAlert(operation, job, finishedJob));
  }, [finishedJob, operation, job]);

  function calculateHostText() {
    // host is resolvable => show link for users
    if (operation.host.csEntryId && operation.host.csEntryIdValid) {
      return (
        <Typography>
          <MuiLink
            component={ReactRouterLink}
            to={`/hosts/${operation.host.csEntryId}`}
            underline="hover"
          >
            {operation.host.fqdn}
          </MuiLink>
        </Typography>
      );
    }

    // gray out hostname if it is from cache
    return <Typography color="gray">{operation.host.fqdn}</Typography>;
  }

  const deploymentDetails = {
    Revision: (
      <GitRefLink
        url={operation.gitProjectUrl}
        revision={operation.gitShortReference}
      />
    ),
    host: calculateHostText(),
    user: (
      <MuiLink
        component={ReactRouterLink}
        to={`/user/${operation.createdBy}`}
        underline="hover"
      >
        {operation.createdBy}
      </MuiLink>
    ),
    "AWX job link": (
      <ExternalLink
        href={operation.awxJobUrl}
        aria-label="AWX job"
      >
        <ExternalLinkContents>{operation.awxJobUrl}</ExternalLinkContents>
      </ExternalLink>
    ),
    "created time": operation?.createdAt
      ? formatDate(operation.createdAt)
      : "-",
    "AWX job start time": operation?.startTime
      ? formatDate(operation.startTime)
      : "-",
    duration: operation?.finishedAt ? (
      <JobDuration
        job={operation}
        renderContents={(duration) => duration}
      />
    ) : (
      "-"
    )
  };

  const finishedJobAlerts = finishedJob ? (
    <AlertBannerList alerts={[alert].concat(jobAlert)} />
  ) : null;

  const unFinishedJobsWithAlerts =
    !finishedJob && alert ? <AlertBannerList alerts={[alert]} /> : null;

  return (
    <Stack spacing={2}>
      <Stack>
        {finishedJobAlerts}
        {unFinishedJobsWithAlerts}
      </Stack>
      <JobBadge operation={operation} />
      <KeyValueTable
        obj={deploymentDetails}
        variant="overline"
        sx={{ border: 0 }}
        valueOptions={{ headerName: "" }}
      />
      {job ? (
        <Card>
          <CardContent>
            <JobStatusStepper {...{ job, operation }} />
          </CardContent>
        </Card>
      ) : null}
      <AccessControl
        allowedRoles={["DeploymentToolAdmin", "DeploymentToolIntegrator"]}
        renderNoAccess={() => <></>}
      >
        {job ? (
          <SimpleAccordion summary="Ansible Job Output">
            <DeploymentJobOutput deploymentJob={job} />
          </SimpleAccordion>
        ) : (
          <></>
        )}
      </AccessControl>
    </Stack>
  );
}
