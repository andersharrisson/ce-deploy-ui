import { Stepper } from "@ess-ics/ce-ui-common";
import React from "react";
import {
  RotateRightOutlined,
  CheckCircleOutline,
  ErrorOutline,
  HelpOutline
} from "@mui/icons-material";
import LabeledIcon from "../common/LabeledIcon";
import { theme } from "../../style/Theme";

const STEPPER_STATUS = {
  queued: {
    label: "Queued",
    alertType: "info",
    icon: RotateRightOutlined,
    color: theme.palette.status.progress
  },
  running: {
    label: "Running",
    alertType: "info",
    icon: RotateRightOutlined,
    color: theme.palette.status.progress
  },
  successful: {
    label: "Completed",
    alertType: "success",
    icon: CheckCircleOutline,
    color: theme.palette.status.ok
  }
};

const STATUS = {
  ...STEPPER_STATUS,
  failed: {
    label: "Error",
    alertType: "error",
    icon: ErrorOutline,
    color: theme.palette.status.fail
  },
  unknown: {
    label: "Unknown",
    alertType: "info",
    icon: HelpOutline,
    color: theme.palette.disabled
  }
};

export const JobStatusStepper = ({ job, operation }) => {
  const normalizedStatus = job?.status?.toLowerCase();

  const currentStep = Object.keys(STEPPER_STATUS).indexOf(normalizedStatus);
  let activeStep = STEPPER_STATUS[normalizedStatus]
    ? currentStep + 1
    : currentStep;
  const jobFailed = job?.status?.toLowerCase() === "failed";

  // to show the correct failed step for an already finished operation
  if (jobFailed) {
    activeStep = operation?.startTime ? 1 : 0;
  }

  return (
    <Stepper
      steps={Object.values(STEPPER_STATUS).map((it) => it.label)}
      activeStep={activeStep}
      isActiveStepFailed={jobFailed}
    />
  );
};

export const JobStatusIcon = ({ job }) => {
  const activeStep = STATUS[job?.status?.toLowerCase()] ?? STATUS.unknown;

  return (
    <LabeledIcon
      label={activeStep.label}
      labelPosition="right"
      LabelProps={{
        color: activeStep.alertType === "error" ? "status.fail" : "inherit"
      }}
      Icon={activeStep.icon}
      IconProps={{
        style: {
          fill: activeStep.color
        }
      }}
    />
  );
};
