export function serialize(value) {
  if (typeof value === "boolean") {
    return value ? "true" : "false";
  }
  if (typeof value === "number") {
    return value.toString();
  }
  if (typeof value === "object") {
    return JSON.stringify(value);
  }
  return value;
}

export function deserialize(value) {
  if (value === "true") {
    return true;
  }
  if (value === "false") {
    return false;
  }
  if (Number(value).toString() === value && !isNaN(Number(value))) {
    return Number(value);
  }
  return value;
}
