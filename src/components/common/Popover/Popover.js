import React from "react";
import { styled } from "@mui/material/styles";
import { Popover as MuiPopover } from "@mui/material";

const StyledMuiPopover = styled(MuiPopover)(({ theme }) => ({
  "&": {
    pointerEvents: "none"
  },
  "& .MuiPopover-paper": {
    padding: theme.spacing(1)
  }
}));

/**
 * This callback renders the owning component of a popover, e.g. the thing the user clicks or hovers over.
 * @callback renderOwningComponent
 * @param {string} ariaOwns - the unique string that identifies this owner with its popover
 * @param {boolean} ariaHasPopup - always true; this is a reminder to supply this aria attribute on your component
 * @param {Object} handlePopoverOpen - function called when the popover opens
 * @param {function} handlePopoverClose - function called when the popover closes
 * @returns JSX component
 */

/**
 * Renders a Popover pre-configured with the necessary classes and states.
 * @param {renderOwningComponent} renderOwningComponent
 * @param {Object} popoverContents - the contents of the popover
 * @param {string} id - the unique id tying the popover to its owning component
 * @param {...Object} popoverProps - Mui Popover props for e.g. overriding behavior such as anchor and transform origin
 * @returns JSX component
 */
export const Popover = ({
  renderOwningComponent,
  popoverContents,
  id,
  ...popoverProps
}) => {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handlePopoverOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handlePopoverClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const elemId = id ? id : "popover-id";
  const ariaOwns = open ? elemId : undefined;

  return (
    <>
      {renderOwningComponent({
        ariaOwns,
        ariaHasPopup: true,
        handlePopoverOpen,
        handlePopoverClose
      })}
      <StyledMuiPopover
        id={elemId}
        open={open}
        anchorEl={anchorEl}
        onClose={handlePopoverClose}
        disableRestoreFocus
        {...popoverProps}
      >
        {popoverContents}
      </StyledMuiPopover>
    </>
  );
};
