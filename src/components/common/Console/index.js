import { Console } from "./Console";
import { ConsoleOutput } from "./ConsoleOutput";
import { LogDialog } from "./LogDialog";

export { Console, ConsoleOutput, LogDialog };
export default Console;
