import React, { useState } from "react";
import { Container, Button, Grid, Box, LinearProgress } from "@mui/material";
import { Launch } from "@mui/icons-material";
import { ConsoleOutput } from "./ConsoleOutput";
import { LogDialog } from "./LogDialog";

export function Console({
  html,
  dataReady,
  extraClass,
  title,
  dialogHeader,
  loading
}) {
  const [logDialogOpen, setLogDialogOpen] = useState(false);

  const onDialogPopUp = () => {
    setLogDialogOpen(true);
  };

  return (
    <>
      {!logDialogOpen && loading ? (
        <div style={{ width: "100%" }}>
          <LinearProgress color="primary" />
        </div>
      ) : (
        <>
          <LogDialog
            open={logDialogOpen}
            setOpen={setLogDialogOpen}
            title={title}
            content={
              loading ? (
                <div style={{ width: "100%" }}>
                  <LinearProgress color="primary" />
                </div>
              ) : (
                <Container maxWidth="xl">
                  {dialogHeader}
                  <div style={{ height: 10 }} />
                  <ConsoleOutput
                    html={html}
                    dataReady={dataReady}
                    extraClass={extraClass}
                    shownInDialog
                  />
                </Container>
              )
            }
          />
          <Grid
            container
            spacing={0}
            justifyContent="flex-end"
            paddingBottom={1}
          >
            <Grid
              item
              xs={9}
            >
              {dialogHeader}
              {/* <div style={{ height: 10 }} /> */}
            </Grid>
            <Grid
              item
              xs={3}
            >
              <Box
                display="flex"
                flexDirection="row-reverse"
                p={0}
                m={0}
              >
                <Button
                  color="inherit"
                  variant="outlined"
                  endIcon={<Launch />}
                  style={{ float: "right" }}
                  onClick={onDialogPopUp}
                >
                  Popout log
                </Button>
              </Box>
            </Grid>
          </Grid>
          <ConsoleOutput
            html={html}
            dataReady={dataReady}
            extraClass={extraClass}
            shownInDialog={false}
          />
        </>
      )}
    </>
  );
}
