import { Dialog } from "@ess-ics/ce-ui-common/dist/components/common/Dialog";
import { Typography } from "@mui/material";
import React from "react";

export function LogDialog({ open, setOpen, title, content }) {
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Dialog
      title={
        <Typography
          variant="h2"
          marginY={1}
        >
          {title}
        </Typography>
      }
      content={content}
      open={open}
      onClose={handleClose}
      DialogProps={{
        fullWidth: true,
        maxWidth: "xl"
      }}
    />
  );
}
