import React, { useState, useRef, useEffect } from "react";
import { styled } from "@mui/material/styles";
import clsx from "clsx";
import { useWindowDimensions } from "../../common/Helper";

const PREFIX = "ConsoleOutput";

const classes = {
  consoleOutput: `${PREFIX}-consoleOutput`,
  dialogOutput: `${PREFIX}-dialogOutput`
};

const Root = styled("div")(({ theme }) => ({
  [`& .${classes.consoleOutput}`]: {
    backgroundColor: "black",
    paddingLeft: theme.spacing(4),
    paddingRight: theme.spacing(4),
    overflow: "auto",
    minHeight: "500px",
    maxHeight: "500px",
    width: "100%",
    color: "white",
    fontSize: "0.8rem"
  },

  [`& .${classes.dialogOutput}`]: {
    backgroundColor: "black",
    paddingLeft: theme.spacing(4),
    paddingRight: theme.spacing(4),
    overflow: "auto",
    width: "100%",
    color: "white"
  }
}));

export function ConsoleOutput({ html, dataReady, extraClass, shownInDialog }) {
  const stdoutRef = useRef(null);
  const [firstTime, setFirstTime] = useState(true);
  const [autoScrollEnabled, setAutoScrollEnabled] = useState(true);
  const { height } = useWindowDimensions();

  const [y, setY] = useState(0);

  let styleClass;

  useEffect(() => {
    function scrollDown() {
      if (stdoutRef.current) {
        stdoutRef.current.scrollTop = stdoutRef.current.scrollHeight;
        if (firstTime && dataReady) {
          setFirstTime(false);
        }
      }
    }

    if (html && autoScrollEnabled) {
      scrollDown();
    }
  }, [dataReady, firstTime, autoScrollEnabled, html]);

  const handleScroll = (e) => {
    const pane = e.currentTarget;
    if (y > pane.scrollTop) {
      // Scroll up event
      setAutoScrollEnabled(false);
    } else if (pane.scrollHeight - pane.scrollTop === pane.clientHeight) {
      // Scroll to bottom
      setAutoScrollEnabled(true);
    }
    setY(pane.scrollTop);
  };

  if (shownInDialog) {
    styleClass = classes.dialogOutput;
  } else {
    styleClass = classes.consoleOutput;
  }

  return (
    <Root>
      <div
        onScroll={handleScroll}
        ref={stdoutRef}
        className={clsx(styleClass, extraClass)}
        style={{ maxHeight: shownInDialog ? height - 220 : 500 }}
        // eslint-disable-next-line react/no-danger
        dangerouslySetInnerHTML={{ __html: html }}
      />
    </Root>
  );
}
