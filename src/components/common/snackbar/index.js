import { useCustomSnackbar } from "./Snackbar";

export { useCustomSnackbar };
export default useCustomSnackbar;
