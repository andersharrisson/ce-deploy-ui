import React from "react";
import { useSnackbar } from "notistack";
import { Button } from "@mui/material";

export function useCustomSnackbar() {
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  function showError(errorMessage, severity = "error") {
    console.error("Snackbar: " + errorMessage);
    const action = (key) => (
      <Button
        variant="text"
        onClick={() => {
          closeSnackbar(key);
        }}
        sx={{
          // Note all colors must be absolute or theme defaults because
          // Snackbar loads before the custom ThemeProvider theme loads
          color: "#f0f0f0",
          borderColor: "#f0f0f0",
          "&:hover": {
            backgroundColor: "#8d8d8d",
            borderColor: "#8d8d8d",
            boxShadow: "none"
          }
        }}
      >
        Dismiss
      </Button>
    );
    enqueueSnackbar(errorMessage, {
      variant: severity,
      autoHideDuration: null,
      action
    });
  }

  return showError;
}
