import React, { useState, useEffect } from "react";
import moment from "moment";
import { alpha } from "@mui/material/styles";
import { Link, Tooltip, Typography, styled } from "@mui/material";
import TimeAgo from "javascript-time-ago";
import en from "javascript-time-ago/locale/en";

export function formatToList(items) {
  if (!items) return null;

  return (
    <ul style={{ padding: 0 }}>
      {items.map((item) => (
        <li
          style={{ listStylePosition: "inside" }}
          key={item}
        >
          {item}
        </li>
      ))}
    </ul>
  );
}

export function searchInArray(array, searchText) {
  if (array) {
    const found = array.find((element) =>
      element.toLowerCase().includes(searchText)
    );

    return found;
  }

  return false;
}

export const formatDate = (value) => {
  if (value) {
    return moment(value).format("DD/MM/YYYY HH:mm:ss");
  }
  return null;
};

export const formatDateOnly = (value) => {
  if (value) {
    return moment(value).format(dateFormat());
  }
  return null;
};

export const dateFormat = () => {
  return "DD/MM/YYYY";
};

function getWindowDimensions() {
  const { innerWidth: width, innerHeight: height } = window;
  return {
    width,
    height
  };
}

export function useWindowDimensions() {
  const [windowDimensions, setWindowDimensions] = useState(
    getWindowDimensions()
  );

  useEffect(() => {
    function handleResize() {
      setWindowDimensions(getWindowDimensions());
    }

    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  return windowDimensions;
}

export function circularPalette(palette, index, opacity = 1.0) {
  const colors = Object.values(palette);
  let n = colors.length;
  return alpha(colors[((index % n) + n) % n], opacity);
}

function applicationSubTitle() {
  const title = `${window.ENVIRONMENT_TITLE}`;

  if (title && title !== "undefined") {
    return " - " + title;
  }

  return "";
}

export function applicationTitle(...breadcrumbs) {
  return [`CE deploy & monitor ${applicationSubTitle()}`, ...breadcrumbs].join(
    " / "
  );
}

export function initRequestParams(lazyParams, filter, columnSort) {
  let requestParams = {
    page: lazyParams.page,
    limit: lazyParams.rows
  };
  if (filter != null && filter) {
    requestParams.query = filter;
  }

  if (columnSort) {
    if (columnSort.sortOrder === 1) {
      requestParams.order_asc = true;
    } else {
      requestParams.order_asc = false;
    }
  }

  return requestParams;
}

export const compareArrays = (a1, a2) =>
  a1.length === a2.length &&
  a1.every((element, index) => element === a2[index]);

function createPartQuery(part, openEnd) {
  if (part && part.length > 0) {
    return "(+fqdn:" + part + (openEnd ? "*)" : ")");
  }
  return null;
}

function joinParts(parts, openEnd) {
  const queries = parts.map((p, index) =>
    createPartQuery(p, openEnd && index === parts.length - 1)
  );
  return queries.filter((q) => q && q.length > 0).join(" AND ");
}

export function transformHostQuery(query) {
  if (!query || query.length === 0) {
    return null;
  }

  const parts = query.split("-");
  if (parts.length === 1) {
    return "fqdn:" + query + "*";
  }

  // if query ends with "-"
  if (parts[parts.length - 1].length === 0) {
    if (parts.length === 2) {
      return "fqdn:" + parts[0];
    }
    return joinParts(parts, false);
  }

  return joinParts(parts, true);
}

export function noWrapText(text) {
  return <Typography noWrap>{text}</Typography>;
}

export const EllipsisTextLink = styled(
  ({ text, className, TooltipProps, ...props }) => {
    return (
      <Tooltip
        title={text}
        className={className}
        {...TooltipProps}
      >
        <Link {...props}>{text}</Link>
      </Tooltip>
    );
  }
)({
  textOverflow: "ellipsis",
  whiteSpace: "nowrap",
  overflow: "hidden"
});

export function extractMainNetwork(interfaces, defaultReturn = "") {
  if (!interfaces) {
    return defaultReturn;
  }

  var i;
  for (i = 0; i < interfaces.length; i++) {
    if (interfaces[i]?.is_main) {
      return interfaces[i].network;
    }
  }

  return defaultReturn;
}

export function onFetchEntityError(message, status, setNotFoundError) {
  const notFound = status === 404;
  if (notFound) {
    setNotFoundError(message);
  }
  return !notFound;
}

export function pick(obj, keys) {
  const ret = {};
  for (const key of keys) {
    ret[key] = obj[key];
  }
  return ret;
}

TimeAgo.addDefaultLocale(en);
export const timeAgo = new TimeAgo("en-GB");
