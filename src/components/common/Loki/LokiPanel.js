import React, {
  useState,
  useEffect,
  useCallback,
  useContext,
  useMemo
} from "react";
import { styled } from "@mui/material/styles";
import {
  Box,
  Grid,
  Select,
  MenuItem,
  InputLabel,
  FormControl,
  LinearProgress
} from "@mui/material";
import { formatDate } from "../Helper.js";
import { Console } from "../Console/Console";
import { useSafePolling } from "../../../hooks/Polling";
import { useCustomSnackbar } from "../snackbar/Snackbar";
import { closeSnackbar } from "notistack";
import { apiContext } from "../../../api/DeployApi";
import { useAPIMethod } from "@ess-ics/ce-ui-common";

const PREFIX = "LokiPanel";

const classes = {
  formControl: `${PREFIX}-formControl`,
  selectEmpty: `${PREFIX}-selectEmpty`,
  deployed: `${PREFIX}-deployed`,
  undeployed: `${PREFIX}-undeployed`
};

const Root = styled("div")(({ theme }) => ({
  [`& .${classes.formControl}`]: {
    margin: theme.spacing(0),
    minWidth: 120
  },

  [`& .${classes.selectEmpty}`]: {
    marginTop: theme.spacing(2)
  },

  [`& .${classes.deployed}`]: {
    backgroundColor: "black",
    msUserSelect: "text",
    MozUserSelect: "text",
    WebkitUserSelect: "text"
  },

  [`& .${classes.undeployed}`]: {
    backgroundColor: "gray",
    msUserSelect: "none",
    MozUserSelect: "none",
    WebkitUserSelect: "none"
  }
}));

/**
 * host: the host URL which will be the qery parameter for backend call
 * iocName: name of ioc (optional). Used when querying procServ log
 * isSyslog: boolean, if true -> SysLog will be queried, if false -> procServLog will be queried
 * isDeployed: boolean, if true -> periodic update (backend query) will be enabled, if false -> periodic update will be disabled
 */

const LOG_POLL_INTERVAL = 5000;
export function LokiPanel({ host, iocName, isSyslog, isDeployed }) {
  const showWarning = useCustomSnackbar();
  const client = useContext(apiContext);

  const handleError = useCallback(
    (error) => {
      // had to add the error code check or else the request aborts would also show up
      if (error?.message && error?.statusCode) {
        showWarning(error.message, "warning");
      }
    },
    [showWarning]
  );

  const hostName = host.csEntryHost.name;
  const [timeRange, setTimeRange] = React.useState(720);
  const [timeRangeText, setTimeRangeText] = React.useState("12 hours");
  const [periodChange, setPeriodChange] = useState(false);
  const [alertIds, setAlertIds] = useState([]);
  const [html, setHtml] = useState("");

  const preprocessLog = useCallback(
    (
      logData,
      isDeployed,
      showWarning,
      timeRangeText,
      alertIds,
      setAlertIds
    ) => {
      if (logData?.warning && alertIds?.length === 0) {
        const warningId = showWarning(logData.warning, "warning");
        setAlertIds((alertIds) => [...alertIds, warningId]);
      }

      if (logData && logData.lines?.length > 0) {
        const logHtml = logData.lines.map((line) => formatLogLine(line));
        return `<html>
                    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                      <style type="text/css">.logdate { color: #5cb85c; } body.pre{font-family: Monaco, Menlo, Consolas, "Courier New", monospace;}
                      </style>
                    </head>
                    <body>
                      <pre>${logHtml.join("")}</pre>
                    </body>
                  </html>`;
      } else {
        if (!(isDeployed === true)) {
          return "<html><body> NOT DEPLOYED YET! </body></html>";
        }
      }

      return `<html><body> - No messages found for ${timeRangeText} period - </body></html>`;
    },
    []
  );

  const logsToPreprocess = useCallback(
    (
      isSysLog,
      logData,
      procServLog,
      isDeployed,
      showWarning,
      timeRangeText,
      alertIds,
      setAlertIds
    ) => {
      if (isSysLog === true) {
        return preprocessLog(
          logData,
          true,
          showWarning,
          timeRangeText,
          alertIds,
          setAlertIds
        );
      }

      return preprocessLog(
        procServLog,
        isDeployed,
        showWarning,
        timeRangeText,
        alertIds,
        setAlertIds
      );
    },
    [preprocessLog]
  );

  const params = useMemo(
    () => ({
      host_name: hostName,
      ioc_name: iocName,
      time_range: timeRange
    }),
    [hostName, iocName, timeRange]
  );

  const {
    value: sysLogData,
    wrapper: getSysLogData,
    sysLoading,
    error: sysLogError
  } = useAPIMethod({
    fcn: client.apis.Monitoring.fetchSyslogLines,
    call: false,
    params
  });

  const {
    value: procServLog,
    wrapper: getProcServLog,
    procServLoading,
    error: procServLogError
  } = useAPIMethod({
    fcn: client.apis.Monitoring.fetchProcServLogLines,
    call: false,
    params
  });

  // show sysLogErrors on snackBar
  useEffect(() => {
    handleError(sysLogError);
  }, [sysLogError, handleError]);

  // show procServErrors on snackBar
  useEffect(() => {
    handleError(procServLogError);
  }, [procServLogError, handleError]);

  const handleChange = (event) => {
    setPeriodChange(true);
    setTimeRange(event.target?.value);
    setTimeRangeText(event.currentTarget?.innerText);
  };

  // remove progressBar if intervall has been changed, and data received
  useEffect(() => {
    setPeriodChange(false);
  }, [sysLogData, procServLog]);

  // remove the logline limit exceed warning when user leaves component
  useEffect(() => {
    return () => {
      alertIds.forEach((id) => {
        closeSnackbar(id);
      });
    };
  }, [alertIds]);

  const updateLogs = useCallback(() => {
    if (isSyslog === true) {
      getSysLogData(hostName, timeRange);
    } else {
      if (isDeployed === true) {
        getProcServLog(hostName, iocName, timeRange);
      }
    }
  }, [
    getSysLogData,
    getProcServLog,
    hostName,
    iocName,
    isDeployed,
    isSyslog,
    timeRange
  ]);

  useSafePolling(updateLogs, sysLoading || procServLoading, LOG_POLL_INTERVAL);

  // if request period has been changed -> show progressBar
  // if (periodChange) {
  //     return <div style={{ width: "100%" }}><LinearProgress color="primary" /></div>;
  // }

  useEffect(() => {
    setHtml(
      logsToPreprocess(
        isSyslog,
        sysLogData,
        procServLog,
        isDeployed,
        showWarning,
        timeRangeText,
        alertIds,
        setAlertIds
      )
    );
  }, [
    setHtml,
    alertIds,
    isDeployed,
    isSyslog,
    logsToPreprocess,
    procServLog,
    showWarning,
    sysLogData,
    timeRangeText
  ]);

  const dataReady = () => {
    return sysLogData || procServLog;
  };

  let header = (
    <FormControl
      variant="filled"
      className={classes.formControl}
    >
      <InputLabel id="time-range-select">Time range</InputLabel>
      <Select
        variant="standard"
        labelId="time-range-select"
        value={timeRange}
        onChange={handleChange}
      >
        <MenuItem value={720}>12 hours</MenuItem>
        <MenuItem value={10080}>7 days</MenuItem>
      </Select>
    </FormControl>
  );

  return (
    <Root>
      <Box>
        {sysLogData || procServLog || !isDeployed ? (
          <Grid
            container
            spacing={1}
            justifyContent="flex-start"
          >
            <Grid
              item
              xs={12}
              md={12}
            >
              <Console
                html={html}
                dataReady={dataReady}
                extraClass={isDeployed ? classes.deployed : classes.undeployed}
                title={isSyslog ? "Host log stream" : "IOC log stream"}
                dialogHeader={header}
                loading={periodChange}
              />
            </Grid>
          </Grid>
        ) : (
          <div style={{ width: "100%" }}>
            <LinearProgress color="primary" />
          </div>
        )}
      </Box>
    </Root>
  );
}

function formatLogLine(logLine) {
  var Convert = require("ansi-to-html");
  var convert = new Convert();

  return (
    "<span><span class=logdate>" +
    formatDate(logLine.logDate) +
    "</span> " +
    convert.toHtml(logLine.logMessage) +
    "<br/></span>"
  );
}
