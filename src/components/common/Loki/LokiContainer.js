import React, { useContext, useMemo } from "react";
import { Box, Typography } from "@mui/material";
import { LokiPanel } from "./LokiPanel";
import { apiContext } from "../../../api/DeployApi";
import { useAPIMethod } from "@ess-ics/ce-ui-common";

export function LokiContainer({ csEntryId, iocName, isDeployed }) {
  const client = useContext(apiContext);

  const { value: iocHost } = useAPIMethod({
    fcn: client.apis.Hosts.findHostById,
    params: useMemo(() => ({ host_csentry_id: csEntryId }), [csEntryId])
  });

  return (
    <>
      {iocHost && (
        <Box sx={{ pt: 2 }}>
          <Typography
            sx={{ my: 2.5 }}
            variant="h3"
          >
            IOC log stream
          </Typography>
          <LokiPanel
            host={iocHost}
            isSyslog={false}
            iocName={iocName}
            isDeployed={isDeployed}
          />
        </Box>
      )}
    </>
  );
}
