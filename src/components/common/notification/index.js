import { NotificationProvider, createContext } from "./Notifications";
import { DeploymentWatcher, CommandWatcher } from "./Watchers";
import {
  removeId,
  clearAll,
  getStoredNotifications,
  appendNotification,
  watchDeployment,
  watchCommand,
  getStoredDeplomentIds,
  getStoredCommandIds,
  WATCHED_DEPLOYMENT_STORAGE_KEY,
  DEPLOYMENT_NOTIFICATIONS,
  WATCHED_COMMAND_STORAGE_KEY,
  CURRENT_NOTIFICATION_VERSION,
  TYPE_DEPLOYMENT,
  TYPE_COMMAND
} from "./Storage";

export {
  NotificationProvider,
  createContext,
  DeploymentWatcher,
  CommandWatcher,
  removeId,
  clearAll,
  getStoredNotifications,
  appendNotification,
  watchDeployment,
  watchCommand,
  getStoredDeplomentIds,
  getStoredCommandIds,
  WATCHED_DEPLOYMENT_STORAGE_KEY,
  DEPLOYMENT_NOTIFICATIONS,
  WATCHED_COMMAND_STORAGE_KEY,
  CURRENT_NOTIFICATION_VERSION,
  TYPE_DEPLOYMENT,
  TYPE_COMMAND
};
