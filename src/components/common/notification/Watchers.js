import { useEffect, useMemo, useContext } from "react";
import { AdHocCommand, DeploymentStatus } from "../../../api/DataTypes";
import { useSafePolling } from "../../../hooks/Polling";
import { STATUS_POLL_INTERVAL } from "./Notifications";
import {
  TYPE_COMMAND,
  TYPE_DEPLOYMENT,
  WATCHED_COMMAND_STORAGE_KEY,
  WATCHED_DEPLOYMENT_STORAGE_KEY
} from "./Storage";
import { useAPIMethod } from "@ess-ics/ce-ui-common";
import { apiContext } from "../../../api/DeployApi";

export function DeploymentWatcher({
  deploymentId,
  addNotification,
  stopWatching
}) {
  const client = useContext(apiContext);

  const deploymentParams = useMemo(
    () => ({
      operation_id: deploymentId
    }),
    [deploymentId]
  );

  const {
    value: deployment,
    wrapper: getDeployment,
    loading: deploymentLoading,
    dataReady: deploymentDataReady,
    abort: abortGetDeployment
  } = useAPIMethod({
    fcn: client.apis.Deployments.fetchOperation,
    call: false,
    params: deploymentParams
  });

  const deploymentJobParams = useMemo(
    () => ({ awx_job_id: deployment?.awxJobId }),
    [deployment]
  );

  const {
    value: deploymentJob,
    wrapper: getDeploymentJob,
    loading: jobLoading,
    dataReady: jobDataReady,
    abort: abortGetDeploymentJob
  } = useAPIMethod({
    fcn: client.apis.Deployments.fetchJobDetails,
    params: deploymentJobParams
  });

  useSafePolling(
    getDeployment,
    deploymentLoading || !deploymentDataReady,
    STATUS_POLL_INTERVAL,
    true,
    abortGetDeployment
  );

  useSafePolling(
    getDeploymentJob,
    jobLoading || !jobDataReady,
    STATUS_POLL_INTERVAL,
    true,
    abortGetDeploymentJob
  );

  const deploymentStatus = useMemo(
    () => new DeploymentStatus(deployment, deploymentJob),
    [deployment, deploymentJob]
  );

  useEffect(() => {
    if (deploymentJob) {
      const message = `${deployment.iocName}: ${deploymentStatus.message()}`;
      addNotification(
        TYPE_DEPLOYMENT,
        deploymentId,
        deploymentStatus.status(),
        message
      );

      if (deploymentStatus.isFinished()) {
        stopWatching(deployment.id, WATCHED_DEPLOYMENT_STORAGE_KEY);
      }
    }
  }, [
    addNotification,
    deployment,
    deploymentId,
    deploymentJob,
    deploymentStatus,
    stopWatching
  ]);

  return null;
}

export function CommandWatcher({ commandId, addNotification, stopWatching }) {
  const client = useContext(apiContext);

  const params = useMemo(
    () => ({
      operation_id: commandId
    }),
    [commandId]
  );

  const {
    value: command,
    wrapper: getCommand,
    loading
  } = useAPIMethod({
    fcn: client.apis.Deployments.fetchOperation,
    call: false,
    params
  });

  useSafePolling(getCommand, loading, STATUS_POLL_INTERVAL);

  const commandStatus = useMemo(
    () => new AdHocCommand(command, null),
    [command]
  );

  useEffect(() => {
    if (command) {
      const message = `${command.iocName}: ${commandStatus.message()}`;
      addNotification(TYPE_COMMAND, commandId, command.status, message);

      if (commandStatus.isFinished()) {
        stopWatching(command.id, WATCHED_COMMAND_STORAGE_KEY);
      }
    }
  }, [addNotification, command, commandId, commandStatus, stopWatching]);

  return null;
}
