import React, { useCallback, useMemo, useState } from "react";
import { createContext } from "react";
import { CommandWatcher, DeploymentWatcher } from "./Watchers.js";
import {
  getStoredCommandIds,
  getStoredDeplomentIds,
  getStoredNotifications,
  watchDeployment as addDeploymentToWatchList,
  watchCommand as addCommandToWatchList,
  clearAll,
  appendNotification,
  removeId
} from "./Storage.js";
import { compareArrays } from "../Helper.js";
import { useInterval } from "../../../hooks/Polling.js";

export const notificationContext = createContext({
  notifications: [],
  watchDeployment: () => {},
  clearNotifications: () => {}
});
export const STATUS_POLL_INTERVAL = 3000;

export function NotificationProvider({ children }) {
  const [deploymentIds, setDeploymentIds] = useState(getStoredDeplomentIds());
  const [commandIds, setCommandIds] = useState(getStoredCommandIds());
  const [notifications, setNotifications] = useState(getStoredNotifications());

  const updateWatchedIds = useCallback(() => {
    updateIdsIfChanged(deploymentIds, getStoredDeplomentIds, setDeploymentIds);
    updateIdsIfChanged(commandIds, getStoredCommandIds, setCommandIds);
  }, [deploymentIds, setDeploymentIds, commandIds, setCommandIds]);

  useInterval(updateWatchedIds, STATUS_POLL_INTERVAL);

  const updateNotifications = useCallback(() => {
    setNotifications(getStoredNotifications());
  }, [setNotifications]);

  const addNotification = useCallback(
    (...args) => {
      appendNotification(...args);
      updateNotifications();
    },
    [updateNotifications]
  );

  const clearNotifications = useCallback(() => {
    clearAll();
    updateNotifications();
  }, [updateNotifications]);

  const watchDeployment = useCallback(
    (...args) => {
      addDeploymentToWatchList(...args);
      updateWatchedIds();
    },
    [updateWatchedIds]
  );

  const watchCommand = useCallback(
    (...args) => {
      addCommandToWatchList(...args);
      updateWatchedIds();
    },
    [updateWatchedIds]
  );

  const stopWatching = useCallback(
    (...args) => {
      removeId(...args);
      updateWatchedIds();
    },
    [updateWatchedIds]
  );

  const contextValue = useMemo(
    () => ({
      notifications,
      watchDeployment,
      watchCommand,
      clearNotifications
    }),
    [clearNotifications, notifications, watchCommand, watchDeployment]
  );

  const deploymentWatchers = useMemo(
    () =>
      deploymentIds.map((id) => (
        <DeploymentWatcher
          key={`deployment-${id}`}
          deploymentId={id}
          addNotification={addNotification}
          stopWatching={stopWatching}
        />
      )),
    [addNotification, deploymentIds, stopWatching]
  );
  const commandWatchers = useMemo(
    () =>
      commandIds.map((id) => (
        <CommandWatcher
          key={`command-${id}`}
          commandId={id}
          addNotification={addNotification}
          stopWatching={stopWatching}
        />
      )),
    [addNotification, commandIds, stopWatching]
  );

  return (
    <>
      <notificationContext.Provider value={contextValue}>
        {children}
      </notificationContext.Provider>
      {deploymentWatchers}
      {commandWatchers}
    </>
  );
}

function updateIdsIfChanged(currentIds, getStored, setIds) {
  const newIds = getStored();
  if (!compareArrays(newIds, currentIds)) {
    setIds(newIds);
  }
}
