import { SnackbarProvider } from "notistack";
import React, { useContext } from "react";
import { DeployAPIProvider } from "../../../api/DeployApi";
import { UserProvider } from "../../../api/UserProvider";
import { userContext } from "@ess-ics/ce-ui-common";
import { useEffectOnMount } from "../../../hooks/MountEffects";
import { notificationContext, NotificationProvider } from "./Notifications";

function AppHarness({ children }) {
  return (
    <SnackbarProvider
      preventDuplicate
      maxSnack="5"
    >
      <DeployAPIProvider>
        <UserProvider>{children}</UserProvider>
      </DeployAPIProvider>
    </SnackbarProvider>
  );
}

function mountIntoHarness(children) {
  cy.mount(<AppHarness>{children}</AppHarness>);
}

function DisplayContextValue({ context }) {
  const { login } = useContext(userContext);
  const contextValue = useContext(context);
  console.debug(contextValue);
  useEffectOnMount(() => login());

  return (
    <pre id="display">
      {JSON.stringify(
        contextValue,
        (key, val) => (typeof val === "function" ? val.toString() : val),
        2
      )}
    </pre>
  );
}

describe("Notifications", () => {
  context("when logged in", () => {
    beforeEach(() => {
      cy.login();
    });

    it("Provides the notification context", () => {
      mountIntoHarness(
        <NotificationProvider>
          <DisplayContextValue context={notificationContext} />
        </NotificationProvider>
      );

      cy.wait("@infoFromUserName");
      cy.wait("@getUserRoles");

      cy.get("#display")
        .contains("notifications")
        .contains("watchDeployment")
        .contains("watchCommand")
        .contains("clearNotifications");
    });
  });
});
