export const WATCHED_DEPLOYMENT_STORAGE_KEY = "WATCHED_DEPLOYMENT_IDS";
export const DEPLOYMENT_NOTIFICATIONS = "DEPLOYMENT_NOTIFICATIONS";
export const WATCHED_COMMAND_STORAGE_KEY = "WATCHED_COMMAND_IDS";
export const CURRENT_NOTIFICATION_VERSION = 1;

export const TYPE_DEPLOYMENT = "deployment";
export const TYPE_COMMAND = "command";

function watchId(watchedId, storageKey) {
  let watchedIds = JSON.parse(localStorage.getItem(storageKey));
  if (watchedId) {
    if (watchedIds) {
      if (!watchedIds.includes(watchedId)) {
        localStorage.setItem(
          storageKey,
          JSON.stringify([watchedId].concat(watchedIds))
        );
      }
    } else {
      localStorage.setItem(storageKey, JSON.stringify([watchedId]));
    }
  }
}

export function removeId(id, storageKey) {
  let watchedIds = JSON.parse(localStorage.getItem(storageKey));
  if (watchedIds) {
    let idsAfterRemove = watchedIds.filter(function (item) {
      return item !== id;
    });
    if (idsAfterRemove.length === 0) {
      localStorage.removeItem(storageKey);
    } else {
      localStorage.setItem(storageKey, JSON.stringify(idsAfterRemove));
    }
  }
}

export function clearAll() {
  localStorage.removeItem(DEPLOYMENT_NOTIFICATIONS);
}

function filterDeprecatedNotifications(notifications) {
  return notifications.filter(
    (notification) => notification.version === CURRENT_NOTIFICATION_VERSION
  );
}

export function getStoredNotifications() {
  const allNotifications =
    JSON.parse(localStorage.getItem(DEPLOYMENT_NOTIFICATIONS)) ?? [];
  let notifications = filterDeprecatedNotifications(allNotifications);
  if (!notifications) {
    return [];
  }
  return notifications;
}

function setDeploymentNotifications(notifications) {
  localStorage.setItem(DEPLOYMENT_NOTIFICATIONS, JSON.stringify(notifications));
}

function getStoredIds(storageKey) {
  let ids = JSON.parse(localStorage.getItem(storageKey));
  return ids ? ids.map(Number) : [];
}

function createNotification(type, id, status, message) {
  if (TYPE_DEPLOYMENT === type) {
    return {
      version: CURRENT_NOTIFICATION_VERSION,
      id: id,
      type: type,
      message: message,
      status: status.toLowerCase(),
      link: "/jobs/" + id
    };
  }

  return {
    version: CURRENT_NOTIFICATION_VERSION,
    id: id,
    type: type,
    message: message,
    status: status.toLowerCase(),
    link: "/jobs/" + id
  };
}

export function appendNotification(type, id, status, message) {
  let notification = createNotification(type, id, status, message);
  let notifications = getStoredNotifications();
  let relatedNotification = notifications.find(
    (n) => n.type === type && n.id === id
  );
  if (relatedNotification) {
    let index = notifications.indexOf(relatedNotification);
    if (index !== -1) {
      notifications[index] = notification;
    }
    setDeploymentNotifications(notifications);
  } else {
    setDeploymentNotifications([notification].concat(notifications));
  }
}

export const watchDeployment = (watchedDeploymentId) =>
  watchId(watchedDeploymentId, WATCHED_DEPLOYMENT_STORAGE_KEY);
export const watchCommand = (watchedCommandId) =>
  watchId(watchedCommandId, WATCHED_COMMAND_STORAGE_KEY);
export const getStoredDeplomentIds = () =>
  getStoredIds(WATCHED_DEPLOYMENT_STORAGE_KEY);
export const getStoredCommandIds = () =>
  getStoredIds(WATCHED_COMMAND_STORAGE_KEY);
