import { useAPIMethod } from "@ess-ics/ce-ui-common/dist/hooks/API";
import { Avatar, Tooltip, styled } from "@mui/material";
import React, { useContext, useEffect, useMemo } from "react";
import { apiContext } from "../../../api/DeployApi";
import { Link } from "react-router-dom";
import { userContext } from "@ess-ics/ce-ui-common/dist/contexts/User";

const unpacker = (data) => {
  if (data) {
    return data[0];
  } else {
    return null;
  }
};

export const UserAvatar = styled(({ username, size = 48 }) => {
  const { user: userChanged } = useContext(userContext);

  const client = useContext(apiContext);

  const params = useMemo(
    () => ({
      user_name: username
    }),
    [username]
  );

  const {
    value: user,
    wrapper: getUser,
    loading,
    error
  } = useAPIMethod({
    fcn: client.apis.Git.infoFromUserName,
    params,
    unpacker
  });

  useEffect(() => {
    getUser();
  }, [userChanged, getUser]);

  const renderedAvatar = (() => {
    if (error || loading || !user?.avatar) {
      return (
        <Avatar
          sx={{ width: size, height: size }}
          alt={username}
          variant="circle"
        >
          {username.toUpperCase().slice(0, 2)}
        </Avatar>
      );
    }
    return (
      <Avatar
        sx={{ width: size, height: size }}
        src={user?.avatar}
        alt={username}
        variant="circle"
      />
    );
  })();

  return (
    <Tooltip title={username}>
      <Link
        to={`/user/${username}`}
        style={{ textDecoration: "none" }}
        aria-label={`User Profile ${username}`}
      >
        {renderedAvatar}
      </Link>
    </Tooltip>
  );
})({});
