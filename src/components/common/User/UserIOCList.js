import React, { useContext } from "react";
import { Card, CardHeader } from "@mui/material";
import { useEffect } from "react";
import { initRequestParams } from "../Helper";
import IOCTable from "../../IOC/IOCTable";
import { apiContext } from "../../../api/DeployApi";
import { useAPIMethod, usePagination } from "@ess-ics/ce-ui-common";

export function UserIocList({ userName }) {
  const client = useContext(apiContext);

  const {
    value: iocs,
    wrapper: getIocs,
    loading,
    dataReady,
    abort
  } = useAPIMethod({
    fcn: client.apis.IOCs.listIocs,
    call: false
  });

  const rowsPerPage = [20, 50];
  const { pagination, setPagination } = usePagination({
    rowsPerPageOptions: rowsPerPage,
    initLimit: rowsPerPage[0],
    initPage: 0
  });

  // update pagination whenever search result total pages change
  useEffect(() => {
    setPagination({ totalCount: iocs?.totalCount ?? 0 });
  }, [setPagination, iocs?.totalCount]);

  // Request more results when pagination changes
  useEffect(() => {
    let requestParams = initRequestParams(pagination);

    requestParams.created_by = userName;

    getIocs(requestParams);

    return () => {
      abort();
    };
  }, [getIocs, pagination, userName, abort]);

  // Invoked by Table on change to pagination
  const onPage = (params) => {
    setPagination(params);
    abort();
  };

  return (
    <Card variant="plain">
      <CardHeader
        title={"IOCs registered"}
        titleTypographyProps={{
          variant: "h2",
          component: "h2"
        }}
      />
      <IOCTable
        iocs={iocs?.iocList}
        loading={loading || !dataReady}
        rowType="explore"
        pagination={pagination}
        onPage={onPage}
      />
    </Card>
  );
}
