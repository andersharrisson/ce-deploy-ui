import React, { useContext } from "react";
import { Card, CardHeader } from "@mui/material";
import { initRequestParams } from "../Helper";
import { useEffect } from "react";
import { JobTable } from "../../Job";
import { apiContext } from "../../../api/DeployApi";
import { useAPIMethod, usePagination } from "@ess-ics/ce-ui-common";

export function UserOperationList({ userName }) {
  const client = useContext(apiContext);

  const {
    value: operations,
    wrapper: getOperations,
    loading,
    dataReady,
    abort
  } = useAPIMethod({
    fcn: client.apis.Deployments.listOperations,
    call: false
  });

  const rowsPerPage = [20, 50];
  const { pagination, setPagination } = usePagination({
    rowsPerPageOptions: rowsPerPage,
    initLimit: rowsPerPage[0],
    initPage: 0
  });

  // update pagination whenever search result total pages change
  useEffect(() => {
    setPagination({ totalCount: operations?.totalCount ?? 0 });
  }, [setPagination, operations?.totalCount]);

  useEffect(() => {
    let requestParams = initRequestParams(pagination);

    requestParams.user = userName;

    getOperations(requestParams);

    return () => {
      abort();
    };
  }, [getOperations, pagination, userName, abort]);

  // Invoked by Table on change to pagination
  const onPage = (params) => {
    setPagination(params);
    abort();
  };

  return (
    <Card variant="plain">
      <CardHeader
        title={"Operations"}
        titleTypographyProps={{
          variant: "h2",
          component: "h2"
        }}
      />
      <JobTable
        jobs={operations?.operations}
        loading={loading || !dataReady}
        pagination={pagination}
        onPage={onPage}
        rowType="userPageJobLog"
      />
    </Card>
  );
}
