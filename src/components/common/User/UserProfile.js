import React from "react";
import {
  Avatar,
  Card,
  CardMedia,
  CardContent,
  Typography
} from "@mui/material";

export function UserProfile({ userInfo }) {
  return (
    <Card
      variant="plain"
      sx={{ display: "flex", flexDirection: "row" }}
    >
      <CardContent>
        <CardMedia>
          <Avatar
            sx={{ width: 96, height: 96 }}
            src={userInfo?.avatar}
            variant="circle"
          />
        </CardMedia>
      </CardContent>
      <CardContent>
        <Typography
          component="h2"
          variant="h2"
        >
          {userInfo?.fullName}
        </Typography>
        <Typography
          component="h3"
          variant="h3"
          fontWeight="bold"
        >
          {`@${userInfo?.loginName}`}
        </Typography>
      </CardContent>
    </Card>
  );
}
