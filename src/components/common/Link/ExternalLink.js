import React from "react";
import { Link, Stack, styled } from "@mui/material";
import LaunchIcon from "@mui/icons-material/Launch";

export const ExternalLinkContents = ({ children }) => {
  return (
    <Stack
      flexDirection="row"
      alignItems="center"
      gap={0.5}
      flexWrap="wrap"
    >
      {children}
      <LaunchIcon fontSize="small" />
    </Stack>
  );
};

const ExternalLink = styled(({ href, children, className, ...props }) => {
  return (
    <Link
      href={href}
      target="_blank"
      rel="noreferrer"
      underline="hover"
      className={className}
      {...props}
    >
      {children}
    </Link>
  );
})({});

export default ExternalLink;
