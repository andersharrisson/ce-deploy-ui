import React, { CircularProgress, Fade, TextField } from "@mui/material";
import { Check } from "@mui/icons-material";
import { useEffect, useState } from "react";
import { useEffectAfterMount } from "../../../hooks/MountEffects";
import { useTypingTimer } from "../SearchBoxFilter/TypingTimer";

export function SearchBar({
  search,
  query,
  loading = false,
  placeholder = "Search",
  children
}) {
  const [value, setValue] = useState(query);
  const [queryString, stillTyping] = useTypingTimer({ init: value });
  const [showAnimation, setShowAnimation] = useState(false);
  const [checkMarkOn, setCheckMarkOn] = useState(false);

  useEffect(() => {
    search(queryString);
  }, [search, queryString]);
  useEffectAfterMount(() => {
    !showAnimation && !loading && setShowAnimation(true);
  }, [showAnimation, loading, setShowAnimation]);

  const solidTime = 1250;
  const fadeTime = 1000;
  useEffect(() => {
    if (!loading) {
      setCheckMarkOn(true);
      const timeout = setTimeout(() => {
        setCheckMarkOn(false);
      }, solidTime);
      return () => clearTimeout(timeout);
    }
  }, [loading]);

  useEffect(() => {
    setValue(query);
  }, [query]);

  const Loading = (
    <Fade in>
      <CircularProgress color="primary" />
    </Fade>
  );

  const Finished = (
    <Fade
      in={checkMarkOn}
      timeout={fadeTime}
      enter={false}
    >
      <Check color="primary" />
    </Fade>
  );

  const LoadingAnimation = loading ? Loading : Finished;

  return (
    <>
      <TextField
        fullWidth
        variant="outlined"
        label={placeholder}
        onKeyUp={stillTyping}
        value={value}
        onChange={(newValue) => setValue(newValue.target.value)}
        InputProps={{
          endAdornment: showAnimation && LoadingAnimation
        }}
        sx={{ mb: 3 }}
      />
      {children}
    </>
  );
}
