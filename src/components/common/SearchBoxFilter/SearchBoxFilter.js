import React, { useState, useEffect } from "react";
import { styled } from "@mui/material/styles";
import { Container, List, ListItem, Paper, TextField } from "@mui/material";
import { useTypingTimer } from "./TypingTimer";

const PREFIX = "SearchBoxFilter";

const classes = {
  searchText: `${PREFIX}-searchText`
};

const StyledSearchBoxFilter = styled(SearchBoxFilter)(({ theme }) => ({
  [`& .${classes.searchText}`]: {
    fontFamily: "Segoe UI"
  }
}));

export function SearchBoxFilter({ items, filter, render }) {
  const [filteredItems, setFilteredItems] = useState(items);
  const [search, searchKeyPress] = useTypingTimer();

  const filterItems = () => {
    const f = filter(search);
    const subset = items.filter(f);
    setFilteredItems(subset);
  };

  useEffect(filterItems, [items, search, filter]);

  return (
    <StyledSearchBoxFilter>
      <Container>
        <TextField
          className={classes.searchText}
          fullWidth
          variant="outlined"
          label="Search"
          onKeyUp={searchKeyPress}
        />
        <div>&nbsp;</div>
        {render(filteredItems)}
      </Container>
    </StyledSearchBoxFilter>
  );
}

export function SearchBoxFilterDemo() {
  const items = ["Big Dog", "Big Cat", "Small Dog", "Small Cat"];

  const filter = (search) => {
    return (item) => {
      return item.toLowerCase().includes(search);
    };
  };

  function MyList({ items }) {
    return (
      <List>
        {items &&
          items.map((item) => {
            return (
              <Paper key={item}>
                <ListItem button>{item}</ListItem>
              </Paper>
            );
          })}
      </List>
    );
  }

  const renderFilteredItems = (items) => {
    return <MyList items={items} />;
  };

  return (
    <SearchBoxFilter
      items={items}
      filter={filter}
      render={renderFilteredItems}
    />
  );
}
