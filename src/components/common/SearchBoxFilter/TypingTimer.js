import { useState } from "react";

export function useTypingTimer({ init = "", interval = 750 } = {}) {
  const [typingTimer, setTypingTimer] = useState();
  const [value, setValue] = useState(init);
  const doneTypingInterval = interval; // ms

  const onKeyUp = (event) => {
    const { target, key } = event;
    clearTimeout(typingTimer);
    if (key === "Enter") {
      setValue(target.value);
      return;
    } else {
      const timer = setTimeout(
        () => setValue(target.value),
        doneTypingInterval
      );
      setTypingTimer(timer);
    }
  };

  return [value, onKeyUp];
}
