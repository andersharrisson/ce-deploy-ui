import React, { useContext, useEffect, useCallback, useState } from "react";
import { CookiesProvider } from "react-cookie";
import { useCookies } from "react-cookie";
import { userContext } from "@ess-ics/ce-ui-common";
import { useEffectOnMount } from "../../../hooks/MountEffects";
// import { useRedirect } from "../../../hooks/Redirect";
import { useNavigate } from "react-router-dom";

export function LoginSuggester({ children }) {
  const navigate = useNavigate();
  const goToLogin = useCallback(() => {
    navigate("/login");
  }, [navigate]);

  return (
    <CookiesProvider>
      <LoginSuggesterMechanics
        visitedCookieName="ce-return-customer"
        loggedInCookieName="ce-has-logged-in"
        loginAction={goToLogin}
      />
      {children}
    </CookiesProvider>
  );
}

export function LoginSuggesterMechanics({
  visitedCookieName,
  loggedInCookieName,
  loginAction
}) {
  const { user } = useContext(userContext);
  const [cookies, setCookie] = useCookies([
    visitedCookieName,
    loggedInCookieName
  ]);
  const [loginSuggested, setLoginSuggested] = useState(false);

  useEffectOnMount(() => {
    let suggestLogin = false;

    // check if it's the first time a user has visited
    // the app or they have logged in in the past.
    // If so, redirect them to the login page.
    if (!(visitedCookieName in cookies)) {
      setCookie(visitedCookieName, true);
      suggestLogin = true;
    } else if (loggedInCookieName in cookies) {
      if (!user) {
        suggestLogin = true;
      }
    }

    // Don't suggest login if the user has refreshed the page
    const navigationType =
      window.performance.getEntriesByType("navigation")[0].type;
    suggestLogin &= navigationType !== "reload";

    if (suggestLogin) {
      setLoginSuggested(true);
    }
  });

  useEffect(() => {
    if (loginSuggested) {
      loginAction();
      setLoginSuggested(false);
    }
  }, [loginSuggested, setLoginSuggested, loginAction]);

  // When people log in, set a cookie
  // we can use to suggest login the next time.
  useEffect(() => {
    if (user) {
      setCookie(loggedInCookieName, true);
    }
  }, [setCookie, user, loggedInCookieName]);

  return null;
}
