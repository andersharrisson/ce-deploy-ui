import React from "react";
import { styled } from "@mui/material/styles";
import clsx from "clsx";
import {
  Container,
  Drawer,
  IconButton,
  Box,
  Tooltip,
  useTheme
} from "@mui/material";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
const PREFIX = "MenuDrawer";

const classes = {
  footer: `${PREFIX}-footer`,
  drawer: `${PREFIX}-drawer`,
  drawerOpen: `${PREFIX}-drawerOpen`,
  drawerClose: `${PREFIX}-drawerClose`,
  toggleButton: `${PREFIX}-toggleButton`
};

// TODO jss-to-styled codemod: The Fragment root was replaced by div. Change the tag if needed.
const Root = styled("div")(({ theme }) => ({
  [`& .${classes.footer}`]: {
    position: "fixed",
    bottom: 0,
    paddingBottom: theme.spacing(2)
  },

  [`& .${classes.drawer}`]: {
    width: theme.drawer.widthOpen,
    flexShrink: 0,
    whiteSpace: "nowrap"
  },

  [`& .${classes.drawerOpen}`]: {
    width: theme.drawer.widthOpen,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },

  [`& .${classes.drawerClose}`]: {
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    overflowX: "hidden",
    width: theme.drawer.widthClose,
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9) + 1
    }
  },

  [`& .${classes.toggleButton}`]: {
    marginRight: theme.spacing(1)
  }
}));

export function MenuDrawer({ open, toggleDrawer, children }) {
  const theme = useTheme();

  const handleDrawerClose = () => {
    toggleDrawer();
  };

  return (
    <Root>
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open
          })
        }}
        open={open}
        onClose={toggleDrawer}
        ModalProps={{
          keepMounted: true // Better open performance on mobile.
        }}
      >
        <Container disableGutters>
          <div style={{ ...theme.mixins.toolbar }} />
          <div
            style={{
              paddingTop: theme.spacing(2),
              paddingBottom: theme.spacing(2)
            }}
          >
            {children}
          </div>
          <div className={classes.footer}>
            <Box
              display="flex"
              className={clsx(classes.drawer, {
                [classes.drawerOpen]: open,
                [classes.drawerClose]: !open
              })}
              flexDirection="row-reverse"
              p={1}
              m={1}
            >
              <Tooltip
                title={open ? "Collapse sidebar" : "Expand sidebar"}
                placement="right"
                arrow
              >
                <IconButton
                  onClick={handleDrawerClose}
                  className={classes.toggleButton}
                  size="large"
                >
                  {open ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                </IconButton>
              </Tooltip>
            </Box>
          </div>
        </Container>
      </Drawer>
    </Root>
  );
}
