/**
 * NotFound
 * when page not found go (redirect) home ("/")
 */
import React from "react";
import { string } from "prop-types";
import { ServerErrorPage, RootPaper } from "@ess-ics/ce-ui-common";

const propTypes = {
  /** String containing message of page not found. Otherwise defaults to a generic "Page Not Found" message */
  message: string,
  status: string
};

export default function NotFoundView({ message, status }) {
  return (
    <RootPaper>
      <ServerErrorPage
        status={status}
        message={message}
        supportHref={window.SUPPORT_URL}
      />
    </RootPaper>
  );
}

NotFoundView.propTypes = propTypes;

NotFoundView.defaultProps = {
  status: "404"
};
