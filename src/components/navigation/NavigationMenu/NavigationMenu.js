import { GlobalAppBar, IconMenuButton } from "@ess-ics/ce-ui-common";
import {
  Assignment,
  HelpCenter,
  SettingsInputComponent,
  Storage,
  TrendingUp
} from "@mui/icons-material";
import {
  Box,
  Divider,
  IconButton,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Tooltip
} from "@mui/material";
import React, { Fragment, useState } from "react";
import { useNavigate } from "react-router";
import { applicationTitle } from "../../common/Helper";
import { LoginControls } from "./LoginControls";
import { Link } from "react-router-dom";
import { CCCEControlSymbol } from "../../../icons/CCCEControlSymbol";
import { CreateIOCButton } from "./CreateIOCButton";
import { ReactComponent as EssLogo } from "../../../logoEssWhite.svg";

function MenuListItem({ url, icon, text, tooltip }) {
  const currentUrl = `${window.location}`;

  return (
    <Tooltip
      title={tooltip ? text : ""}
      placement="right"
      arrow
    >
      <ListItemButton
        component={Link}
        to={url}
        selected={currentUrl.split("?")[0].endsWith(url)}
      >
        <ListItemIcon>{icon}</ListItemIcon>
        <ListItemText
          primary={text}
          primaryTypographyProps={{ variant: "button" }}
        />
      </ListItemButton>
    </Tooltip>
  );
}

function MenuListItems({ menuItems, drawerOpen }) {
  return (
    <Box sx={{ paddingTop: 3 }}>
      {menuItems.map(({ text, url, icon }, index) => (
        <Fragment key={index}>
          {typeof (text && url && icon) === "undefined" ? (
            <Divider sx={{ marginTop: 5 }} />
          ) : (
            <MenuListItem
              tooltip={!drawerOpen}
              url={url}
              icon={icon}
              text={text}
            />
          )}
        </Fragment>
      ))}
    </Box>
  );
}
const makeLink = (text, url, icon) => ({ text, url, icon });
const menuItemsAll = [
  makeLink("Records", "/records", <SettingsInputComponent />),
  makeLink("IOCs", "/iocs", <CCCEControlSymbol />),
  makeLink("IOC hosts", "/hosts", <Storage />),
  makeLink("Log", "/jobs", <Assignment />),
  makeLink("Statistics", "/statistics", <TrendingUp />)
];

const NavigationMenu = ({ children }) => {
  const [drawerOpen, setDrawerOpen] = useState(false);
  const navigate = useNavigate();
  const goHome = () => {
    navigate("/");
  };

  const helpButtonProps = {
    icon: <HelpCenter />,
    menuItems: [
      {
        text: "Help",
        action: () => navigate("/help")
      }
    ]
  };

  const args = {
    defaultHomeButton: (
      <IconButton
        edge="start"
        color="inherit"
        onClick={goHome}
      >
        <EssLogo
          width="36px"
          height="36px"
        />
      </IconButton>
    ),
    defaultTitle: applicationTitle(),
    defaultActionButton: <LoginControls />,
    defaultOpen: false,
    widthOpen: "170px", // default size 240px
    widthClosed: "57px",
    defaultIconMenuButton: <CreateIOCButton />,
    defaultHelpButton: <IconMenuButton {...helpButtonProps} />
  };

  return (
    <GlobalAppBar
      getDrawerOpen={setDrawerOpen}
      menuItems={
        <MenuListItems
          drawerOpen={drawerOpen}
          menuItems={menuItemsAll}
        />
      }
      {...args}
    >
      {children}
    </GlobalAppBar>
  );
};

export default NavigationMenu;
