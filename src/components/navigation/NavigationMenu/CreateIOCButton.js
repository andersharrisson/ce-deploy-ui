import React, { useContext } from "react";
import { userContext, IconMenuButton } from "@ess-ics/ce-ui-common";
import { useNavigate } from "react-router-dom";

export function CreateIOCButton() {
  const { user } = useContext(userContext);
  const navigate = useNavigate();

  const iconMenuButtonProps = {
    menuItems: [
      {
        text: "New IOC",
        action: () => navigate("/iocs/create")
      }
    ]
  };

  return user ? <IconMenuButton {...iconMenuButtonProps} /> : null;
}
