import React from "react";
import { ServerErrorPage } from "@ess-ics/ce-ui-common";

export default function AccessDenied() {
  return (
    <ServerErrorPage
      message="You do not have permission to access this page. Please contact an administrator to request access"
      status={401}
      supportHref={window.SUPPORT_URL}
    />
  );
}
