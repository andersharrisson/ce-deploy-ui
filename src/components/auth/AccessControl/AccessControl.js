import React, { useContext } from "react";
import { userContext } from "@ess-ics/ce-ui-common";
import AccessDenied from "../AccessDenied";

const checkPermissions = (userRoles, allowedRoles) => {
  if (allowedRoles.length === 0) {
    return true;
  }

  return userRoles?.some((role) => allowedRoles.includes(role));
};

const checkUser = (user, userRoles, allowedUsersWithRoles) => {
  if (!allowedUsersWithRoles || allowedUsersWithRoles.length === 0) {
    return false;
  }

  return allowedUsersWithRoles.find(
    (userWithRole) =>
      userWithRole.user === user?.loginName &&
      userRoles.includes(userWithRole.role)
  );
};

export default function AccessControl({
  allowedRoles,
  allowedUsersWithRoles,
  children,
  renderNoAccess
}) {
  const { user, userRoles } = useContext(userContext);

  const permitted =
    checkPermissions(userRoles, allowedRoles) ||
    checkUser(user, userRoles, allowedUsersWithRoles);
  if (permitted) {
    return children;
  }
  if (!renderNoAccess) {
    return <AccessDenied />;
  }
  return renderNoAccess();
}
