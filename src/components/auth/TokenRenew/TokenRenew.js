import { useContext, useCallback } from "react";
import { apiContext } from "../../../api/DeployApi";
import { userContext, useAPIMethod } from "@ess-ics/ce-ui-common";
import { useSafePolling } from "../../../hooks/Polling";

export default function TokenRenew() {
  const { user } = useContext(userContext);

  const client = useContext(apiContext);
  const {
    wrapper: renewToken,
    loading,
    abort
  } = useAPIMethod({
    fcn: client.apis.Authentication.tokenRenew,
    call: false
  });

  const renewTokenWhenLoggedIn = useCallback(() => {
    if (user) {
      renewToken();

      return () => {
        abort();
      };
    }
  }, [user, renewToken, abort]);

  useSafePolling(renewTokenWhenLoggedIn, loading, window.TOKEN_RENEW_INTERVAL);

  return null;
}
