import { SnackbarProvider } from "notistack";
import React from "react";
import { UserProvider } from "../../../api/UserProvider";
import TokenRenew from ".";
import { DeployAPIProvider } from "../../../api/DeployApi";

function AppHarness({ children }) {
  return (
    <SnackbarProvider
      preventDuplicate
      maxSnack="5"
    >
      <DeployAPIProvider>
        <UserProvider>{children}</UserProvider>
      </DeployAPIProvider>
    </SnackbarProvider>
  );
}

function mountIntoHarness(children) {
  cy.mount(<AppHarness>{children}</AppHarness>);
}

describe("TokenRenew", () => {
  context("when logged in", () => {
    beforeEach(() => {
      cy.login();
    });

    it("renews the token periodically", () => {
      cy.clock();
      mountIntoHarness(<TokenRenew />);
      cy.tick(window.TOKEN_RENEW_INTERVAL);
      cy.wait("@renew", { responseTimeout: 1000 });
      cy.getCookie("ce-deploy-auth").should(
        "have.property",
        "value",
        "FADEDEAD"
      );
    });
  });
});
