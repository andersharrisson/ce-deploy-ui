import React from "react";
import { IconBadge } from "../common/Badge/IconBadge";
import { HostStatusIcon } from "./HostIcons";
import { extractMainNetwork } from "../common/Helper";

export function HostBadge({ host, ...rest }) {
  return (
    <IconBadge
      icon={<HostStatusIcon host={host} />}
      title={host.csEntryHost?.fqdn}
      subheader={extractMainNetwork(host?.csEntryHost?.interfaces, "---")}
      {...rest}
    />
  );
}
