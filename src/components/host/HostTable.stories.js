import React from "react";
import { Container } from "@mui/material";
import hosts from "../../mocks/fixtures/PagedCSEntryHostResponse.json";
import { HostTable } from "./HostTable";
import { RouterHarness } from "../../mocks/AppHarness";

export default {
  title: "Host/HostTable",
  component: HostTable
};

const Template = (args) => (
  <RouterHarness>
    <Container>
      <HostTable {...args} />
    </Container>
  </RouterHarness>
);

export const Empty = Template.bind({});
Empty.args = {
  loading: false,
  hosts: [],
  totalCount: 0,
  rowsPerPage: [5, 10, 20, 50, 100],
  lazyParams: { rows: 10, page: 0 },
  columnSort: { sortField: null, sortOrder: null },
  setLazyParams: () => {},
  setColumnSort: () => {}
};

export const EmptyLoading = Template.bind({});
EmptyLoading.args = { ...Empty.args, loading: true };

export const Populated = Template.bind({});
Populated.args = {
  ...Empty.args,
  totalCount: hosts.totalCount,
  hosts: hosts.csEntryHosts
};
