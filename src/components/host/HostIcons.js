import React from "react";
import { useTheme } from "@mui/material";
import {
  Brightness1,
  ErrorOutline,
  Error,
  RadioButtonUnchecked,
  HelpOutline
} from "@mui/icons-material";
import LabeledIcon from "../common/LabeledIcon";

export function HostStatusIcon({ host }) {
  const theme = useTheme();

  let { status, alertSeverity, alerts } = host;

  const iconConfig = {
    available: {
      title: "Active",
      icon: Brightness1
    },
    alert: {
      title: "Alert",
      icon: Error
    },
    inactive: {
      title: "Inactive",
      icon: RadioButtonUnchecked
    },
    "inactive alert": {
      title: "Alert",
      icon: ErrorOutline
    },
    null: {
      title: "Unknown",
      icon: HelpOutline
    }
  };

  let state = null;
  status = status?.toLowerCase();

  if (typeof alerts === "object") alertSeverity = alerts?.[0]?.type;

  alertSeverity = alertSeverity?.toLowerCase();

  if (
    status === "available" &&
    (alertSeverity === undefined || alertSeverity === "info")
  ) {
    // Available status / no error or warning
    state = "available";
  } else if (
    status === "available" &&
    (alertSeverity === "warning" || alertSeverity === "error")
  ) {
    // Available status / with error or warning
    state = "alert";
  } else if (
    status === "unreachable" &&
    (alertSeverity === undefined || alertSeverity === "info")
  ) {
    // Unreachable status / no error or warning
    state = "inactive";
  } else if (
    status === "unreachable" &&
    (alertSeverity === "warning" || alertSeverity === "error")
  ) {
    // Unreachable status / with error or warning
    state = "inactive alert";
  } else {
    // unknown fallback state
    state = null;
  }

  const iconStyle = { fill: theme.palette.status.icons };
  const iconTitle = iconConfig[state].title;
  const statusIcon = iconConfig[state].icon;

  return (
    <LabeledIcon
      label={iconTitle}
      labelPosition="tooltip"
      Icon={statusIcon}
      IconProps={{ style: iconStyle }}
    />
  );
}
