import React from "react";
import { Box, Tooltip, Link } from "@mui/material";
import { Table } from "@ess-ics/ce-ui-common";
import { HostStatusIcon } from "./HostIcons";
import { noWrapText } from "../common/Helper";

const columns = [
  {
    field: "bulb",
    headerName: "Status",
    flex: 0,
    headerAlign: "center",
    align: "center"
  },
  { field: "hostName", headerName: "Host" },
  { field: "description", headerName: "Description" },
  { field: "network", headerName: "Network" },
  { field: "scope", headerName: "Network scope" }
];

export function rowDescription(description) {
  return (
    <Box
      sx={{
        textOverflow: "ellipsis",
        whiteSpace: "nowrap",
        overflow: "hidden"
      }}
    >
      <Tooltip
        title={description}
        arrow
        enterDelay={400}
      >
        <label>{noWrapText(description)}</label>
      </Tooltip>
    </Box>
  );
}

export function createRow(hostContainer) {
  const host = hostContainer.csEntryHost;
  return {
    id: host.id,
    bulb: <HostStatusIcon host={hostContainer} />,
    hostName: <Link href={`/hosts/${host.id}`}> {noWrapText(host.name)} </Link>,
    host: host,
    description: rowDescription(host.description),
    network: noWrapText(host.network),
    scope: noWrapText(host.scope),
    discrepancy: hostContainer.alertSeverity === "WARNING",
    inconsistentState: hostContainer.alertSeverity === "ERROR",
    shortenLongData: true,
    iocNotDeployed: !hostContainer.iocDeployed
  };
}

export function HostTable({ hosts, pagination, onPage, loading }) {
  return (
    <Table
      columns={columns}
      rows={hosts.map((host) => createRow(host))}
      pagination={pagination}
      onPage={onPage}
      loading={loading}
      disableHover
    />
  );
}
