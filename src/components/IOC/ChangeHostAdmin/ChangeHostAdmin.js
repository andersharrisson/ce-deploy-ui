import React, {
  useState,
  useEffect,
  useCallback,
  useMemo,
  useContext
} from "react";
import AccessControl from "../../auth/AccessControl";
import { ConfirmationDialog, useAPIMethod } from "@ess-ics/ce-ui-common";
import {
  Box,
  Button,
  Typography,
  Grid,
  Tooltip,
  TextField,
  CircularProgress,
  Alert,
  Autocomplete
} from "@mui/material";
import { useTypingTimer } from "../../common/SearchBoxFilter/TypingTimer";
import { transformHostQuery } from "../../common/Helper";
import { apiContext } from "../../../api/DeployApi";

export default function ChangeHostAdmin({
  ioc,
  getIOC,
  resetTab,
  buttonDisabled,
  setButtonDisabled
}) {
  const initHost = useMemo(
    () => ({
      csEntryHost: {
        fqdn: ioc.activeDeployment.host.fqdn,
        id: ioc.activeDeployment.host.csEntryId
      }
    }),
    [ioc?.activeDeployment?.host]
  );
  const [host, setHost] = useState(initHost);

  const client = useContext(apiContext);

  const {
    value: hosts,
    wrapper: getHosts,
    loading: loadingHosts
  } = useAPIMethod({
    fcn: client.apis.Hosts.listHosts,
    call: false
  });

  const [query, onHostKeyUp] = useTypingTimer({ interval: 500 });

  const noModification = useCallback(
    () => !host || host.csEntryHost.id === ioc.activeDeployment.host.csEntryId,
    [host, ioc]
  );

  // for the dialog
  const [error, setError] = useState();
  const [open, setOpen] = useState(false);

  const {
    value: updatedIoc,
    wrapper: updateHost,
    error: updateHostError
  } = useAPIMethod({
    fcn: client.apis.IOCs.updateActiveDeploymentHost,
    call: false
  });

  useEffect(() => {
    if (updateHostError) {
      setButtonDisabled(false);
      setError(updateHostError?.message);
    }
  }, [updateHostError, setError, setButtonDisabled]);

  useEffect(() => {
    if (updatedIoc) {
      getIOC();
      resetTab();
      setButtonDisabled(false);
    }
  }, [updatedIoc, getIOC, resetTab, setButtonDisabled]);

  useEffect(() => {
    getHosts({ query: transformHostQuery(`${query}`) });
  }, [query, getHosts]);

  const onClose = useCallback(() => {
    setOpen(false);
    setHost(initHost);
  }, [setOpen, initHost]);

  const onConfirm = useCallback(() => {
    setButtonDisabled(true);
    updateHost(
      {
        ioc_id: ioc.id
      },
      {
        requestBody: {
          hostCSEntryId: host.csEntryHost.id
        }
      }
    );
  }, [updateHost, ioc, host?.csEntryHost?.id, setButtonDisabled]);

  let disabledButtonTitle = "";
  if (buttonDisabled || ioc.operationInProgress) {
    disabledButtonTitle =
      "There is an ongoing operation, you cannot 'undeploy' the IOC right now";
  } else {
    if (!ioc.activeDeployment) {
      disabledButtonTitle = "IOC has no active deployment";
    }
  }

  return (
    <>
      <AccessControl
        allowedRoles={["DeploymentToolAdmin"]}
        renderNoAccess={() => <></>}
      >
        <ConfirmationDialog
          title={
            <Typography
              variant="h2"
              marginY={1}
            >
              Modifying deployment host
            </Typography>
          }
          content={
            <>
              <Typography component="span">
                Are you sure want to modify deployment host of
                <Typography
                  component="span"
                  fontFamily="monospace"
                >
                  {" "}
                  {ioc.namingName}
                </Typography>{" "}
                ?
              </Typography>
            </>
          }
          confirmText="Modify Host"
          cancelText="Cancel"
          open={open}
          onClose={onClose}
          onConfirm={onConfirm}
        />
        <Box sx={{ pt: 2 }}>
          <Typography
            sx={{ my: 2.5 }}
            variant="h3"
          >
            Change deployment host
          </Typography>
          <Grid
            container
            spacing={1}
          >
            {error ? (
              <Grid
                item
                xs={12}
              >
                <Alert severity="error">{error}</Alert>
              </Grid>
            ) : (
              <></>
            )}
            <Grid
              item
              xs={12}
            >
              <Autocomplete
                autoHighlight
                id="host"
                options={query ? hosts?.csEntryHosts ?? [] : []}
                loading={loadingHosts}
                clearOnBlur={false}
                value={host}
                getOptionLabel={(option) => {
                  return option?.csEntryHost?.fqdn ?? "";
                }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="host"
                    variant="outlined"
                    required
                    InputProps={{
                      ...params.InputProps,
                      endAdornment: (
                        <React.Fragment>
                          {loadingHosts ? (
                            <CircularProgress
                              color="inherit"
                              size={20}
                            />
                          ) : null}
                          {params.InputProps.endAdornment}
                        </React.Fragment>
                      )
                    }}
                  />
                )}
                onChange={(event, value, reason) => {
                  setHost(value);
                }}
                onInputChange={(event, value, reason) => {
                  event && onHostKeyUp(event.nativeEvent);
                }}
                autoSelect
                filterOptions={(options, state) => options}
              />
            </Grid>
            <Grid
              item
              xs={12}
            >
              <Tooltip title={disabledButtonTitle}>
                <span>
                  <Button
                    color="primary"
                    variant="contained"
                    onClick={() => setOpen(true)}
                    disabled={
                      buttonDisabled ||
                      ioc.operationInProgress ||
                      !ioc.activeDeployment ||
                      noModification()
                    }
                  >
                    CHANGE HOST
                  </Button>
                </span>
              </Tooltip>
            </Grid>
          </Grid>
        </Box>
      </AccessControl>
    </>
  );
}
