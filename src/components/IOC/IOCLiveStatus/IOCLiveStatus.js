import React, { useCallback } from "react";
import { Typography, Link as MuiLink } from "@mui/material";
import { SimpleAccordion } from "@ess-ics/ce-ui-common";
import { IOCDetails } from "../IOCDetails";
import { Link as ReactRouterLink } from "react-router-dom";
import { LokiContainer } from "../../common/Loki/LokiContainer";
import { RecordSearch } from "../../records/RecordSearch";
import GitRefLink from "../GitRefLink";
import { AlertBannerList } from "@ess-ics/ce-ui-common";
import AccessControl from "../../auth/AccessControl";
import { IocActiveDeployment } from "../../../api/DataTypes";
import useUrlState from "@ahooksjs/use-url-state";
import { serialize, deserialize } from "../../common/URLState/URLState";

export function IOCLiveStatus({ ioc }) {
  const [state, setState] = useUrlState(
    {
      procserv_log_open: "true",
      records_open: "false"
    },
    { navigateMode: "replace" }
  );

  const liveIOC = { ...ioc };
  liveIOC.name = ioc.namingName;

  const getSubset = useCallback((ioc) => {
    const iocHasDeployment = new IocActiveDeployment(
      ioc.activeDeployment
    ).hasDeployment();

    // if IOC is not deployed some fields has to show '---' value
    const grayOutNoText = <Typography color="gray">---</Typography>;

    let subset = {
      Description: ioc.description,
      Revision: iocHasDeployment ? (
        <GitRefLink
          url={ioc.activeDeployment?.sourceUrl}
          revision={ioc.activeDeployment?.sourceVersion}
        />
      ) : (
        grayOutNoText
      ),
      "Deployed on":
        iocHasDeployment && ioc.activeDeployment?.host.csEntryIdValid ? (
          <Typography>
            {ioc.activeDeployment?.host.csEntryId ? (
              <MuiLink
                component={ReactRouterLink}
                to={`/hosts/${ioc.activeDeployment?.host.csEntryId}`}
                underline="hover"
              >
                {ioc.activeDeployment?.host.fqdn}
              </MuiLink>
            ) : (
              ioc.activeDeployment?.host.fqdn
            )}
          </Typography>
        ) : (
          grayOutNoText
        ),
      "Created by": (
        <MuiLink
          component={ReactRouterLink}
          to={`/user/${ioc.createdBy}`}
          underline="hover"
        >
          {ioc.createdBy}
        </MuiLink>
      )
    };

    return subset;
  }, []);

  function isIocDeployed(ioc) {
    return Boolean(ioc.activeDeployment);
  }

  return (
    <>
      <IOCDetails
        ioc={liveIOC}
        getSubset={getSubset}
        alert={<AlertBannerList alerts={liveIOC.alerts} />}
      />
      {liveIOC.activeDeployment?.host?.csEntryIdValid && (
        <AccessControl
          allowedRoles={["DeploymentToolAdmin", "DeploymentToolIntegrator"]}
          renderNoAccess={() => <></>}
        >
          <LokiContainer
            csEntryId={liveIOC.activeDeployment?.host.csEntryId}
            iocName={ioc.namingName}
            isDeployed={isIocDeployed(ioc)}
          />
          <SimpleAccordion
            summary="Records"
            expanded={deserialize(state.records_open)}
            onChange={(event, expanded) =>
              setState({ records_open: serialize(expanded) })
            }
          >
            <RecordSearch
              iocName={ioc.namingName}
              rowType="iocDetails"
            />
          </SimpleAccordion>
        </AccessControl>
      )}
    </>
  );
}
