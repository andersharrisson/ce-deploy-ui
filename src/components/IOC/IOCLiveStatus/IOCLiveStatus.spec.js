import React, { useContext, useMemo } from "react";
import { AppHarness } from "../../../mocks/AppHarness";
import { IOCLiveStatus } from ".";
import { apiContext } from "../../../api/DeployApi";
import { useAPIMethod } from "@ess-ics/ce-ui-common/dist/hooks/API";

function mountIntoHarness(children) {
  cy.mount(<AppHarness>{children}</AppHarness>);
}

function IOCLiveStatusContainer() {
  const client = useContext(apiContext);

  const params = useMemo(
    () => ({
      ioc_id: 2
    }),
    []
  );

  const { value: ioc } = useAPIMethod({
    fcn: client.apis.IOCs.getIoc,
    params
  });

  return ioc ? <IOCLiveStatus ioc={ioc} /> : null;
}

function commonTests() {
  // Expect IOC name in badge
  cy.contains("CCCE:SC-IOC-001");

  // Expected in Info table:
  cy.contains("ee3ae2a8"); // git reference
  cy.contains("Test IOC for CCCE"); // description
  cy.contains("johnsparger"); // created by
  cy.contains("ccce-w40.cslab.esss.lu.se"); // deployed on

  // Expect alerts to be displayed
  cy.get("#ioc-alerts")
    .should("contain", "Example alert without link")
    .and("contain", "Example alert with link");

  cy.get('#ioc-alerts a[href="https://google.com"]').contains("More Info", {
    matchCase: false
  });
}

describe("IOCLiveStatus", () => {
  context("when not logged in", () => {
    it("displays the common components", () => {
      mountIntoHarness(<IOCLiveStatusContainer />);
      commonTests();
    });
  });

  context("when logged in", () => {
    beforeEach(() => {
      cy.login();
      mountIntoHarness(<IOCLiveStatusContainer />);
    });

    it("displays the logged in view", () => {
      commonTests();
      // check for procserv logs
      cy.wait("@getProcservLogs");
      cy.contains("This is some fake log data");
    });
  });
});
