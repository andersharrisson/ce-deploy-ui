import React, {
  useState,
  useEffect,
  useCallback,
  useContext,
  useMemo
} from "react";
import { useNavigate } from "react-router-dom";
import {
  Box,
  Button,
  Typography,
  Grid,
  Tooltip,
  LinearProgress
} from "@mui/material";
import { ConfirmationDialog, useAPIMethod } from "@ess-ics/ce-ui-common";
import Alert from "@mui/material/Alert";
import AccessControl from "../../auth/AccessControl";
import { IocActiveDeployment } from "../../../api/DataTypes";
import { apiContext } from "../../../api/DeployApi";

export default function AdministerUndeployment({ ioc, buttonDisabled }) {
  const navigate = useNavigate();

  // for the dialog
  const [error, setError] = useState();
  const [open, setOpen] = useState(false);

  const client = useContext(apiContext);

  const params = useMemo(
    () => ({
      ioc_id: ioc?.id
    }),
    [ioc]
  );

  const {
    value: response,
    wrapper: undeployIOC,
    loading,
    error: errorResponse
  } = useAPIMethod({
    fcn: client.apis.IOCs.unDeployInDb,
    call: false,
    params
  });

  useEffect(() => {
    setError(errorResponse?.message);
  }, [errorResponse]);

  const onClose = useCallback(() => {
    setOpen(false);
  }, []);

  const onConfirm = useCallback(() => {
    undeployIOC();
  }, [undeployIOC]);

  useEffect(() => {
    if (response) {
      setOpen(false);
      navigate(-1);
    }
  }, [response, navigate]);

  const hasDeployment = new IocActiveDeployment(
    ioc.activeDeployment
  ).hasDeployment();

  let disabledButtonTitle = "";
  if (buttonDisabled || ioc.operationInProgress) {
    disabledButtonTitle =
      "There is an ongoing operation, you cannot 'undeploy' the IOC right now";
  } else {
    if (!hasDeployment) {
      disabledButtonTitle = "IOC is not deployed";
    }
  }

  return (
    <AccessControl
      allowedRoles={["DeploymentToolAdmin"]}
      renderNoAccess={() => <></>}
    >
      <>
        <ConfirmationDialog
          title={
            <Typography
              variant="h2"
              marginY={1}
            >
              Administratively undeploy IOC
            </Typography>
          }
          content={
            <>
              <Typography component="span">
                Are you sure want to administer IOC as undeployed
                <Typography
                  fontFamily="monospace"
                  component="span"
                >
                  {" "}
                  {ioc.namingName}
                </Typography>{" "}
                ?
              </Typography>
              {loading ? <LinearProgress /> : null}
            </>
          }
          confirmText="Undeploy IOC"
          cancelText="Cancel"
          open={open}
          onClose={onClose}
          onConfirm={onConfirm}
        />
        <Box sx={{ pt: 2 }}>
          <Typography
            sx={{ my: 2.5 }}
            variant="h3"
          >
            Change deployment status to not deployed (e.g. if IOC has been
            manually removed)
          </Typography>
          <Grid
            container
            spacing={1}
          >
            {error ? (
              <Grid
                item
                xs={12}
              >
                <Alert severity="error">{error}</Alert>
              </Grid>
            ) : (
              <></>
            )}
            <Grid
              item
              xs={12}
            >
              <Tooltip title={disabledButtonTitle}>
                <span>
                  <Button
                    onClick={() => setOpen(true)}
                    disabled={
                      buttonDisabled ||
                      ioc.operationInProgress ||
                      !hasDeployment ||
                      loading
                    }
                    color="error"
                    variant="contained"
                  >
                    SET DEPLOYMENT STATE TO NOT DEPLOYED
                  </Button>
                </span>
              </Tooltip>
            </Grid>
          </Grid>
        </Box>
      </>
    </AccessControl>
  );
}
