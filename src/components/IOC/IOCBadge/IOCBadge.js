import React from "react";
import { IconBadge } from "../../common/Badge/IconBadge";
import { IOCStatusIcon } from "../IOCIcons";

export function IOCBadge({ ioc, ...rest }) {
  return (
    <IconBadge
      icon={<IOCStatusIcon ioc={ioc} />}
      title={ioc.namingName ?? ioc.name}
      subheader={ioc.activeDeployment?.host?.fqdn || "---"}
      {...rest}
    />
  );
}
