import {
  Button,
  LinearProgress,
  Grid,
  Typography,
  Tooltip
} from "@mui/material";
import { styled } from "@mui/material/styles";
import React, {
  useState,
  useEffect,
  useContext,
  useCallback,
  useMemo
} from "react";
import { ConfirmationDialog, useAPIMethod } from "@ess-ics/ce-ui-common";
import { notificationContext } from "../../common/notification/Notifications";
import Alert from "@mui/material/Alert";
import { initRequestParams } from "../../common/Helper";
import AccessControl from "../../auth/AccessControl";
import { useNavigate } from "react-router-dom";
import { apiContext } from "../../../api/DeployApi";

const StartButton = styled(Button)(({ theme }) => ({
  backgroundColor: theme.palette.essGrass.main,
  color: theme.palette.essWhite.main,
  "&:disabled": {
    backgroundColor: theme.palette.grey[300],
    color: theme.palette.grey[500]
  },
  "&:hover": {
    backgroundColor: theme.palette.essGrass.main,
    color: theme.palette.essWhite.main
  }
}));
const StopButton = styled(Button)(({ theme }) => ({
  backgroundColor: theme.palette.essNavy.main,
  color: theme.palette.essWhite.main,
  "&:disabled": {
    backgroundColor: theme.palette.grey[300],
    color: theme.palette.grey[500]
  },
  "&:hover": {
    backgroundColor: theme.palette.essNavy.main,
    color: theme.palette.essWhite.main
  }
}));

export function IOCService({
  ioc,
  currentCommand,
  getOperations,
  buttonDisabled,
  setButtonDisabled,
  jobLazyParams
}) {
  const [error, setError] = useState();
  const navigate = useNavigate();
  const [inProgress, setInProgress] = useState(false);
  const [startModalOpen, setStartModalOpen] = useState(false);
  const [stopModalOpen, setStopModalOpen] = useState(false);
  const [command, setCommand] = useState(null);
  const { watchCommand } = useContext(notificationContext);

  const client = useContext(apiContext);

  const params = useMemo(
    () => ({
      ioc_id: ioc?.id
    }),
    [ioc?.id]
  );

  const {
    value: startJob,
    wrapper: startIOC,
    error: startJobError
  } = useAPIMethod({
    fcn: client.apis.IOCs.startIoc,
    call: false,
    params
  });

  const {
    value: stopJob,
    wrapper: stopIOC,
    error: stopJobError
  } = useAPIMethod({
    fcn: client.apis.IOCs.stopIoc,
    call: false,
    params
  });

  useEffect(() => {
    setError(startJobError?.message);
  }, [startJobError]);

  useEffect(() => {
    setError(stopJobError?.message);
  }, [stopJobError]);

  useEffect(() => {
    if (startJob && (!command || command.id !== startJob.id)) {
      watchCommand(startJob.id);
      navigate(`/jobs/${startJob.id}`);
      setCommand(startJob);
    } else if (stopJob && (!command || command.id !== stopJob.id)) {
      watchCommand(stopJob.id);
      navigate(`/jobs/${stopJob.id}`);
      setCommand(stopJob);
    } else if (currentCommand) {
      watchCommand(currentCommand.id);
      setCommand(currentCommand);
    }
  }, [startJob, stopJob, command, watchCommand, currentCommand, navigate]);

  const resetUI = useCallback(() => {
    setCommand(null);
    setError(null);
  }, []);

  const start = useCallback(() => {
    resetUI();
    setInProgress(true);
    setButtonDisabled(true);

    startIOC();

    let requestParams = initRequestParams(jobLazyParams);

    requestParams.deployment_id = ioc.activeDeployment?.id;
    getOperations(requestParams);
  }, [
    getOperations,
    ioc.activeDeployment?.id,
    jobLazyParams,
    resetUI,
    setButtonDisabled,
    startIOC
  ]);

  const stop = useCallback(() => {
    resetUI();
    setInProgress(true);
    setButtonDisabled(true);

    stopIOC();

    let requestParams = initRequestParams(jobLazyParams);

    requestParams.deployment_id = ioc.activeDeployment?.id;
    getOperations(requestParams);
  }, [
    getOperations,
    ioc.activeDeployment?.id,
    jobLazyParams,
    resetUI,
    setButtonDisabled,
    stopIOC
  ]);

  const onStartModalClose = useCallback(() => {
    setStartModalOpen(false);
  }, [setStartModalOpen]);

  const onStartModalConfirm = useCallback(() => {
    start();
  }, [start]);

  const onStopModalClose = useCallback(() => {
    setStopModalOpen(false);
  }, [setStopModalOpen]);

  const onStopModalConfirm = useCallback(() => {
    stop();
  }, [stop]);

  let disabledStartButtonTitle = "";
  if (buttonDisabled || ioc.operationInProgress) {
    disabledStartButtonTitle =
      "There is an ongoing operation, you can not Start the IOC right now";
  }

  let disabledStopButtonTitle = "";
  if (buttonDisabled || ioc.operationInProgress) {
    disabledStopButtonTitle =
      "There is an ongoing operation, you can not Stop the IOC right now";
  }

  return (
    <AccessControl
      allowedRoles={["DeploymentToolAdmin", "DeploymentToolIntegrator"]}
      renderNoAccess={() => "You are not authorized to control this IOC"}
    >
      <ConfirmationDialog
        title={
          <Typography
            variant="h2"
            marginY={1}
          >
            Start IOC
          </Typography>
        }
        content={
          <>
            <Typography component="span">
              Are you sure want to start{" "}
              <Typography
                component="span"
                fontFamily="monospace"
              >
                {ioc.namingName}
              </Typography>{" "}
              ?
            </Typography>
          </>
        }
        confirmText="Start IOC"
        cancelText="Cancel"
        open={startModalOpen}
        onClose={onStartModalClose}
        onConfirm={onStartModalConfirm}
      />
      <ConfirmationDialog
        title={
          <Typography
            variant="h2"
            marginY={1}
          >
            Stop IOC
          </Typography>
        }
        content={
          <>
            <Typography component="span">
              Are you sure want to stop{" "}
              <Typography
                component="span"
                fontFamily="monospace"
              >
                {ioc.namingName}
              </Typography>{" "}
              ?
            </Typography>
          </>
        }
        confirmText="Stop IOC"
        cancelText="Cancel"
        open={stopModalOpen}
        onClose={onStopModalClose}
        onConfirm={onStopModalConfirm}
      />
      <Grid
        container
        spacing={1}
      >
        <Grid item>
          <Tooltip title={disabledStopButtonTitle}>
            <StopButton
              onClick={() => setStopModalOpen(true)}
              disabled={buttonDisabled}
            >
              Stop
            </StopButton>
          </Tooltip>
        </Grid>
        <Grid item>
          <Tooltip title={disabledStartButtonTitle}>
            <StartButton
              onClick={() => setStartModalOpen(true)}
              disabled={buttonDisabled}
            >
              Start
            </StartButton>
          </Tooltip>
        </Grid>
        <Grid
          item
          xs={12}
          md={12}
        >
          {error ? (
            <Alert severity="error">{error}</Alert>
          ) : (
            inProgress && <LinearProgress color="primary" />
          )}
        </Grid>
      </Grid>
    </AccessControl>
  );
}
