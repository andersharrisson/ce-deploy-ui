/**
 * GitRefLink
 * Component providing link (and tag) to gitlab
 */
import React from "react";
import { Typography } from "@mui/material";
import { string } from "prop-types";
import { ExternalLink, ExternalLinkContents } from "../../common/Link";

const propTypes = {
  /** String containing url to gitlab template */
  url: string,
  /**  String containing gitlab tag */
  revision: string
};

const defaultRenderLinkContents = (revision) => {
  return <ExternalLinkContents>{revision}</ExternalLinkContents>;
};

export default function GitRefLink({
  url,
  revision,
  renderLinkContents = defaultRenderLinkContents,
  LinkProps
}) {
  // if no git reference has been entered
  if (!url || url.trim === "") {
    return <></>;
  }

  // if user enters GIT url with '.git' postfix -> remove it
  if (url.toLowerCase().endsWith(".git")) {
    url = url.toLowerCase().split(".git")[0];
  }

  // add trailing '/' if needed for GIT server address
  if (!url.endsWith("/")) {
    url += "/";
  }

  // calculate the endpoint for tag/commitId
  url = url + "-/tree/" + revision;

  return (
    <Typography display="inline">
      <ExternalLink
        href={url}
        aria-label={`External Git Link, revision ${revision}`}
        {...LinkProps}
      >
        {renderLinkContents(revision)}
      </ExternalLink>
    </Typography>
  );
}

GitRefLink.propTypes = propTypes;
