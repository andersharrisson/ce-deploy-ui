import React, { useCallback, useContext, useEffect, useState } from "react";
import {
  Button,
  TextField,
  Typography,
  Tooltip,
  CircularProgress,
  Autocomplete,
  Alert,
  Stack
} from "@mui/material";
import { useTypingTimer } from "../../common/SearchBoxFilter/TypingTimer";
import { formatDate, transformHostQuery } from "../../common/Helper";
import { apiContext } from "../../../api/DeployApi";
import { useAPIMethod, Dialog } from "@ess-ics/ce-ui-common";

export function IOCDeployDialog({
  open,
  setOpen,
  iocId,
  submitCallback,
  hasActiveDeployment,
  init = {},
  error,
  resetError,
  buttonDisabled,
  setButtonDisabled
}) {
  const client = useContext(apiContext);
  const {
    value: hosts,
    wrapper: getHosts,
    loading: loadingHosts
  } = useAPIMethod({
    fcn: client.apis.Hosts.listHosts,
    call: false
  });

  const {
    value: tagOrCommitId,
    wrapper: callGetTagOrCommitId,
    loading: loadingTagsAndCommitIds,
    error: tagOrCommitIdError
  } = useAPIMethod({
    fcn: client.apis.Git.listTagsAndCommitIds,
    call: false,
    init: []
  });
  const getTagOrCommitId = useCallback(
    (gitProjectId, reference, includeAllReference, searchMethod) => {
      callGetTagOrCommitId({
        project_id: gitProjectId,
        reference: reference,
        include_all_reference: includeAllReference,
        search_method: searchMethod
      });
    },
    [callGetTagOrCommitId]
  );

  const [host, setHost] = useState(init);
  const [query, onHostKeyUp] = useTypingTimer({ interval: 500 });

  const [gitRepo, setGitRepo] = useState(init.git || "");
  const [gitVersion, setGitVersion] = useState(init.version || "");
  const [queryGitVersion, onGitVersionKeyUp] = useTypingTimer({
    interval: 500
  });
  const gitProjectId = init.gitProjectId;

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    getHosts({ query: transformHostQuery(`${query}`) });
  }, [query, getHosts]);
  useEffect(() => {
    getTagOrCommitId(gitProjectId, queryGitVersion, false, "CONTAINS");
  }, [queryGitVersion, gitProjectId, getTagOrCommitId]);

  const onSubmit = (event) => {
    event.preventDefault();
    setButtonDisabled(true);
    const { git: gitText } = event.currentTarget.elements;
    const git = gitText.value;

    submitCallback(
      {
        ioc_id: iocId
      },
      {
        requestBody: {
          sourceUrl: git,
          sourceVersion: gitVersion,
          hostCSEntryId: host
            ? Number(host.csEntryHost.id)
            : init.csEntryHost
              ? init.csEntryHost.id
              : undefined
        }
      }
    );
  };

  // Creates, and formats tags/commitIds with dates into a table-format for using it in autocomplete
  function createOptionTags(option) {
    return (
      <table>
        <tr>
          <td style={{ width: "7em", maxWidth: "7em" }}>
            <Typography
              fontFamily="monospace"
              fontWeight="bolder"
              overflow="hidden"
              textOverflow="ellipsis"
              whiteSpace="nowrap"
            >
              {option.shortReference}
            </Typography>
          </td>
          <td style={{ width: "1em" }}>-</td>
          <td>
            <Typography
              fontFamily="monospace"
              display="inline-block"
            >
              {formatDate(option.commitDate)}
            </Typography>
          </td>
        </tr>
      </table>
    );
  }

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      title={
        <Typography
          variant="h2"
          marginY={1}
        >
          {hasActiveDeployment ? "Deploy revision" : "Deploy"}
        </Typography>
      }
      content={
        <Stack
          component="form"
          onSubmit={onSubmit}
          gap={1}
        >
          <TextField
            autoComplete="off"
            id="git"
            label="Git repository"
            variant="standard"
            defaultValue={init.git || ""}
            fullWidth
            onChange={(event) => {
              setGitRepo(event.target.value);
              resetError();
            }}
            disabled
            required
          />

          <Autocomplete
            autoHighlight
            id="version"
            options={tagOrCommitId}
            loading={loadingTagsAndCommitIds}
            clearOnBlur={false}
            defaultValue={
              init.version
                ? {
                    reference: init.version,
                    shortReference: init.shortVersion
                      ? init.shortVersion
                      : init.version
                  }
                : null
            }
            getOptionLabel={(option) => {
              return option.shortReference;
            }}
            renderOption={(props, option) => {
              return (
                <li {...props}>
                  <Tooltip title={option.description}>
                    <div>{createOptionTags(option)}</div>
                  </Tooltip>
                </li>
              );
            }}
            onChange={(event, value, reason) => {
              setGitVersion(value?.reference);
              resetError();
              getTagOrCommitId(
                gitProjectId,
                value?.reference,
                false,
                "CONTAINS"
              );
            }}
            onInputChange={(event, value, reason) => {
              event && onGitVersionKeyUp(event.nativeEvent);
            }}
            disabled={!gitRepo || gitRepo.trim() === ""}
            renderInput={(params) => (
              <TextField
                {...params}
                label="Revision"
                variant="outlined"
                fullWidth
                required
                InputProps={{
                  ...params.InputProps,
                  endAdornment: (
                    <React.Fragment>
                      {loadingTagsAndCommitIds ? (
                        <CircularProgress
                          color="inherit"
                          size={20}
                        />
                      ) : null}
                      {params.InputProps.endAdornment}
                    </React.Fragment>
                  )
                }}
                helperText={
                  tagOrCommitIdError ? `Error: ${tagOrCommitId?.message}` : ""
                }
              />
            )}
            autoSelect
          />

          <Autocomplete
            autoHighlight
            id="host"
            options={query ? hosts?.csEntryHosts ?? [] : []}
            loading={loadingHosts}
            clearOnBlur={false}
            defaultValue={init}
            getOptionLabel={(option) => {
              return option?.csEntryHost?.fqdn ?? "";
            }}
            renderInput={(params) => (
              <TextField
                {...params}
                label="Host"
                variant="outlined"
                required
                InputProps={{
                  ...params.InputProps,
                  endAdornment: (
                    <React.Fragment>
                      {loadingHosts ? (
                        <CircularProgress
                          color="inherit"
                          size={20}
                        />
                      ) : null}
                      {params.InputProps.endAdornment}
                    </React.Fragment>
                  )
                }}
              />
            )}
            onChange={(event, value, reason) => {
              setHost(value);
              resetError();
            }}
            onInputChange={(event, value, reason) => {
              event && onHostKeyUp(event.nativeEvent);
            }}
            disabled={hasActiveDeployment}
            autoSelect
            filterOptions={(options, state) => options}
          />

          {hasActiveDeployment ? (
            <Typography variant="body2">
              Hint: First undeploy IOC if you want to move to another host.
            </Typography>
          ) : (
            <></>
          )}

          {error ? <Alert severity="error">{error}</Alert> : <></>}
          <Stack
            flexDirection="row"
            justifyContent="flex-end"
            gap={2}
            marginTop={1}
          >
            <Button
              onClick={handleClose}
              color="primary"
            >
              Cancel
            </Button>
            <Button
              color="primary"
              variant="contained"
              type="submit"
              disabled={!host || !gitVersion}
            >
              Deploy
            </Button>
          </Stack>
        </Stack>
      }
    />
  );
}
