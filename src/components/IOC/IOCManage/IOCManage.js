import { Button, Stack, Tooltip, Typography } from "@mui/material";
import React, {
  useState,
  useEffect,
  useContext,
  useCallback,
  useMemo
} from "react";
import { IOCDetails } from "../IOCDetails";
import { DeployIOC } from "../DeployIOC";
import { UndeployIOC } from "../UndeployIOC";
import {
  userContext,
  useAPIMethod,
  AlertBannerList
} from "@ess-ics/ce-ui-common";
import AccessControl from "../../auth/AccessControl";
import { DeploymentStatus } from "../../../api/DataTypes";
import { IOCService } from "../IOCService";
import { JobTable } from "../../Job";
import { apiContext } from "../../../api/DeployApi";
import { ExternalLink, ExternalLinkContents } from "../../common/Link";

export function IOCManage({
  ioc,
  getIOC,
  buttonDisabled,
  currentCommand,
  operations,
  operationsLoading,
  getOperations,
  setButtonDisabled,
  pagination,
  onPage
}) {
  const { user } = useContext(userContext);
  const client = useContext(apiContext);

  const [deployDialogOpen, setDeployDialogOpen] = useState(false);
  const [undeployDialogOpen, setUndeployDialogOpen] = useState(false);

  const deploymentJobParams = useMemo(
    () => ({ awx_job_id: ioc.activeDeployment?.awxJobId }),
    [ioc]
  );

  const { value: deploymentJob, wrapper: getDeploymentJobById } = useAPIMethod({
    fcn: client.apis.Deployments.fetchJobDetails,
    call: false,
    params: deploymentJobParams
  });

  useEffect(() => {
    if (ioc.activeDeployment?.awxJobId) {
      getDeploymentJobById(ioc.activeDeployment?.awxJobId);
    }
  }, [getDeploymentJobById, ioc.activeDeployment?.awxJobId]);

  const closeDeployModal = () => {
    setDeployDialogOpen(false);
    getIOC();
  };

  const closeUndeployModal = () => {
    setUndeployDialogOpen(false);
  };

  const getSubset = useCallback(
    (ioc) => {
      ioc = { ...ioc };
      const { sourceUrl: git } = ioc;

      // Show START/STOP components only when IOC was deployed SUCCESSFULLY
      const deploymentStatus = new DeploymentStatus(
        ioc.activeDeployment,
        deploymentJob
      );
      const showControls = deploymentStatus.wasSuccessful();

      let subset = {
        "Naming service record": (
          <ExternalLink
            href={`${window.NAMING_ADDRESS}/devices.xhtml?i=2&deviceName=${ioc.namingName}`}
            aria-label="Naming Service Record, External Link"
          >
            <ExternalLinkContents>{ioc.namingName}</ExternalLinkContents>
          </ExternalLink>
        ),
        Repository: (
          <ExternalLink
            href={git}
            aria-label="Git Repository"
          >
            <ExternalLinkContents>{git}</ExternalLinkContents>
          </ExternalLink>
        )
      };

      if (user) {
        subset["IOC Service Controls"] = showControls ? (
          <IOCService
            {...{
              ioc,
              currentCommand,
              getOperations,
              buttonDisabled,
              setButtonDisabled,
              jobLazyParams: pagination
            }}
          />
        ) : (
          "IOC not yet deployed"
        );
      }

      return subset;
    },
    [
      buttonDisabled,
      pagination,
      currentCommand,
      deploymentJob,
      getOperations,
      setButtonDisabled,
      user
    ]
  );

  if (ioc) {
    const managedIOC = { ...ioc };

    const formInit = {
      name: ioc.namingName,
      description: ioc.description,
      git: ioc.sourceUrl,
      gitProjectId: ioc.gitProjectId
    };

    if (ioc.activeDeployment) {
      formInit.version = ioc.activeDeployment.sourceVersion;
      formInit.shortVersion = ioc.activeDeployment.sourceVersionShort;
      formInit.csEntryHost = {
        fqdn: ioc.activeDeployment.host.fqdn,
        id: ioc.activeDeployment.host.csEntryId
      };
    }

    let disabledDebployButtonTitle = "";
    if (buttonDisabled || ioc.operationInProgress) {
      disabledDebployButtonTitle =
        "There is an ongoing operation, you can not deploy the IOC right now";
    }

    let disabledUndeployButtonTitle = "";
    if (buttonDisabled || ioc.operationInProgress) {
      disabledUndeployButtonTitle =
        "There is an ongoing operation, you can not undeploy the IOC right now";
    }

    return (
      <Stack gap={2}>
        <IOCDetails
          ioc={managedIOC}
          getSubset={getSubset}
          alert={<AlertBannerList alerts={managedIOC.alerts} />}
          buttons={
            <>
              <Tooltip title={disabledDebployButtonTitle}>
                <span>
                  <Button
                    variant="contained"
                    color="secondary"
                    onClick={() => {
                      setDeployDialogOpen(true);
                    }}
                    disabled={buttonDisabled || ioc.operationInProgress}
                  >
                    {ioc.activeDeployment ? "Deploy revision" : "Deploy"}
                  </Button>
                </span>
              </Tooltip>
              {ioc.activeDeployment ? (
                <Tooltip title={disabledUndeployButtonTitle}>
                  <span>
                    <Button
                      variant="contained"
                      color="secondary"
                      style={{ marginRight: 20 }}
                      onClick={() => {
                        setUndeployDialogOpen(true);
                      }}
                      disabled={buttonDisabled || ioc.operationInProgress}
                    >
                      Undeploy
                    </Button>
                  </span>
                </Tooltip>
              ) : (
                <></>
              )}
            </>
          }
        />
        <AccessControl
          allowedRoles={["DeploymentToolAdmin", "DeploymentToolIntegrator"]}
          renderNoAccess={() => <></>}
        >
          <Stack gap={2}>
            <Typography variant="h3">Operations</Typography>
            <JobTable
              jobs={operations}
              pagination={pagination}
              onPage={onPage}
              loading={operationsLoading}
            />
          </Stack>
          <DeployIOC
            open={deployDialogOpen}
            setOpen={setDeployDialogOpen}
            submitCallback={closeDeployModal}
            init={formInit}
            iocId={ioc.id}
            hasActiveDeployment={Boolean(ioc.activeDeployment)}
            buttonDisabled={buttonDisabled}
            setButtonDisabled={setButtonDisabled}
          />
          <UndeployIOC
            open={undeployDialogOpen}
            setOpen={setUndeployDialogOpen}
            submitCallback={closeUndeployModal}
            buttonDisabled={buttonDisabled}
            setButtonDisabled={setButtonDisabled}
            ioc={ioc}
          />
        </AccessControl>
      </Stack>
    );
  }
  return null;
}
