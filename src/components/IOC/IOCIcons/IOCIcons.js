import React from "react";
import { Typography, useTheme } from "@mui/material";
import {
  Brightness1,
  Cancel,
  Error,
  RadioButtonUnchecked,
  ErrorOutline,
  HelpOutline,
  PlayCircleFilled,
  PauseCircleFilled
} from "@mui/icons-material";
import Popover from "../../common/Popover";
import { AlertBannerList } from "@ess-ics/ce-ui-common";
import LabeledIcon from "../../common/LabeledIcon/LabeledIcon";

function AlertMessagesPopoverContents({ title, alerts }) {
  // Filter out INFO level alerts
  // And for now filter out links on alerts due to issues with
  // focus behavior of popovers
  const alertsWithoutLinks = (
    alerts.filter((alert) => alert?.type.toLowerCase() !== "info") || []
  ).map((alert) => ({
    ...alert,
    link: undefined
  }));

  return (
    <div style={{ maxWidth: "75vw" }}>
      <Typography
        sx={{
          fontWeight: alertsWithoutLinks.length > 0 ? "bold" : "normal"
        }}
      >
        {title}
      </Typography>
      {alertsWithoutLinks.length > 0 ? (
        <AlertBannerList alerts={alertsWithoutLinks} />
      ) : null}
    </div>
  );
}

export function IOCStatusIcon({ ioc }) {
  const theme = useTheme();
  let { active, alerts, activeDeployment, id } = ioc;
  const alertSeverity = ioc?.alertSeverity?.toLowerCase();

  const iconConfig = {
    active: {
      title: "Active",
      icon: Brightness1
    },
    disabled: {
      title: "Disabled",
      icon: Cancel
    },
    alert: {
      title: "Alert",
      icon: Error
    },
    inactive: {
      title: "Inactive",
      icon: RadioButtonUnchecked
    },
    "inactive alert": {
      title: "Alert",
      icon: ErrorOutline
    },
    null: {
      title: "Unknown",
      icon: HelpOutline
    }
  };

  let status = null;

  if (active === null) active = undefined;

  if (active && (alertSeverity === undefined || alertSeverity === "info")) {
    // Active status / running
    status = "active";
  } else if (
    active === false &&
    (alertSeverity === undefined || alertSeverity === "info")
  ) {
    // stopped/paused
    status = "disabled";
  } else if (
    active !== undefined &&
    alertSeverity !== undefined &&
    alertSeverity !== "info"
  ) {
    // warning/error
    status = "alert";
  } else if (
    (active === undefined || activeDeployment === null) &&
    (alertSeverity === undefined || alertSeverity === "info")
  ) {
    // Inactive status
    status = "inactive";
  } else if (
    (active === undefined || activeDeployment === null) &&
    alertSeverity !== undefined &&
    alertSeverity !== "info"
  ) {
    // inactive with warning/error
    status = "inactive alert";
  } else {
    // unknown fallback state
    status = null;
  }

  const iconTitle = iconConfig[status].title;
  const statusIcon = (
    <LabeledIcon
      label={iconTitle}
      Icon={iconConfig[status].icon}
      labelPosition="none"
    />
  );

  return (
    <div>
      <Popover
        id={`ioc-status-popover-${id}`}
        renderOwningComponent={({
          ariaOwns,
          ariaHasPopup,
          handlePopoverOpen,
          handlePopoverClose
        }) => (
          <div
            aria-owns={ariaOwns}
            aria-haspopup={ariaHasPopup}
            onMouseEnter={handlePopoverOpen}
            onMouseLeave={handlePopoverClose}
            style={{ color: theme.palette.status.icons }}
          >
            {statusIcon}
          </div>
        )}
        popoverContents={
          <AlertMessagesPopoverContents
            title={iconTitle}
            alerts={alerts}
          />
        }
        anchorOrigin={{
          vertical: "top",
          horizontal: "right"
        }}
      />
    </div>
  );
}

export function CommandTypeIcon({
  type,
  colorStyle = "colors",
  labelPosition = "tooltip"
}) {
  const theme = useTheme();

  const commandTypeColors = {
    start: theme.palette.status.ok,
    stop: theme.palette.status.fail
  };

  const commandTypeBlack = {
    start: theme.palette.status.icons,
    stop: theme.palette.status.icons
  };

  const colorConfig = {
    black: commandTypeBlack,
    colors: commandTypeColors
  };

  const commandTypeIcons = {
    start: {
      title: "Set to active",
      icon: PlayCircleFilled
    },
    stop: {
      title: "Set to inactive",
      icon: PauseCircleFilled
    }
  };

  const iconStyle = { fill: colorConfig[colorStyle][type.toLowerCase()] };
  const iconTitle = commandTypeIcons[type.toLowerCase()].title;
  const StatusIcon = commandTypeIcons[type.toLowerCase()].icon;

  return (
    <LabeledIcon
      label={iconTitle}
      LabelProps={{ noWrap: true }}
      labelPosition={labelPosition}
      Icon={StatusIcon}
      IconProps={{ style: { iconStyle } }}
    />
  );
}
