import React, { useContext, useState, useEffect } from "react";
import { Navigate } from "react-router-dom";
import { IOCDeployDialog } from "../IOCDeployDialog";
import { notificationContext } from "../../common/notification/Notifications";
import { apiContext } from "../../../api/DeployApi";
import { useAPIMethod } from "@ess-ics/ce-ui-common";

// Process component
export function DeployIOC({
  open,
  setOpen,
  submitCallback,
  iocId,
  hasActiveDeployment,
  init = {},
  buttonDisabled,
  setButtonDisabled
}) {
  const [error, setError] = useState();
  const client = useContext(apiContext);
  const {
    value: deployment,
    wrapper: action,
    error: deployError
  } = useAPIMethod({
    fcn: client.apis.IOCs.updateAndDeployIoc,
    call: false
  });

  useEffect(() => {
    if (deployError) {
      setButtonDisabled(false);
      setError(deployError?.message);
    }
  }, [deployError, setButtonDisabled]);

  const { watchDeployment } = useContext(notificationContext);

  if (!deployment) {
    return (
      <IOCDeployDialog
        open={open}
        setOpen={setOpen}
        iocId={iocId}
        submitCallback={action}
        init={init}
        hasActiveDeployment={hasActiveDeployment}
        error={error}
        resetError={() => setError(null)}
        buttonDisabled={buttonDisabled}
        setButtonDisabled={setButtonDisabled}
      />
    );
  } else {
    submitCallback(); // This works but throws a warning because I am changing state in the parent while the child is rerendering. Not sure yet how to fix.
    watchDeployment(deployment.operationId);
    return (
      <Navigate
        to={`/jobs/${deployment.operationId}`}
        push
      />
    );
  }
}
