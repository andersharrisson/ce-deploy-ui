import React from "react";
import { Stack, Typography } from "@mui/material";
import Alert from "@mui/material/Alert";
import { ConfirmationDialog } from "@ess-ics/ce-ui-common";

export function IOCUndeployDialog({
  open,
  setOpen,
  submitCallback,
  ioc,
  error,
  buttonDisabled,
  setButtonDisabled
}) {
  const handleClose = () => {
    setOpen(false);
  };

  const onSubmit = (event) => {
    event.preventDefault();
    setButtonDisabled(true);

    submitCallback({
      ioc_id: ioc.id
    });
  };

  const onConfirm = () => {
    submitCallback({
      ioc_id: ioc.id
    });
  };

  return (
    <ConfirmationDialog
      open={open}
      onClose={handleClose}
      title={
        <Typography
          variant="h2"
          marginY={1}
        >
          Undeploy
        </Typography>
      }
      confirmText="Undeploy"
      cancelText="Cancel"
      onConfirm={onConfirm}
      content={
        <Stack
          component="form"
          onSubmit={onSubmit}
          gap={1}
        >
          <Typography>
            Do you really want to undeploy {ioc.namingName} from{" "}
            {ioc?.activeDeployment?.host?.fqdn}?
          </Typography>
          {error ? <Alert severity="error">{error}</Alert> : <></>}
        </Stack>
      }
    />
  );
}
