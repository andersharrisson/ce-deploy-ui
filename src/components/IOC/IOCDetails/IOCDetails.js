import { Grid, Box } from "@mui/material";
import React from "react";
import { KeyValueTable } from "@ess-ics/ce-ui-common";
import { IOCBadge } from "../IOCBadge";
import AccessControl from "../../auth/AccessControl";

export function diff(o1, o2) {
  let diff = Object.keys(o2).reduce((diff, key) => {
    if (o1[key] === o2[key]) return diff;
    return {
      ...diff,
      [key]: o2[key]
    };
  }, {});
  return diff;
}

const defaultSubset = ({
  name,
  description,
  host,
  active,
  status,
  git,
  version
}) => ({ name, description, host, active, status, git, version });

export function IOCDetails({ ioc, getSubset = defaultSubset, alert, buttons }) {
  ioc = { ...ioc };
  const subset = getSubset(ioc);

  return (
    <>
      <Grid
        container
        spacing={1}
      >
        {alert && (
          <Grid
            item
            xs={12}
          >
            {alert}
          </Grid>
        )}
        {buttons && (
          <Grid
            item
            xs={12}
          >
            <Box
              display="flex"
              flexDirection="row-reverse"
              p={2}
              m={1}
            >
              <AccessControl
                allowedRoles={[
                  "DeploymentToolAdmin",
                  "DeploymentToolIntegrator"
                ]}
                renderNoAccess={() => <> </>}
              >
                <>{buttons}</>
              </AccessControl>
            </Box>
          </Grid>
        )}
        <Grid
          item
          xs={12}
        >
          <IOCBadge
            ioc={ioc}
            sx={{ mt: 2.5 }}
          />
          <KeyValueTable
            obj={subset}
            variant="overline"
            sx={{ border: 0 }}
            valueOptions={{ headerName: "" }}
          />
        </Grid>
      </Grid>
    </>
  );
}
