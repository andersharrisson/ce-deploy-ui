import React from "react";
import AdministerUndeployment from "../AdministerUndeployment";
import IOCDelete from "../IOCDelete";
import IOCDetailAdmin from "../IOCDetailAdmin";
import ChangeHostAdmin from "../ChangeHostAdmin";

export default function IOCAdmin({
  ioc,
  getIOC,
  resetTab,
  buttonDisabled,
  setButtonDisabled
}) {
  return (
    <>
      <IOCDetailAdmin
        ioc={ioc}
        getIOC={getIOC}
        resetTab={resetTab}
        buttonDisabled={buttonDisabled}
        setButtonDisabled={setButtonDisabled}
      />
      {ioc.activeDeployment && (
        <ChangeHostAdmin
          ioc={ioc}
          getIOC={getIOC}
          resetTab={resetTab}
          buttonDisabled={buttonDisabled}
          setButtonDisabled={setButtonDisabled}
        />
      )}
      <AdministerUndeployment
        ioc={ioc}
        buttonDisabled={buttonDisabled}
      />
      <IOCDelete
        ioc={ioc}
        buttonDisabled={buttonDisabled}
        setButtonDisabled={setButtonDisabled}
      />
    </>
  );
}
