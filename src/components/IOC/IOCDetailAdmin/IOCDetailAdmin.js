import React, { useState, useEffect, useCallback, useContext } from "react";
import { ConfirmationDialog, useAPIMethod } from "@ess-ics/ce-ui-common";
import {
  Box,
  Button,
  CircularProgress,
  TextField,
  Tooltip,
  Typography,
  Autocomplete,
  Alert
} from "@mui/material";
import { useTypingTimer } from "../../common/SearchBoxFilter/TypingTimer";
import AccessControl from "../../auth/AccessControl";
import { apiContext } from "../../../api/DeployApi";

export default function IOCDetailAdmin({
  ioc,
  getIOC,
  resetTab,
  buttonDisabled,
  setButtonDisabled
}) {
  const [gitId, setGitId] = useState(ioc.gitProjectId);

  const [nameQuery, onNameKeyUp] = useTypingTimer({ interval: 500 });
  const [name, setName] = useState({
    uuid: ioc.namingUuid,
    description: ioc.description,
    name: ioc.namingName
  });
  const [gitInput, setGitInput] = useState(null);

  // for the dialog
  const [open, setOpen] = useState(false);
  const [error, setError] = useState();

  const client = useContext(apiContext);

  const {
    value: allowedGitProjects,
    wrapper: getAllowedGitProjects,
    loading: loadingAllowedGitProjects
  } = useAPIMethod({
    fcn: client.apis.Git.listProjects,
    call: false
  });

  const {
    value: names,
    wrapper: getNames,
    loading: loadingNames
  } = useAPIMethod({
    fcn: client.apis.Naming.fetchIOCByName,
    call: false
  });

  const {
    value: uioc,
    wrapper: actionUpdateIoc,
    error: updateError
  } = useAPIMethod({
    fcn: client.apis.IOCs.updateIoc,
    call: false
  });

  useEffect(() => {
    if (updateError) {
      setButtonDisabled(false);
      setError(updateError?.message);
    }
  }, [updateError, setError, setButtonDisabled]);

  const requiredDataMissing = useCallback(() => !gitId || !name, [gitId, name]);

  const noModification = useCallback(
    () =>
      gitId === ioc.gitProjectId &&
      name?.uuid === ioc.namingUuid &&
      name?.description === ioc.description &&
      name?.name === ioc.namingName,
    [gitId, name, ioc]
  );

  useEffect(() => {
    // fetch git repos only if user has entered a text and it wasn't previously fetched
    if (gitInput && !allowedGitProjects) {
      getAllowedGitProjects();
    }
  }, [gitInput, allowedGitProjects, getAllowedGitProjects]);

  useEffect(() => {
    if (nameQuery) {
      getNames(nameQuery);
    }
  }, [nameQuery, getNames]);

  // when user clicks Submit button a dialog should open
  const onSubmit = (event) => {
    event.preventDefault();
    setOpen(true);
  };

  const onClose = useCallback(() => {
    setOpen(false);
  }, [setOpen]);

  const onConfirm = useCallback(() => {
    setButtonDisabled(true);
    actionUpdateIoc(
      { ioc_id: ioc?.id },
      {
        requestBody: {
          gitProjectId: gitId,
          namingUuid: name ? name.uuid : null
        }
      }
    );
  }, [actionUpdateIoc, ioc, name, gitId, setButtonDisabled]);

  useEffect(() => {
    if (uioc) {
      getIOC();
      resetTab();
      setButtonDisabled(false);
    }
  }, [uioc, getIOC, resetTab, setButtonDisabled]);

  const iocIsDeployed = Boolean(ioc.activeDeployment);

  let nameDisabledTitle = "";
  if (iocIsDeployed) {
    nameDisabledTitle =
      "IOC cannot be renamed while deployed - please undeploy first";
  }

  function nameAutocomplete(disabled) {
    const isDisabled = disabled || iocIsDeployed;
    const loading = loadingNames && !isDisabled;
    return (
      <Tooltip title={nameDisabledTitle}>
        <Autocomplete
          autoHighlight
          id="namingName"
          options={nameQuery ? names ?? [] : []}
          loading={loading}
          clearOnBlur={false}
          defaultValue={name}
          getOptionLabel={(option) => {
            return option?.name ?? "";
          }}
          renderInput={(params) => (
            <TextField
              {...params}
              label="IOC name"
              variant="outlined"
              required
              InputProps={{
                ...params.InputProps,
                endAdornment: (
                  <React.Fragment>
                    {loading ? (
                      <CircularProgress
                        color="inherit"
                        size={20}
                      />
                    ) : null}
                    {params.InputProps.endAdornment}
                  </React.Fragment>
                )
              }}
            />
          )}
          onChange={(event, value, reason) => {
            setName(value);
            setError(null);
          }}
          onInputChange={(event, value, reason) => {
            event && onNameKeyUp(event.nativeEvent);
          }}
          disabled={isDisabled}
          autoSelect
        />
      </Tooltip>
    );
  }

  function gitRepoAutocomplete(disabled) {
    const isDisabled = disabled || iocIsDeployed;
    const loading = loadingAllowedGitProjects && !isDisabled;
    return (
      <Autocomplete
        autoHighlight
        id="gitId"
        options={gitInput ? allowedGitProjects ?? [] : []}
        loading={loading}
        clearOnBlur={false}
        defaultValue={{
          id: gitId,
          url: ioc.sourceUrl
        }}
        getOptionLabel={(option) => {
          return option.url;
        }}
        onChange={(event, value, reason) => {
          setGitId(value?.id);
          setError(null);
        }}
        onInputChange={(event, value, reason) => {
          if (reason === "input") {
            setGitInput(value);
          } else {
            setGitInput(null);
          }
        }}
        renderInput={(params) => (
          <TextField
            {...params}
            label="Git repository"
            variant="outlined"
            fullWidth
            required
            InputProps={{
              ...params.InputProps,
              endAdornment: (
                <React.Fragment>
                  {loading ? (
                    <CircularProgress
                      color="inherit"
                      size={20}
                    />
                  ) : null}
                  {params.InputProps.endAdornment}
                </React.Fragment>
              )
            }}
          />
        )}
        disabled={isDisabled}
        autoSelect
      />
    );
  }

  let disabledButtonTitle = "";
  if (buttonDisabled || ioc.operationInProgress) {
    disabledButtonTitle =
      "There is an ongoing operation, you can not modify the IOC right now";
  }

  return (
    <>
      <ConfirmationDialog
        title={
          <Typography
            variant="h2"
            marginY={1}
          >
            Modifying IOC
          </Typography>
        }
        content={
          <>
            <Typography component="span">
              Are you sure want to modify
              <Typography
                component="span"
                fontFamily="monospace"
              >
                {ioc.namingName}
              </Typography>{" "}
              ?
            </Typography>
          </>
        }
        confirmText="Modify IOC"
        cancelText="Cancel"
        open={open}
        onClose={onClose}
        onConfirm={onConfirm}
      />
      <Box sx={{ pt: 2 }}>
        <Typography
          sx={{ my: 2.5 }}
          variant="h3"
        >
          Modify IOC
        </Typography>
        <form
          onSubmit={onSubmit}
          style={{
            width: "100%",
            display: "flex",
            flexDirection: "column",
            gap: "0.5rem"
          }}
        >
          <AccessControl
            allowedRoles={["DeploymentToolAdmin"]}
            renderNoAccess={() => nameAutocomplete(true)}
          >
            {nameAutocomplete(iocIsDeployed)}
          </AccessControl>

          <AccessControl
            allowedRoles={["DeploymentToolAdmin"]}
            allowedUsersWithRoles={[
              { user: ioc.createdBy, role: "DeploymentToolIntegrator" }
            ]}
            renderNoAccess={() => gitRepoAutocomplete(true)}
          >
            {gitRepoAutocomplete(false)}
          </AccessControl>

          {error ? <Alert severity="error">{error}</Alert> : <></>}

          <Tooltip title={disabledButtonTitle}>
            <span>
              <Button
                color="primary"
                variant="contained"
                disabled={
                  requiredDataMissing() ||
                  noModification() ||
                  buttonDisabled ||
                  ioc.operationInProgress
                }
                type="submit"
              >
                Modify IOC
              </Button>
            </span>
          </Tooltip>
        </form>
      </Box>
    </>
  );
}
