import React, {
  useState,
  useEffect,
  useCallback,
  useContext,
  useMemo
} from "react";
import { useNavigate } from "react-router-dom";
import { Box, Button, Typography, Grid, Tooltip } from "@mui/material";
import { ConfirmationDialog } from "@ess-ics/ce-ui-common";
import Alert from "@mui/material/Alert";
import AccessControl from "../../auth/AccessControl";
import { apiContext } from "../../../api/DeployApi";
import { useAPIMethod } from "@ess-ics/ce-ui-common";

export default function IOCDelete({ ioc, buttonDisabled, setButtonDisabled }) {
  const navigate = useNavigate();

  // for the dialog
  const [error, setError] = useState();
  const [open, setOpen] = useState(false);

  const client = useContext(apiContext);

  const params = useMemo(
    () => ({
      ioc_id: ioc?.id
    }),
    [ioc]
  );

  const {
    wrapper: deleteIOC,
    dataReady: dataready,
    error: errorResponse
  } = useAPIMethod({
    fcn: client.apis.IOCs.deleteIoc,
    call: false,
    params
  });

  useEffect(() => {
    if (errorResponse) {
      setButtonDisabled(false);
      setError(errorResponse?.message);
    }
  }, [errorResponse, setError, setButtonDisabled]);

  useEffect(() => {
    if (dataready && !error) {
      setButtonDisabled(false);
      navigate(-1);
    }
  }, [dataready, navigate, error, setButtonDisabled]);

  let disabledButtonTitle = "";

  if (buttonDisabled || ioc.operationInProgress) {
    disabledButtonTitle =
      "There is an ongoing operation, you can not delete the IOC right now";
  }

  const onClose = useCallback(() => {
    setOpen(false);
  }, [setOpen]);

  const onConfirm = useCallback(() => {
    setButtonDisabled(true);
    deleteIOC();
  }, [deleteIOC, setButtonDisabled]);

  return (
    <>
      <ConfirmationDialog
        title={
          <Typography
            variant="h2"
            marginY={1}
          >
            Deleting IOC
          </Typography>
        }
        content={
          <>
            <Typography component="span">
              Are you sure want to delete
              <Typography
                component="span"
                fontFamily="monospace"
              >
                {" "}
                {ioc.namingName}
              </Typography>{" "}
              ?
            </Typography>
          </>
        }
        confirmText="Delete IOC"
        cancelText="Cancel"
        open={open}
        onClose={onClose}
        onConfirm={onConfirm}
      />

      <Box sx={{ pt: 2 }}>
        <Typography
          sx={{ my: 2.5 }}
          variant="h3"
        >
          Delete IOC
        </Typography>
        <Grid
          container
          spacing={1}
        >
          {error ? (
            <Grid
              item
              xs={12}
            >
              <Alert severity="error">{error}</Alert>
            </Grid>
          ) : (
            <></>
          )}
          <Grid
            item
            xs={12}
          >
            <AccessControl
              allowedRoles={["DeploymentToolAdmin"]}
              renderNoAccess={() => (
                <Tooltip title={"Only admins can delete the IOC"}>
                  <span>
                    <Button
                      color="error"
                      variant="contained"
                      disabled
                    >
                      Delete IOC
                    </Button>
                  </span>
                </Tooltip>
              )}
            >
              <Tooltip title={disabledButtonTitle}>
                <span>
                  <Button
                    onClick={() => setOpen(true)}
                    disabled={
                      buttonDisabled ||
                      ioc.operationInProgress ||
                      Boolean(ioc.activeDeployment)
                    }
                    color="error"
                    variant="contained"
                  >
                    Delete IOC
                  </Button>
                </span>
              </Tooltip>
            </AccessControl>
          </Grid>
        </Grid>
      </Box>
    </>
  );
}
