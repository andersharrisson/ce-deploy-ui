import React, { useContext, useState, useEffect } from "react";
import { Navigate } from "react-router-dom";
import { IOCUndeployDialog } from "../IOCUndeployDialog";
import { notificationContext } from "../../common/notification/Notifications";
import { apiContext } from "../../../api/DeployApi";
import { useAPIMethod } from "@ess-ics/ce-ui-common";

// Process component
export function UndeployIOC({
  open,
  setOpen,
  submitCallback,
  ioc,
  buttonDisabled,
  setButtonDisabled
}) {
  const [error, setError] = useState();
  const client = useContext(apiContext);
  const {
    value: deployment,
    wrapper: action,
    error: deploymentError
  } = useAPIMethod({
    fcn: client.apis.IOCs.createUndeployment,
    call: false
  });

  useEffect(() => {
    if (deploymentError) {
      setButtonDisabled(false);
      setError(deploymentError?.message);
    }
  }, [deploymentError, setButtonDisabled]);

  const { watchDeployment } = useContext(notificationContext);

  if (!deployment) {
    return (
      <IOCUndeployDialog
        open={open}
        setOpen={setOpen}
        submitCallback={action}
        ioc={ioc}
        error={error}
        buttonDisabled={buttonDisabled}
        setButtonDisabled={setButtonDisabled}
      />
    );
  } else {
    submitCallback(); // This works but throws a warning because I am changing state in the parent while the child is rerendering. Not sure yet how to fix.
    watchDeployment(deployment.operationId);
    return (
      <Navigate
        to={`/jobs/${deployment.operationId}`}
        push
      />
    );
  }
}
