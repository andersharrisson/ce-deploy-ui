import React, { useContext, useMemo } from "react";
import { apiContext } from "../../../api/DeployApi";
import { Skeleton, Tooltip, Typography } from "@mui/material";
import { useAPIMethod } from "@ess-ics/ce-ui-common";

function createIocDescription(description) {
  return (
    <>
      {description ? (
        <Tooltip
          title={description}
          arrow
          enterDelay={400}
        >
          <Typography
            sx={{
              overflow: "hidden",
              textOverflow: "ellipsis",
              whiteSpace: "nowrap"
            }}
          >
            {description}
          </Typography>
        </Tooltip>
      ) : (
        "---"
      )}
    </>
  );
}

export const IOCDescription = ({ id }) => {
  const client = useContext(apiContext);

  const params = useMemo(
    () => ({
      ioc_id: id
    }),
    [id]
  );

  const {
    value: ioc,
    loading,
    dataReady
  } = useAPIMethod({
    fcn: client.apis.IOCs.getIoc,
    params
  });

  if (loading || !dataReady) {
    return <Skeleton width="100%" />;
  }

  return createIocDescription(ioc?.description);
};
