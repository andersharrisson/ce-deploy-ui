import React, { useContext, useMemo } from "react";
import { Grid, Skeleton } from "@mui/material";
import { IOCStatusIcon } from "../IOCIcons";
import { apiContext } from "../../../api/DeployApi";
import { useAPIMethod } from "@ess-ics/ce-ui-common";

function createRequest(id) {
  return {
    ioc_id: id
  };
}

export const IOCStatus = ({ id, activeDeployment }) => {
  const client = useContext(apiContext);

  const params = useMemo(() => createRequest(id), [id]);

  const {
    value: ioc,
    loading,
    dataReady
  } = useAPIMethod({
    fcn: client.apis.Monitoring.fetchIocStatus,
    params
  });

  return (
    <Grid
      container
      direction="column"
      justifyContent="center"
      alignItems="center"
    >
      {loading || !dataReady ? (
        <Skeleton
          variant="circular"
          height={20}
          width={20}
        />
      ) : (
        <IOCStatusIcon ioc={{ id, activeDeployment, ...ioc }} />
      )}
    </Grid>
  );
};
