import React from "react";
import { Table } from "@ess-ics/ce-ui-common";
import { Link } from "@mui/material";
import { IOCDescription } from "./IOCDescription";
import { IOCStatus } from "./IOCStatus";
import { noWrapText } from "../../common/Helper";

const exploreIocsColumns = [
  {
    field: "status",
    headerName: "Status",
    flex: 0,
    headerAlign: "center",
    align: "center"
  },
  {
    field: "namingName",
    headerName: "IOC name",
    sortable: false
  },
  {
    field: "description",
    headerName: "Description",
    sortable: false
  },
  { field: "host", headerName: "Host", width: "10ch", sortable: false },
  { field: "network", headerName: "Network", width: "10ch", sortable: false }
];

const hostDetailsColumns = [
  {
    field: "status",
    headerName: "Status",
    flex: 0,
    headerAlign: "center",
    align: "center"
  },
  {
    field: "namingName",
    headerName: "IOC name",
    sortable: false
  },
  {
    field: "description",
    headerName: "Description",
    sortable: false,
    flex: 3
  }
];

function createHostLink(host) {
  if (host?.externalHostId) {
    return (
      <Link href={`/hosts/${host?.externalHostId}`}>
        {noWrapText(host.hostName || "---")}
      </Link>
    );
  }

  return "---";
}

function createTableRowForHostDetails(ioc) {
  const { id, name, namingName, activeDeployment } = ioc;

  return {
    id: id,
    status: (
      <IOCStatus
        id={ioc.id}
        activeDeployment={activeDeployment}
      />
    ),
    namingName: (
      <Link href={`/iocs/${id}`}>{noWrapText(namingName ?? name)}</Link>
    ),
    description: <IOCDescription id={ioc.id} />
  };
}

function createTableRowForExploreIocs(ioc) {
  const { id, namingName, activeDeployment } = ioc;

  return {
    id: id,
    status: (
      <IOCStatus
        id={ioc.id}
        activeDeployment={ioc.activeDeployment}
      />
    ),
    namingName: (
      <Link href={`/iocs/${id}`}>{noWrapText(namingName ?? "---")}</Link>
    ),
    description: <IOCDescription id={ioc.id} />,
    host: createHostLink(activeDeployment?.host),
    network: noWrapText(activeDeployment?.host?.network || "---")
  };
}

export function IOCTable({
  iocs = [],
  rowType = "explore",
  loading,
  pagination,
  onPage
}) {
  const tableTypeSpecifics = {
    host: [hostDetailsColumns, createTableRowForHostDetails],
    explore: [exploreIocsColumns, createTableRowForExploreIocs]
  };

  const [columns, createRow] = tableTypeSpecifics[rowType];
  const rows = iocs.map((ioc) => createRow(ioc));

  return (
    <Table
      columns={columns}
      rows={rows}
      pagination={pagination}
      onPage={onPage}
      loading={loading}
      disableHover
    />
  );
}
