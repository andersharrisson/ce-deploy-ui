import React from "react";
import { composeStories } from "@storybook/testing-react";
import * as stories from "../../../stories/components/common/IOC/IOCTable.stories";

const { AfterAsync } = composeStories(stories);
const textColumns = ["IOC name", "Description", "Host", "Network"];
const columns = ["Status"].concat(textColumns);
const firstRowData = [
  "Active",
  "VacS-RFQ:SC-IOC-130",
  "Some description",
  "vacs-accv-vm-ioc",
  "ChannelAccess-FEB"
];

describe("HostTable", () => {
  context("Populated Table", () => {
    beforeEach(() => {
      cy.mount(<AfterAsync />);
    });

    it("Has the correct columns", () => {
      cy.findAllByRole("columnheader").each(($el, index) => {
        console.debug(index, columns[index]);
        cy.wrap($el).contains(columns[index], { matchCase: false });
      });
    });

    it("Truncates all text content", () => {
      cy.findAllByRole("row")
        .eq(1) // first row is headers, so get next index
        .find(".MuiTypography-noWrap")
        .should("have.length", textColumns.length - 1);
    });

    it("Displays correct content in first row", () => {
      cy.findAllByRole("row")
        .eq(1) // first row is headers, so get next index
        .each(($el, index) => {
          if (index === 0) {
            const iconTitle = firstRowData[index];
            cy.wrap($el).findByRole("img", { name: iconTitle });
          } else {
            cy.wrap($el)
              .find("p")
              .should(($el) => {
                const text = $el.text().trim().toLowerCase();
                const expected = firstRowData[index].trim().toLowerCase();
                cy.expect(text).to.equal(expected);
              });
          }
        });
    });
  });
});
