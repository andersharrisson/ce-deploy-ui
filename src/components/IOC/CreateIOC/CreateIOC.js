import React, { useEffect, useState, useContext } from "react";
import { useNavigate } from "react-router-dom";
import { useTypingTimer } from "../../common/SearchBoxFilter/TypingTimer";
import { RootPaper } from "@ess-ics/ce-ui-common/dist/components/common/container/RootPaper";
import {
  Alert,
  Autocomplete,
  Button,
  CircularProgress,
  LinearProgress,
  Stack,
  TextField,
  Typography
} from "@mui/material";
import { useAPIMethod } from "@ess-ics/ce-ui-common";
import { apiContext } from "../../../api/DeployApi";

const renderErrorMessage = (error) => {
  const { response, status: requestStatus, message: requestMessage } = error;
  const {
    body: { description },
    status: httpStatus
  } = response;

  return `${httpStatus ?? requestStatus ?? "unknown"}: ${
    description ?? requestMessage ?? "An unknown error has occurred"
  }`;
};

export function CreateIOC() {
  const navigate = useNavigate();
  const [name, setName] = useState();
  const [gitId, setGitId] = useState(null);
  const [gitInput, setGitInput] = useState(null);
  const [nameQuery, onNameKeyUp] = useTypingTimer({ interval: 500 });

  const client = useContext(apiContext);

  const {
    value: allowedGitProjects,
    wrapper: getAllowedGitProjects,
    loading: loadingAllowedGitProjects
  } = useAPIMethod({
    fcn: client.apis.Git.listProjects,
    call: false
  });

  const {
    value: ioc,
    wrapper: createIoc,
    loading,
    error
  } = useAPIMethod({
    fcn: client.apis.IOCs.createIoc,
    call: false
  });

  const {
    value: names,
    wrapper: getNames,
    loading: loadingNames
  } = useAPIMethod({
    fcn: client.apis.Naming.fetchIOCByName,
    call: false
  });

  // Return home on cancel
  const handleCancel = () => {
    navigate("/");
  };

  // fetch new names when name query changes
  useEffect(() => {
    if (nameQuery) {
      getNames(nameQuery);
    }
  }, [nameQuery, getNames]);

  // create the ioc on form submit
  const onSubmit = (event) => {
    event.preventDefault();
    createIoc(
      {},
      {
        requestBody: {
          gitProjectId: gitId,
          namingUuid: name ? name.uuid : undefined
        }
      }
    );
  };

  // navigate home once ioc created
  useEffect(() => {
    if (ioc) {
      navigate(`/iocs/${ioc.id}`);
    }
  }, [ioc, navigate]);

  return (
    <RootPaper
      sx={{ display: "flex", flexDirection: "column", alignItems: "center" }}
    >
      <Stack
        component="form"
        onSubmit={onSubmit}
        gap={2}
        aria-live="polite"
        width="600px"
      >
        <Typography variant="h2">Create new IOC</Typography>
        <Autocomplete
          autoHighlight
          id="nameAutocomplete"
          options={nameQuery ? names ?? [] : []}
          loading={loadingNames}
          clearOnBlur={false}
          getOptionLabel={(option) => {
            return option?.name ?? "";
          }}
          renderInput={(params) => (
            <TextField
              {...params}
              label="IOC name"
              variant="outlined"
              required
              InputProps={{
                ...params.InputProps,
                endAdornment: (
                  <React.Fragment>
                    {loadingNames ? (
                      <CircularProgress
                        color="inherit"
                        size={20}
                      />
                    ) : null}
                    {params.InputProps.endAdornment}
                  </React.Fragment>
                )
              }}
              disabled={loading}
              helperText={name ? name.description : ""}
            />
          )}
          onChange={(event, value, reason) => {
            setName(value);
          }}
          onInputChange={(event, value, reason) => {
            event && onNameKeyUp(event.nativeEvent);
          }}
          autoSelect
        />

        <Autocomplete
          autoHighlight
          id="gitId"
          options={gitInput ? allowedGitProjects ?? [] : []}
          loading={loadingAllowedGitProjects}
          clearOnBlur={false}
          defaultValue={{
            id: null,
            url: ""
          }}
          getOptionLabel={(option) => {
            return option?.url ?? "";
          }}
          onChange={(event, value, reason) => {
            setGitId(value?.id);
          }}
          onInputChange={(event, value, reason) => {
            if (reason === "input") {
              setGitInput(value);

              // load the git projects only if user entered text and data is not (being) fetched
              if (!allowedGitProjects && !loadingAllowedGitProjects) {
                getAllowedGitProjects();
              }
            } else {
              setGitInput(null);
            }
          }}
          renderInput={(params) => (
            <TextField
              {...params}
              label="Git repository"
              variant="outlined"
              fullWidth
              required
              InputProps={{
                ...params.InputProps,
                endAdornment: (
                  <React.Fragment>
                    {loadingAllowedGitProjects ? (
                      <CircularProgress
                        color="inherit"
                        size={20}
                      />
                    ) : null}
                    {params.InputProps.endAdornment}
                  </React.Fragment>
                )
              }}
              disabled={loading}
            />
          )}
          autoSelect
        />
        {error ? (
          <Alert severity="error">{renderErrorMessage(error)}</Alert>
        ) : (
          <></>
        )}
        <Stack
          direction="row"
          justifyContent="flex-end"
        >
          <Button
            onClick={handleCancel}
            color="primary"
            disabled={loading}
          >
            Cancel
          </Button>
          <Button
            color="primary"
            variant="contained"
            type="submit"
            disabled={!name || !gitId || loading}
          >
            Create
          </Button>
        </Stack>
        {loading ? (
          <LinearProgress
            aria-busy="true"
            aria-label="Creating IOC, please wait"
          />
        ) : null}
      </Stack>
    </RootPaper>
  );
}
