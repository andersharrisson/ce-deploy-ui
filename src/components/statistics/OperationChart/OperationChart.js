import React from "react";
import { useEffect } from "react";
import { formatDateOnly } from "../../common/Helper";
import { circularPalette } from "../../common/Helper";
import { LinearProgress, useTheme, Typography } from "@mui/material";
import {
  AreaChart,
  Area,
  CartesianGrid,
  XAxis,
  YAxis,
  Legend,
  ResponsiveContainer,
  Tooltip
} from "recharts";

export default function OperationChart({
  title,
  iocDeployments,
  getIOCDeployments,
  abortGetIOCDeployments
}) {
  const theme = useTheme();

  useEffect(() => {
    getIOCDeployments();

    return () => {
      abortGetIOCDeployments();
    };
  }, [getIOCDeployments, abortGetIOCDeployments]);

  return iocDeployments ? (
    <>
      <Typography variant="h2">{title}</Typography>
      <ResponsiveContainer
        width="100%"
        aspect={3}
      >
        <AreaChart
          data={
            iocDeployments?.operationStatistics
              ? Object.values(iocDeployments?.operationStatistics)
              : []
          }
        >
          <Area
            type="linear"
            stroke={circularPalette(theme.palette.graph, 0)}
            dataKey="deploymentCount"
            fillOpacity={0.2}
            fill={circularPalette(theme.palette.graph, 0)}
            strokeWidth={2}
            stackId="1"
            name="Deployments"
          />
          <Area
            type="linear"
            stroke={circularPalette(theme.palette.graph, 1)}
            dataKey="commandCount"
            fillOpacity={0.2}
            stackId="1"
            fill={circularPalette(theme.palette.graph, 1)}
            strokeWidth={2}
            name="Commands"
          />
          <CartesianGrid
            stroke="#ccc"
            strokeDasharray="5 5"
          />
          <XAxis
            dataKey="historyDate"
            tickFormatter={formatDateOnly}
          />
          <YAxis
            type="number"
            allowDataOverflow
          />
          <Legend
            verticalAlign="top"
            height={36}
          />
          <Tooltip
            labelFormatter={(value) => {
              return `Date: ${formatDateOnly(value)}`;
            }}
          />
        </AreaChart>
      </ResponsiveContainer>
    </>
  ) : (
    <div style={{ width: "100%" }}>
      <LinearProgress color="primary" />
    </div>
  );
}
