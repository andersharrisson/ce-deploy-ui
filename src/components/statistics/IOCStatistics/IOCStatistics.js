import React, { useContext, useEffect } from "react";
import { Box, Skeleton } from "@mui/material";
import { KeyValueTable } from "@ess-ics/ce-ui-common/dist/components/common/KeyValueTable";
import { apiContext } from "../../../api/DeployApi";
import { useAPIMethod } from "@ess-ics/ce-ui-common";

const renderValue = (val) => {
  return val ?? <Skeleton width="3ch" />;
};

export function IOCStatistics() {
  const client = useContext(apiContext);

  // Do not call on render; when one of these finishes then it and the others
  // will be called again on the next render; this causes an infinite loop.
  const {
    value: iocsRegistered,
    wrapper: getIocsRegistered,
    abort: abortGetIocsRegistered
  } = useAPIMethod({
    fcn: client.apis.Statistics.calculateStatistics,
    call: false
  });
  const {
    value: iocsDeployed,
    wrapper: getIocsDeployed,
    abort: abortGetIocsDeployed
  } = useAPIMethod({
    fcn: client.apis.Statistics.calculateStatistics,
    call: false
  });
  const {
    value: iocsReachable,
    wrapper: getIocsReachable,
    abort: abortGetIocsReachable
  } = useAPIMethod({
    fcn: client.apis.Statistics.calculateStatistics,
    call: false
  });

  useEffect(() => {
    getIocsRegistered({ statistics_type: "IOCS_REGISTERED" });
    getIocsDeployed({ statistics_type: "IOCS_DEPLOYED" });
    getIocsReachable({ statistics_type: "IOCS_RUNNING" });

    return () => {
      abortGetIocsRegistered();
      abortGetIocsDeployed();
      abortGetIocsReachable();
    };
  }, [
    abortGetIocsDeployed,
    abortGetIocsReachable,
    abortGetIocsRegistered,
    getIocsDeployed,
    getIocsReachable,
    getIocsRegistered
  ]);

  const iocStats = {
    "Registered IOCs": renderValue(iocsRegistered?.value),
    "Deployed IOCs": renderValue(iocsDeployed?.value),
    "Running IOCs": renderValue(iocsReachable?.value)
  };

  return (
    <Box>
      <KeyValueTable
        obj={iocStats}
        keyOptions={{ headerName: "IOC statistics" }}
        valueOptions={{
          headerName: "",
          align: "right"
        }}
        sx={{ border: 0 }}
      />
    </Box>
  );
}
