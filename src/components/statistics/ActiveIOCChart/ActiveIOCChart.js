import React, { useContext } from "react";
import { useEffect } from "react";
import { LinearProgress, useTheme, Typography } from "@mui/material";
import { circularPalette } from "../../common/Helper";
import {
  BarChart,
  Bar,
  Tooltip,
  ResponsiveContainer,
  CartesianGrid,
  XAxis,
  YAxis,
  Cell,
  LabelList
} from "recharts";
import { apiContext } from "../../../api/DeployApi";
import { useAPIMethod } from "@ess-ics/ce-ui-common";

export default function ActiveIOCChart({ title }) {
  const theme = useTheme();
  const GRAPH_THRESHOLD = 0.2;
  const client = useContext(apiContext);

  const { value: activeIocs, wrapper: getActiveIocs } = useAPIMethod({
    fcn: client.apis.Statistics.currentlyActiveIocs,
    call: false
  });

  useEffect(() => {
    getActiveIocs();
  }, [getActiveIocs]);

  const iocObjToArray = () => {
    if (activeIocs) {
      return Object.values(activeIocs);
    }

    return [];
  };

  const renderCustomizedLabel = (props) => {
    const { x, y, width, value, fill } = props;
    const radius = 15;

    return (
      <g>
        <circle
          cx={x + width / 2}
          cy={y - radius - 10}
          r={radius}
          fill={fill}
        />
        <text
          x={x + width / 2}
          y={y - radius - 10}
          fill="#fff"
          textAnchor="middle"
          dominantBaseline="middle"
          style={{ fontWeight: "bold" }}
        >
          {value}
        </text>
      </g>
    );
  };

  return activeIocs ? (
    <>
      <Typography
        variant="h2"
        sx={{ marginBottom: 4 }}
      >
        {title}
      </Typography>
      <ResponsiveContainer
        width="100%"
        aspect={2}
      >
        <BarChart
          data={iocObjToArray()}
          margin={{ bottom: 120 }}
        >
          <XAxis
            dataKey="network"
            interval={0}
            angle="-50"
            textAnchor="end"
            sclaeToFit="true"
            verticalAnchor="start"
          />
          <YAxis
            domain={[
              0,
              (dataMax) => Math.floor(dataMax * (1 + GRAPH_THRESHOLD))
            ]}
          />
          <CartesianGrid
            stroke="#ccc"
            strokeDasharray="5 5"
          />
          <Bar
            dataKey="iocCount"
            fill={circularPalette(theme.palette.graph, 0)}
            isAnimationActive={false}
            minPointSize={10}
            name="No. of IOCs running"
          >
            {iocObjToArray().map((entry, index) => (
              <Cell
                fill={circularPalette(theme.palette.graph, index)}
                key={entry}
              />
            ))}
            <LabelList
              dataKey="iocCount"
              content={renderCustomizedLabel}
            />
          </Bar>
          <Tooltip cursor={false} />
        </BarChart>
      </ResponsiveContainer>
    </>
  ) : (
    <div style={{ width: "100%" }}>
      <LinearProgress color="primary" />
    </div>
  );
}
