import React, { useContext, useEffect } from "react";
import { Box, Skeleton } from "@mui/material";
import { KeyValueTable, useAPIMethod } from "@ess-ics/ce-ui-common";
import { apiContext } from "../../../api/DeployApi";

const renderValue = (val) => {
  return val ?? <Skeleton width="3ch" />;
};

export function HostStatistics() {
  const client = useContext(apiContext);

  // Do not call on render; when one of these finishes then it and the others
  // will be called again on the next render; this causes an infinite loop.
  const {
    value: hostsRegistered,
    wrapper: getHostsRegistered,
    abort: abortGetHostsRegistered
  } = useAPIMethod({
    fcn: client.apis.Statistics.calculateStatistics,
    call: false
  });
  const {
    value: hostsWithIocs,
    wrapper: getHostsWithIocs,
    abort: abortGetHostsWithIocs
  } = useAPIMethod({
    fcn: client.apis.Statistics.calculateStatistics,
    call: false
  });
  const {
    value: hostsReachable,
    wrapper: getHostsReachable,
    abort: abortGetHostsReachable
  } = useAPIMethod({
    fcn: client.apis.Statistics.calculateStatistics,
    call: false
  });

  useEffect(() => {
    getHostsRegistered({ statistics_type: "HOSTS_REGISTERED" });
    getHostsWithIocs({ statistics_type: "HOSTS_WITH_IOCS" });
    getHostsReachable({ statistics_type: "HOSTS_REACHABLE" });

    return () => {
      abortGetHostsRegistered();
      abortGetHostsWithIocs();
      abortGetHostsReachable();
    };
  }, [
    getHostsRegistered,
    getHostsWithIocs,
    getHostsReachable,
    abortGetHostsRegistered,
    abortGetHostsWithIocs,
    abortGetHostsReachable
  ]);

  const hostStats = {
    "Registered IOC-hosts": renderValue(hostsRegistered?.value),
    "IOC-hosts with IOCs": renderValue(hostsWithIocs?.value),
    "Reachable IOC-hosts with IOCs": renderValue(hostsReachable?.value)
  };

  return (
    <Box>
      <KeyValueTable
        obj={hostStats}
        variant="table"
        keyOptions={{ headerName: "Host statistics" }}
        valueOptions={{
          headerName: "",
          align: "right"
        }}
        sx={{ border: 0 }}
      />
    </Box>
  );
}
