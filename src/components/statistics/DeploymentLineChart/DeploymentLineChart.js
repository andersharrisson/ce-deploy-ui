import React from "react";
import { useEffect } from "react";
import { formatDateOnly } from "../../common/Helper";
import { circularPalette } from "../../common/Helper";
import { LinearProgress, useTheme, Typography } from "@mui/material";
import {
  AreaChart,
  Area,
  CartesianGrid,
  XAxis,
  YAxis,
  ResponsiveContainer,
  Tooltip
} from "recharts";

export default function DeploymentLineChart({
  title,
  chartLabel,
  iocDeployments,
  getIOCDeployments,
  abortGetIOCDeployments
}) {
  const theme = useTheme();

  useEffect(() => {
    getIOCDeployments();

    return () => {
      abortGetIOCDeployments();
    };
  }, [getIOCDeployments, abortGetIOCDeployments]);

  const GRAPH_THRESHOLD = 0.05;

  return iocDeployments ? (
    <>
      <Typography
        variant="h2"
        sx={{ marginBottom: 4 }}
      >
        {title}
      </Typography>
      <ResponsiveContainer
        width="100%"
        aspect={3}
      >
        <AreaChart data={iocDeployments ? Object.values(iocDeployments) : []}>
          <Area
            type="linear"
            stroke={circularPalette(theme.palette.graph, 0)}
            dataKey="iocCount"
            fillOpacity={0.2}
            fill={circularPalette(theme.palette.graph, 0)}
            strokeWidth={2}
            name={chartLabel}
          />
          <CartesianGrid
            stroke="#ccc"
            strokeDasharray="5 5"
          />
          <XAxis
            dataKey="historyDate"
            tickFormatter={formatDateOnly}
          />
          <YAxis
            dataKey="iocCount"
            type="number"
            domain={[
              (dataMin) => Math.floor(dataMin * (1 - GRAPH_THRESHOLD)),
              (dataMax) => Math.floor(dataMax * (1 + GRAPH_THRESHOLD))
            ]}
          />
          <Tooltip
            labelFormatter={(value) => {
              return `Date: ${formatDateOnly(value)}`;
            }}
          />
        </AreaChart>
      </ResponsiveContainer>
    </>
  ) : (
    <div style={{ width: "100%" }}>
      <LinearProgress color="primary" />
    </div>
  );
}
