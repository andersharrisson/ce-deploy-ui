import React from "react";
import { SnackbarProvider } from "notistack";
import { Container, CssBaseline, StyledEngineProvider } from "@mui/material";
import { ThemeProvider } from "@mui/material/styles";
import { theme } from "../style/Theme";
import { UserProvider } from "../api/UserProvider";
import { NotificationProvider } from "../components/common/notification/Notifications";
import NavigationMenu from "../components/navigation/NavigationMenu";
import { MemoryRouter } from "react-router-dom";
import { DeployAPIProvider } from "../api/DeployApi";
import { TestUserProvider, UserImpersonator } from "./UserImpersonator";

export function RouterHarness({ children, initialHistory = ["/"] }) {
  return (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <MemoryRouter initialEntries={initialHistory}>
          <DeployAPIProvider>{children}</DeployAPIProvider>
        </MemoryRouter>
      </ThemeProvider>
    </StyledEngineProvider>
  );
}

export function AppHarness({
  children,
  initialHistory = ["/"],
  useTestUser = false,
  ...args
}) {
  const SelectedUserProvider = useTestUser
    ? ({ children }) => (
        <TestUserProvider>
          <UserImpersonator {...args}>{children}</UserImpersonator>
        </TestUserProvider>
      )
    : UserProvider;

  return (
    <SnackbarProvider
      preventDuplicate
      maxSnack="5"
    >
      <RouterHarness initialHistory={initialHistory}>
        <SelectedUserProvider>
          <NotificationProvider>
            <NavigationMenu>
              <Container>{children}</Container>
            </NavigationMenu>
          </NotificationProvider>
        </SelectedUserProvider>
      </RouterHarness>
    </SnackbarProvider>
  );
}
