import { rest } from "msw";
import spec from "./fixtures/ccce-api.json";
import OpenAPIBackend from "openapi-backend";
import { apiHandlers } from "./mockAPI";

// create our mock backend with openapi-backend
// we can use this to automatically fill in unimplemented api endpoints
// using examples or response schemas from the api specification
const api = new OpenAPIBackend({
  definition: spec,
  handlers: {
    validationFail: async (c, res, ctx) =>
      res(ctx.status(400), ctx.json({ err: c.validation.errors })),
    notFound: (c, res, ctx) => res(ctx.status(404)),
    notImplemented: async (c, res, ctx) => {
      const { status, mock } = await api.mockResponseForOperation(
        c.operation.operationId
      );
      return res(ctx.status(status), ctx.json(mock));
    }
  }
});
api.init();

const glue = (requestHandler) => {
  return (req, res, ctx) => {
    const d = requestHandler(req);
    const body = ctx.json(d.body || {});
    const status = ctx.status(d.status || 200);
    let cookie;
    // the only cookie the mockAPI will set is the authorization cookie
    const cookieHeader = d.headers?.["Set-Cookie"];
    if (cookieHeader) {
      const [name, value] = cookieHeader.split(";")[0].split("=");
      const opts = cookieHeader.includes("Max-Age")
        ? { expires: new Date(0) }
        : {};
      cookie = ctx.cookie(name, value, opts);
    }
    return res(body, status, cookie);
  };
};

const restMap = {
  GET: rest.get,
  POST: rest.post
};

// define our MSW handlers to mock our API endpoints
export const handlers = apiHandlers.map((h) => {
  const method = restMap[h.method];
  return method(h.matcher, glue(h.requestHandler));
});

const catchAllHandler = (req, res, ctx) => {
  console.warn("[MSW] Catchall API request", req.url.href, req);
  // the OpenAPIBackend.hanlde request expects a certain form to the input
  // see https://github.com/anttiviljami/openapi-backend/blob/master/DOCS.md#handlerequestreq-handlerargs
  const digested_request = {
    method: req.method,
    path: req.url.pathname, // path must not be undefined
    body: req.body,
    query: req.url.search,
    headers: req.headers
  };
  // leave a catch with warning in here for now because it's tricky
  return api
    .handleRequest(digested_request, res, ctx)
    .catch((x) => console.warn(x));
};

handlers.push(rest.get("*/api/v1/*", catchAllHandler));
handlers.push(rest.post("*/api/v1/*", catchAllHandler));
handlers.push(rest.put("*/api/v1/*", catchAllHandler));
handlers.push(rest.patch("*/api/v1/*", catchAllHandler));
handlers.push(rest.delete("*/api/v1/*", catchAllHandler));
