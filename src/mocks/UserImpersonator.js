import React, { useCallback, useContext, useEffect, useState } from "react";
import testUser from "./fixtures/User.json";
import { userContext } from "@ess-ics/ce-ui-common";

const testAuthContext = userContext;

export const defaultUser = testUser;
export const defaultUserRoles = [
  "DeploymentToolAdmin",
  "DeploymentToolIntegrator"
];
const defaultLogin = (username, password) => {
  console.log(
    `Login called with username '${username}', password '${password}'`
  );
};
const defaultLogout = () => {
  console.log("Logout called");
};
const defaultLoginError = "";

const TestAuthContextProvider = ({ children }) => {
  const [user, setUser] = useState(defaultUser);
  const [userRoles, setUserRoles] = useState(defaultUserRoles);
  const [login, setLogin] = useState(defaultLogin);
  const [loginError, setLoginError] = useState(defaultLoginError);
  const [logout, setLogout] = useState(defaultLogout);

  const resetLoginError = useCallback(() => {
    setLoginError(defaultLoginError);
  }, [setLoginError]);

  const value = {
    user,
    setUser,
    userRoles,
    setUserRoles,
    login,
    setLogin,
    loginError,
    setLoginError,
    logout,
    setLogout,
    resetLoginError
  };

  return (
    <testAuthContext.Provider value={value}>
      {children}
    </testAuthContext.Provider>
  );
};

export const TestUserProvider = ({ children }) => {
  return <TestAuthContextProvider>{children}</TestAuthContextProvider>;
};

export const UserImpersonator = ({ user, userRoles, children }) => {
  const { setUser, setUserRoles } = useContext(testAuthContext);

  useEffect(() => {
    setUser(user);
    setUserRoles(userRoles);
  }, [setUser, setUserRoles, user, userRoles]);

  return <>{children}</>;
};
