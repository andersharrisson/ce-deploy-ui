import { matchPath } from "react-router-dom";

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
function randBetween(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1) + min);
}

function getParameters(req, pattern) {
  const match = matchPath(
    { path: "/api/v1" + pattern },
    new URL(req.url).pathname
  );
  return match?.params;
}

function withMockPagination({ req, data, key }) {
  const params = new URL(req.url).searchParams;
  const page = Number(params.get("page")) || 0;
  const limit = Number(params.get("limit")) || 20;

  const startIndex = page * limit;
  const pagedData = data[key].slice(startIndex, startIndex + limit);

  const results = {
    limit,
    listSize: pagedData.length,
    pageNumber: page,
    totalCount: data[key].length,
    [key]: pagedData,
    DEVELOPER_NOTE: "THIS IS MOCKED PAGING!"
  };

  return results;
}

function spec(req) {
  const data = require("./fixtures/ccce-api.json");
  return {
    body: data
  };
}

function login(req) {
  const token = "DEADBEEF";
  return {
    body: { token },
    headers: {
      "Set-Cookie": `ce-deploy-auth=${token}`
    }
  };
}

function logout(req) {
  return {
    headers: {
      "Set-Cookie": "ccce-auth=; Max-Age=0"
    }
  };
}

function isLoggedIn(req) {
  if (req.cookies) {
    return Boolean(req.cookies["ce-deploy-auth"]);
  } else {
    return (req.headers?.cookie ?? "").includes("ce-deploy-auth=");
  }
}

function auth(routeHandler) {
  return function (req) {
    if (isLoggedIn(req)) {
      return routeHandler(req);
    } else {
      return { status: 401 };
    }
  };
}

function getUserRoles(req) {
  const body = ["DeploymentToolAdmin", "DeploymentToolIntegrator"];
  return { body };
}

// Git
function infoFromUserName(req) {
  const body = [require("./fixtures/User.json")];
  return { body };
}
function listProjects(req) {
  const body = require("./fixtures/GitProjects.json");
  return { body };
}
function listTagsAndCommitIds(req) {
  const body = require("./fixtures/GitTagsAndCommits.json");
  return { body };
}

function renew(req) {
  const token = "FADEDEAD";
  return {
    body: { token },
    headers: {
      "Set-Cookie": `ce-deploy-auth=${token}`
    }
  };
}

function listIOCs(req) {
  const body = require("./fixtures/PagedIOCResponse.json");
  return { body };
}

function createIoc(req) {
  const body = require("./fixtures/CreateIocResult.json");
  return { body };
}

function getIOC(req) {
  const params = getParameters(req, "/iocs/:id");
  const data = require("./fixtures/IOCDetails.json");
  const body = data.find((x) => x.id === Number(params.id));
  console.debug("getIOC", data, params, body);
  const status = body ? 200 : 404;
  return { body, status };
}

function getDeployment(req) {
  console.debug(req);
  const params = getParameters(req, "/deployments/:id");
  console.debug(params);
  const data = require("./fixtures/DeploymentInfoDetails.json");
  const body = data.find((x) => x.id === Number(params.id));
  const status = body ? 200 : 404;
  return { body, status };
}

function getDeploymentJob(req) {
  const params = getParameters(req, "/deployments/operations/job/:id");
  const data = require("./fixtures/AwxJobDetails.json");
  const body = data.find((x) => x.id === Number(params.id));
  const status = body ? 200 : 404;
  return { body, status };
}

function getDeploymentJobLog(req) {
  const params = getParameters(req, "/deployments/jobs/:id/log");
  const jobDetailsData = require("./fixtures/AwxJobDetails.json");
  const index = jobDetailsData.findIndex((x) => x.id === Number(params.id));
  const logData = require("./fixtures/AwxJobLog.json");
  const body = logData[index];
  const status = body ? 200 : 404;
  return { body, status };
}

function listOperations(req) {
  const data = require("./fixtures/OperationList.json");
  const body = withMockPagination({ req, data, key: "operations" });
  return { body };
}

function fetchOperation(req) {
  const params = getParameters(req, "/deployments/operations/:id");
  const data = require("./fixtures/OperationList.json");

  const body = data.operations.find((x) => x?.id === Number(params?.id));
  const status = body ? 200 : 404;
  return { body, status };
}

function getCommandList(req) {
  const body = require("./fixtures/PagedCommandResponse.json");
  return { body };
}

function getCSEntryHostWithStatus(req) {
  const params = getParameters(req, "/hosts/:id/with_status");
  const data = require("./fixtures/CSEntryHostWithStatus.json");
  const body = data.find((x) => x.csEntryHost.id === Number(params.id));
  const status = body ? 200 : 404;
  return { body, status };
}

function listHosts(req) {
  const data = require("./fixtures/Hosts.json");
  const body = withMockPagination({ req, data, key: "csEntryHosts" });
  return { body };
}

function getProcservLogs(req) {
  const body = require("./fixtures/LokiResponse.json");
  return { body };
}

function fetchIocStatus(req) {
  const params = getParameters(req, "/monitoring/status/:id");
  const data = require("./fixtures/IOCStatus.json");
  const { id, alertSeverity, alerts, active } = data.find(
    (x) => `${x.id}` === params.id
  );

  const body = {
    iocId: id,
    alertSeverity,
    alerts,
    active
  };
  console.debug("fetchIocStatus", data, params, body);
  const status = body ? 200 : 404;

  return { body, status };
}

function getStatistics(req) {
  const body = require("./fixtures/GeneralStatisticsResponse.json");
  return { body };
}

function currentlyActiveIocs(req) {
  const body = require("./fixtures/ActiveIocStatisticsResponse.json");
  return { body };
}

function iocDeploymentHistory(req) {
  const body = require("./fixtures/DeployedIOCsHistory.json");
  return { body };
}

function activeIocHistory(req) {
  const body = require("./fixtures/ActiveIOCsForHistoryResponse.json");
  return { body };
}

function operationHistory(req) {
  const body = require("./fixtures/OperationHistoryStatistics.json");
  return { body };
}

function calculateStatistics(req) {
  const params = getParameters(req, "/statistics/general/:val");
  const validValues = [
    "HOSTS_REGISTERED",
    "HOSTS_WITH_IOCS",
    "HOSTS_REACHABLE",
    "HOSTS_WITHOUT_ISSUE",
    "IOCS_REGISTERED",
    "IOCS_DEPLOYED",
    "IOCS_RUNNING",
    "IOCS_RUNNING_WITHOUT_ISSUE"
  ];
  if (validValues.includes(params?.val)) {
    return {
      status: 200,
      body: {
        statisticType: params.val,
        value: randBetween(100, 1500)
      }
    };
  } else {
    return { status: 400 };
  }
}

function fetchIOCByName(req) {
  const body = require("./fixtures/NamingNames.json");
  return { body };
}

// just to organize
const mockAPI = {
  spec: spec,
  authentication: {
    login,
    logout,
    renew,
    getUserRoles
  },
  iocs: {
    listIOCs,
    getIOC,
    createIoc
  },
  deployments: {
    getDeployment,
    getDeploymentJob,
    getDeploymentJobLog,
    getCommandList,
    listOperations,
    fetchOperation
  },
  hosts: {
    getCSEntryHostWithStatus,
    listHosts
  },
  git_helper: {
    infoFromUserName,
    listProjects,
    listTagsAndCommitIds
  },
  monitoring: {
    getProcservLogs,
    fetchIocStatus
  },
  statistics: {
    getStatistics,
    currentlyActiveIocs,
    iocDeploymentHistory,
    activeIocHistory,
    calculateStatistics,
    operationHistory
  },
  naming: {
    fetchIOCByName
  }
};

// Enough info to create handlers in cypress or MSW
const makeHandler = (method, matcher, requestHandler, wrapper) => ({
  method,
  matcher,
  requestHandler: wrapper ? wrapper(requestHandler) : requestHandler,
  name: requestHandler.name // use caution when changing api function names
});

const queryPattern = "(\\?.*)?$";
const qRegExp = (pattern) => RegExp(pattern + queryPattern);

// Handlers for our whole API
export const apiHandlers = [
  // api spec
  makeHandler("GET", qRegExp(".*/api/spec"), mockAPI.spec),

  // authentication
  makeHandler(
    "POST",
    qRegExp(".*/authentication/login"),
    mockAPI.authentication.login
  ),
  makeHandler(
    "POST",
    qRegExp(".*/authentication/logout"),
    mockAPI.authentication.logout,
    auth
  ),
  makeHandler("POST", qRegExp(".*/renew"), mockAPI.authentication.renew, auth),
  makeHandler(
    "GET",
    qRegExp(".*/authentication/roles"),
    mockAPI.authentication.getUserRoles,
    auth
  ),

  // iocs
  makeHandler("GET", qRegExp(".*/iocs"), mockAPI.iocs.listIOCs),
  makeHandler("POST", qRegExp(".*/iocs"), mockAPI.iocs.createIoc),
  makeHandler("GET", qRegExp(".*/iocs/[0-9]+"), mockAPI.iocs.getIOC),

  // git helper
  makeHandler(
    "GET",
    qRegExp(".*/git_helper/user_info"),
    mockAPI.git_helper.infoFromUserName,
    auth
  ),
  makeHandler(
    "GET",
    qRegExp(".*/git_helper/projects"),
    mockAPI.git_helper.listProjects
  ),
  makeHandler(
    "GET",
    qRegExp(".*/git_helper/[0-9]+/tags_and_commits"),
    mockAPI.git_helper.listTagsAndCommitIds
  ),

  // deployments
  makeHandler(
    "GET",
    qRegExp(".*/operations"),
    mockAPI.deployments.listOperations
  ),
  makeHandler(
    "GET",
    qRegExp(".*/deployments/[0-9]+"),
    mockAPI.deployments.getDeployment
  ),
  makeHandler(
    "GET",
    qRegExp(".*/deployments/operations/[0-9]+"),
    mockAPI.deployments.fetchOperation,
    auth
  ),
  makeHandler(
    "GET",
    qRegExp(".*/deployments/operations/job/[0-9]+"),
    mockAPI.deployments.getDeploymentJob
  ),
  makeHandler(
    "GET",
    qRegExp(".*/deployments/jobs/[0-9]+/log"),
    mockAPI.deployments.getDeploymentJobLog,
    auth
  ),

  // commands
  makeHandler(
    "GET",
    qRegExp(".*/deployments/commands"),
    mockAPI.deployments.getCommandList
  ),

  // hosts
  makeHandler("GET", qRegExp(".*/hosts"), mockAPI.hosts.listHosts),
  makeHandler(
    "GET",
    qRegExp(".*/hosts/[0-9]+/with_status"),
    mockAPI.hosts.getCSEntryHostWithStatus
  ),

  // monitoring
  makeHandler(
    "GET",
    qRegExp(".*/monitoring/procserv/host/.+"),
    mockAPI.monitoring.getProcservLogs,
    auth
  ),
  makeHandler(
    "GET",
    qRegExp(".*/monitoring/status/.+"),
    mockAPI.monitoring.fetchIocStatus
  ),

  // statistics
  makeHandler(
    "GET",
    qRegExp(".*/statistics/general"),
    mockAPI.statistics.getStatistics
  ),
  makeHandler(
    "GET",
    qRegExp(".*/statistics/general/active_ioc_statistics"),
    mockAPI.statistics.currentlyActiveIocs
  ),
  makeHandler(
    "GET",
    qRegExp(".*/statistics/general/deployed_ioc_history"),
    mockAPI.statistics.iocDeploymentHistory
  ),
  makeHandler(
    "GET",
    qRegExp(".*/statistics/general/active_ioc_history"),
    mockAPI.statistics.activeIocHistory
  ),
  makeHandler(
    "GET",
    qRegExp(".*/statistics/general/operation_history"),
    mockAPI.statistics.operationHistory
  ),
  makeHandler(
    "GET",
    qRegExp(".*/statistics/general/.*"),
    mockAPI.statistics.calculateStatistics
  ),
  // naming
  makeHandler(
    "GET",
    qRegExp(".*/naming/ioc_names_by_name"),
    mockAPI.naming.fetchIOCByName
  )
];
