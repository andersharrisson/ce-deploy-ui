import { useCallback, useEffect, useRef } from "react";

const defaultCleanup = () => {};

export function useInterval(
  callback,
  interval,
  call = false,
  cleanup = defaultCleanup
) {
  useEffect(() => {
    if (call) callback(); // call the callback right away
    const id = setInterval(callback, interval);
    return () => {
      clearInterval(id);
      cleanup();
    };
  }, [call, callback, interval, cleanup]);
}

export function useSafePolling(callback, busy, interval, call = true, cleanup) {
  const busyRef = useRef(busy);
  busyRef.current = busy;

  const poll = useCallback(() => {
    if (!busyRef.current) {
      callback();
    }
  }, [callback, busyRef]);
  useInterval(poll, interval, call, cleanup);
}
