import React from "react";
import { UserDetailsContainer } from "../../../views/UserPage";
import { AppHarness } from "../../../mocks/AppHarness";
import { userImpersonatorArgs } from "../../utils/common-args";

export default {
  title: "Views/UserPage/UserPageView"
};

const Template = (args) => (
  <AppHarness
    useTestUser
    {...args}
  >
    <UserDetailsContainer />
  </AppHarness>
);

export const Default = (args) => <Template {...args} />;
Default.args = {
  ...userImpersonatorArgs
};
