import React from "react";
import { StatisticsView } from "../../../views/statistics/StatisticsView";
import { AppHarness } from "../../../mocks/AppHarness";

export default {
  title: "Views/Statistics/StatisticsView"
};

export const Default = () => (
  <AppHarness>
    <StatisticsView />
  </AppHarness>
);
