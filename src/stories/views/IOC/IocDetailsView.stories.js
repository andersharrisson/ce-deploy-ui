import React from "react";
import { AppHarness } from "../../../mocks/AppHarness";
import { IOCDetailsContainer } from "../../../views/IOC/IOCDetailsContainer";
import { userImpersonatorArgs } from "../../utils/common-args";

export default {
  title: "Views/IOC/IOCDetailsView"
};

const Template = (args) => (
  <AppHarness
    useTestUser
    {...args}
  >
    <IOCDetailsContainer
      id={346}
      {...args}
    />
  </AppHarness>
);

export const Default = (args) => <Template {...args} />;
Default.args = {
  ...userImpersonatorArgs
};

export const Deployable = (args) => <Template {...args} />;
Deployable.args = {
  id: 2,
  ...Default.args
};
