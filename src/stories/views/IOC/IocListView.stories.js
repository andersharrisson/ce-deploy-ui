import React from "react";
import { AppHarness } from "../../../mocks/AppHarness";
import { rest } from "msw";
import { handlers } from "../../../mocks/handlers";
import { IOCListView } from "../../../views/IOC/IOCListView";

export default {
  title: "Views/IOC/IocListView"
};

const Template = () => (
  <AppHarness>
    <IOCListView />
  </AppHarness>
);

export const Default = () => <Template />;

export const LoadingAsyncCells = () => <Template />;
LoadingAsyncCells.parameters = {
  msw: {
    handlers: [
      rest.get("*/iocs/*", (req, res, ctx) => res(ctx.delay("infinite"))),
      ...handlers
    ]
  }
};
