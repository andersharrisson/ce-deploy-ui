import React from "react";
import { AppHarness } from "../../../mocks/AppHarness";
import { CreateIOCView } from "../../../views/IOC/CreateIOCView";
import { rest } from "msw";
import { handlers } from "../../../mocks/handlers";
import { userImpersonatorArgs } from "../../utils/common-args";

export default {
  title: "Views/IOC/CreateIOCView"
};

const Template = (args) => (
  <AppHarness
    useTestUser
    {...args}
  >
    <CreateIOCView />
  </AppHarness>
);

export const Default = (args) => <Template {...args} />;
Default.args = {
  ...userImpersonatorArgs
};

export const CreateLoading = (args) => <Template {...args} />;
CreateLoading.args = {
  ...Default.args
};
CreateLoading.parameters = {
  msw: {
    handlers: [
      rest.post("*/iocs", (req, res, ctx) => res(ctx.delay("infinite"))),
      ...handlers
    ]
  }
};

export const CreateWithError = (args) => <Template {...args} />;
CreateWithError.args = {
  ...Default.args
};
CreateWithError.parameters = {
  msw: {
    handlers: [
      rest.post("*/iocs", (req, res, ctx) =>
        res(
          ctx.status(400),
          ctx.json(require("../../../mocks/fixtures/GeneralException.json"))
        )
      ),
      ...handlers
    ]
  }
};
