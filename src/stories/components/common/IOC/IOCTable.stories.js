import React from "react";
import { IOCTable } from "../../../../components/IOC/IOCTable";
import { Box } from "@mui/material";
import iocs from "../../../../mocks/fixtures/PagedIOCResponse.json";
import { rest } from "msw";
import { handlers } from "../../../../mocks/handlers";
import { RouterHarness } from "../../../../mocks/AppHarness";
import {
  hideStorybookControls,
  paginationNoResults
} from "../../../utils/common-args";

export default {
  title: "IOC/IOCTable",
  argTypes: {
    rowType: {
      options: ["explore", "host"],
      control: { type: "radio" }
    },
    iocs: hideStorybookControls,
    pagination: hideStorybookControls,
    onPage: hideStorybookControls
  }
};

const Template = (args) => {
  return (
    <RouterHarness>
      <Box height="90vh">
        <IOCTable {...args} />
      </Box>
    </RouterHarness>
  );
};

export const Empty = (args) => <Template {...args} />;

Empty.args = {
  rowType: "explore",
  loading: false,
  iocs: [],
  pagination: paginationNoResults,
  onPage: () => {}
};

export const EmptyLoading = (args) => <Template {...args} />;

EmptyLoading.args = {
  ...Empty.args,
  loading: true
};

export const BeforeAsync = (args) => <Template {...args} />;

BeforeAsync.args = {
  ...Empty.args,
  pagination: { ...paginationNoResults, totalCount: iocs.totalCount },
  iocs: iocs.iocList
};
BeforeAsync.argTypes = {
  loading: hideStorybookControls
};
BeforeAsync.parameters = {
  msw: {
    handlers: [
      rest.get("*/iocs/*", (req, res, ctx) => res(ctx.delay("infinite"))),
      rest.get("*/monitoring/status/*", (req, res, ctx) =>
        res(ctx.delay("infinite"))
      ),
      ...handlers
    ]
  }
};

export const AfterAsync = (args) => <Template {...args} />;
AfterAsync.args = { ...BeforeAsync.args };
AfterAsync.argTypes = { ...BeforeAsync.argTypes };
