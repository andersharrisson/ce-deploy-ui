import React from "react";
import LabeledIcon from "../../../../components/common/LabeledIcon/LabeledIcon";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import { RouterHarness } from "../../../../mocks/AppHarness";

export default {
  title: "common/LabeledIcon",
  argTypes: {
    labelPosition: {
      options: ["none", "right", "left"],
      control: { type: "radio" }
    }
  }
};

const Template = (args) => {
  return (
    <RouterHarness>
      <LabeledIcon
        Icon={AccountCircleIcon}
        {...args}
      />
    </RouterHarness>
  );
};

export const Default = (args) => <Template {...args} />;
Default.args = {
  label: "Account",
  LabelProps: { fontWeight: "bold" },
  labelPosition: "right",
  IconProps: { color: "primary" }
};
