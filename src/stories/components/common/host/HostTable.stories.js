import React from "react";
import { Container } from "@mui/material";
import hosts from "../../../../mocks/fixtures/PagedCSEntryHostResponse.json";
import { HostTable } from "../../../../components/host/HostTable";
import { RouterHarness } from "../../../../mocks/AppHarness";
import { hideStorybookControls } from "../../../utils/common-args";

export default {
  title: "Host/HostTable",
  argTypes: {
    hosts: hideStorybookControls,
    totalCount: hideStorybookControls,
    rowsPerPage: hideStorybookControls,
    lazyParams: hideStorybookControls,
    columnSort: hideStorybookControls,
    setLazyParams: hideStorybookControls,
    setColumnSort: hideStorybookControls
  }
};

const Template = (args) => (
  <RouterHarness>
    <Container>
      <HostTable {...args} />
    </Container>
  </RouterHarness>
);

export const Empty = (args) => <Template {...args} />;

Empty.args = {
  loading: false,
  hosts: [],
  totalCount: 0,
  rowsPerPage: [5, 10, 20, 50, 100],
  lazyParams: { rows: 10, page: 0 },
  columnSort: { sortField: null, sortOrder: null },
  setLazyParams: () => {},
  setColumnSort: () => {}
};

export const EmptyLoading = (args) => <Template {...args} />;

EmptyLoading.args = {
  ...Empty.args,
  loading: true
};

export const Populated = (args) => <Template {...args} />;

Populated.args = {
  ...Empty.args,
  totalCount: hosts.totalCount,
  hosts: hosts.csEntryHosts
};

Populated.argTypes = {
  totalCount: hideStorybookControls,
  hosts: hideStorybookControls,
  loading: hideStorybookControls
};
