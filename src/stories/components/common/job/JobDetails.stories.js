import React from "react";
import { Container, StyledEngineProvider } from "@mui/material";
import { ThemeProvider } from "@mui/material/styles";
import { theme } from "../../../../style/Theme";
import { JobDetails } from "../../../../components/Job/JobDetails";
import deploymentOperation from "../../../../mocks/fixtures/OperationDeploymentDetails.json";
import { RouterHarness } from "../../../../mocks/AppHarness";

export default {
  title: "Jobs/JobDetails"
};

const Template = (args) => (
  <StyledEngineProvider injectFirst>
    <ThemeProvider theme={theme}>
      <RouterHarness>
        <Container>
          <JobDetails {...args} />
        </Container>
      </RouterHarness>
    </ThemeProvider>
  </StyledEngineProvider>
);

export const Deployment = (args) => <Template {...args} />;

Deployment.args = {
  operation: deploymentOperation,
  job: {
    status: "SUCCESSFUL"
  }
};
