import React from "react";
import { RouterHarness } from "../../../../mocks/AppHarness";
import { JobTable } from "../../../../components/Job/JobTable";
import {
  hideStorybookControls,
  paginationNoResults
} from "../../../utils/common-args";
import { Box } from "@mui/material";
import operationList from "../../../../mocks/fixtures/OperationList.json";

export default {
  title: "Jobs/JobTable",
  argTypes: {
    rowType: {
      options: ["jobLog", "userPageJobLog"],
      control: { type: "radio" }
    },
    jobs: hideStorybookControls,
    pagination: hideStorybookControls,
    onPage: hideStorybookControls
  }
};

const Template = (args) => (
  <RouterHarness>
    <Box height="90vh">
      <JobTable {...args} />
    </Box>
  </RouterHarness>
);

export const Empty = (args) => <Template {...args} />;

Empty.args = {
  rowType: "jobLog",
  loading: false,
  jobs: [],
  pagination: paginationNoResults,
  onPage: () => {}
};

export const EmptyLoading = (args) => <Template {...args} />;

EmptyLoading.args = {
  ...Empty.args,
  loading: true
};

export const Populated = (args) => <Template {...args} />;

Populated.args = {
  ...Empty.args,
  jobs: operationList.operations,
  pagination: { ...paginationNoResults, totalCount: operationList.totalCount }
};

Populated.argTypes = {
  loading: hideStorybookControls
};
