import React, { useContext, useEffect } from "react";
import { Navigate, Route, Routes, BrowserRouter } from "react-router-dom";
import { StyledEngineProvider } from "@mui/material";
import { ThemeProvider } from "@mui/material/styles";
import { CssBaseline } from "@mui/material";
import { theme } from "./style/Theme";
import { IOCListView } from "./views/IOC/IOCListView";
import NavigationMenu from "./components/navigation/NavigationMenu";
import { IOCDetailsAccessControl } from "./views/IOC/IOCDetailsAccessControl";
import { JobDetailsAccessControl } from "./views/jobs/JobDetailsAccessControl";
import { UserProvider } from "./api/UserProvider";
import { HostListView } from "./views/host/HostListView";
import { HostDetailsAccessControl } from "./views/host/HostDetailsAccessControl";
import { StatisticsView } from "./views/statistics/StatisticsView";
import { HelpView } from "./views/help/HelpView";
import { SnackbarProvider } from "notistack";
import TokenRenew from "./components/auth/TokenRenew";
import { NotificationProvider } from "./components/common/notification/Notifications";
import { NotFoundView } from "./components/navigation/NotFoundView";
import { AppErrorBoundary } from "@ess-ics/ce-ui-common";
import { LoginView } from "./views/login/LoginView";
import { JobLogAccessControl } from "./views/jobs/JobLogAccessControl";
import { RecordListView } from "./views/records/RecordListView";
import { RecordDetailsView } from "./views/records/RecordDetailsView";
import { TestErrorView } from "./views/TestErrorView";
import { GlobalAppBarContext } from "@ess-ics/ce-ui-common";
import { applicationTitle } from "./components/common/Helper";
import { CreateIOCAccessControl } from "./views/IOC/CreateIOCAccessControl";
import { UserDetailsAccessControl } from "./views/UserPage/UserDetailsAccessControl";
import { DeployAPIProvider } from "./api/DeployApi";

// setting up the application (TAB)title
function App() {
  const { setTitle } = useContext(GlobalAppBarContext);
  useEffect(() => setTitle(applicationTitle()), [setTitle]);

  return (
    <AppErrorBoundary supportHref={window.SUPPORT_URL}>
      <BrowserRouter>
        <SnackbarProvider
          preventDuplicate
          maxSnack="5"
        >
          <StyledEngineProvider injectFirst>
            <ThemeProvider theme={theme}>
              <CssBaseline />
              <DeployAPIProvider>
                <UserProvider>
                  <TokenRenew />
                  <NotificationProvider>
                    <NavigationMenu>
                      <Routes>
                        <Route
                          path="/"
                          element={<Navigate to="/iocs" />}
                          exact
                        />
                        <Route
                          path="/records"
                          element={<RecordListView />}
                          exact
                        />
                        <Route
                          path="/records/:name"
                          element={<RecordDetailsView />}
                          exact
                        />
                        <Route
                          path="/iocs/create"
                          element={<CreateIOCAccessControl />}
                          exact
                        />
                        <Route
                          path="/iocs/:id"
                          element={<IOCDetailsAccessControl exact />}
                        />
                        <Route
                          path="/iocs"
                          element={<IOCListView />}
                          exact
                        />
                        <Route
                          path="/jobs"
                          element={<JobLogAccessControl />}
                          exact
                        />
                        <Route
                          path="/jobs/:id"
                          element={<JobDetailsAccessControl exact />}
                        />
                        <Route
                          path="/hosts/:id"
                          element={<HostDetailsAccessControl />}
                          exact
                        />
                        <Route
                          path="/hosts"
                          element={<HostListView />}
                          exact
                        />
                        <Route
                          path="/statistics"
                          element={<StatisticsView />}
                          exact
                        />
                        <Route
                          path="/help"
                          element={<HelpView />}
                          exact
                        />
                        <Route
                          path="/login"
                          element={<LoginView />}
                          exact
                        />
                        <Route
                          path="/error-test"
                          element={<TestErrorView />}
                          exact
                        />
                        <Route
                          path="/user/:userName"
                          element={<UserDetailsAccessControl />}
                          exact
                        />
                        <Route
                          path="*"
                          element={<NotFoundView />}
                          exact
                        />
                      </Routes>
                    </NavigationMenu>
                  </NotificationProvider>
                </UserProvider>
              </DeployAPIProvider>
            </ThemeProvider>
          </StyledEngineProvider>
        </SnackbarProvider>
      </BrowserRouter>
    </AppErrorBoundary>
  );
}

export default App;
