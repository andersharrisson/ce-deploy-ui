# build CE deploy & monitor Frontend
FROM node:16.14.0 as builder
ARG REACT_APP_MUI_PRO_LICENSE_KEY
RUN mkdir /usr/src/app
WORKDIR /usr/src/app
ENV PATH /usr/src/app/node_modules/.bin:$PATH
COPY . /usr/src/app
#Remove node-cache if building from local env that compiled project previously natively
RUN rm -rf /usr/src/app/node_modules
RUN npm ci

ENV PUBLIC_URL "/"
ENV REACT_APP_LOGIN_METHOD "STD"
RUN REACT_APP_MUI_PRO_LICENSE_KEY=${REACT_APP_MUI_PRO_LICENSE_KEY} npm run build

# production environment
FROM nginx:1.19.6-alpine
RUN rm -rf /etc/nginx/conf.d 
RUN mkdir /etc/nginx/conf.d
COPY docker/default.conf /etc/nginx/conf.d
COPY --from=builder /usr/src/app/build /usr/share/nginx/html/
EXPOSE 8080
CMD ["nginx", "-g", "daemon off;"]
