const { defineConfig } = require("cypress");
const cucumber = require('cypress-cucumber-preprocessor').default

module.exports = defineConfig({
  fixturesFolder: "src/mocks/fixtures",
  projectId: "ea57uq",
  viewportWidth: 1280,
  viewportHeight: 720,

  e2e: {
    // We've imported your old cypress plugins here.
    // You may want to clean this up later by importing these.
    setupNodeEvents(on, config) {
      on('file:preprocessor', cucumber());
    },
    specPattern: "cypress/e2e/**/*.{feature,features}",
  },

  component: {
    setupNodeEvents(on, config) {},
    specPattern: "src/**/*.spec.{js,jsx,ts,tsx}",
    devServer: {
      framework: "create-react-app",
      bundler: "webpack",
    },
  }

});
